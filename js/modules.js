let renderCaptcha = function(element) {
    element.html("");
    let siteKey = element.attr("data-site-key");
    if(siteKey !== undefined && siteKey.trim() !== "") {
        let inner = $('<div></div>');
        inner.appendTo(element);
        let theme = element.attr("data-theme");
        if(theme === undefined || theme.trim() === "") {
            theme = "light";
        }
        grecaptcha.render(inner[0], {"sitekey": siteKey, "theme": theme, "callback":function(response) {
            element.parent().find('.captcha-response').val(response);
            }});
    } else {
        element.html('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Captcha is not setup correctly!</div>');
    }
};
loadedCaptcha = function() {
    $('.captchaContainer').each(function() {
        renderCaptcha($(this));
    });
};

let initParticles = function(element) {
    let child = $('<div class="particle-container">');
    //TODO improve id generation. This "Could" throw errors on multiple per page!
    let id = "particles-"+Math.floor(Math.random()*100);
    child.attr("id",id);
    child.prependTo(element);
    loadParticles(child);
};

let loadParticles = function(element) {
    particlesJS.load(element.attr("id"), '/js/particles-config.json', function() {
        console.log('callback - particles.js config loaded');
    });
};

$('.particle-container').each(function() {
    loadParticles($(this));
});

let lastHash = "";
let hashChanged = function(hash) {
    console.log(hash);
    if(lastHash !== hash) {
        lastHash = hash;
        let top = 0;
        if(hash !== '') {
            top = $(hash).offset().top
        }
        window.scroll({
            top: top - 56,
            left: 0,
            behavior: 'smooth'
        });
    }
};

if ("onhashchange" in window) { // event supported?
    window.onhashchange = function () {
        hashChanged(window.location.hash);
    }
}
else { // event not supported:
    let storedHash = window.location.hash;
    window.setInterval(function () {
        if (window.location.hash !== storedHash) {
            storedHash = window.location.hash;
            hashChanged(storedHash);
        }
    }, 100);
}

/*
var x = document.getElementsByClassName("module-map");
for (let i = 0; i < x.length; i++) {
    let iframe = x[i].getElementsByTagName('iframe')[0];
    let newSrc = iframe.getAttribute("data-src");
    iframe.setAttribute('src', newSrc);
}*/
/*
$('.module.module-map iframe').each(function() {
    let newSrc = $(this).attr("data-src");
    if(newSrc !== undefined) {
        //$(this)[0].setAttribute('src', newSrc);
    }
});*/