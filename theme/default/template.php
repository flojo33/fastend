<?php Session::StartSession(); ?>
<?php if (!Fastend::GetEditor()) { ?>
    <!doctype html>
    <html lang="en">
    <head>
        <?php Fastend::GetConsentScript(); ?>
        <?php Fastend::GetAnalyticsScript(); ?>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/fastend.css?2">
        <!-- Multi-Dropdown CSS -->
        <link rel="stylesheet" href="/css/multi-dropdown.css">
        <?php Seo::GenerateMetaTags(); ?>
        <title><?php echo Fastend::GetPageName(); ?></title>
    </head>
    <body>
<?php } ?>
<?php Fastend::CreateEditBar(); ?>
<?php echo Fastend::GetNavbar('navbar-dark bg-dark'); ?>
<!--------------------------------->
<!-- Page Specific Content Start -->
<!--------------------------------->
<div class="page-content-container"><?php echo Fastend::GetPageContent(); ?></div>
<!--------------------------------->
<!--  Page Specific Content End  -->
<!--------------------------------->

<?php echo Fastend::GetPageFooter(); ?>

<?php if (!Fastend::GetEditor()) { ?>
    <?php Fastend::GetScripts(); ?>
    <?php Fastend::GetConsent(); ?>

    </body>
    </html>
<?php } ?>