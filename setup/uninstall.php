<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
Sql::executeCommandFast('DROP TABLE `page_content`');
Sql::executeCommandFast('DROP TABLE `page`');
Sql::executeCommandFast('DROP TABLE `language`');
Sql::executeCommandFast('DROP TABLE `system`');
Sql::executeCommandFast('DROP TABLE `user_session_right`');
Sql::executeCommandFast('DROP TABLE `user`');
Sql::executeCommandFast('DROP TABLE `session_right`');
unlink($_SERVER['DOCUMENT_ROOT'].'/.htaccess');
unlink($_SERVER['DOCUMENT_ROOT'].'/mysql-settings.php');