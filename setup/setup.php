<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
$error = "";
if(count($_POST) != 0){
    /*
     * MySQL Settings
     */
    if(isset($_POST["mysql-server"]) && trim($_POST["mysql-server"]) != "") {
        $mysqlServer = $_POST["mysql-server"];
    } else {
        $error .= "<li>Please enter the MySQL Server Address</li>";
    }
    if(isset($_POST["mysql-database"]) && trim($_POST["mysql-database"]) != "") {
        $mysqlDatabase = $_POST["mysql-database"];
    } else {
        $error .= "<li>Please enter the MySQL Database</li>";
    }
    if(isset($_POST["mysql-username"]) && trim($_POST["mysql-username"]) != "") {
        $mysqlUsername = $_POST["mysql-username"];
    } else {
        $error .= "<li>Please enter the MySQL Username</li>";
    }
    if(isset($_POST["mysql-password"]) && trim($_POST["mysql-password"]) != "") {
        $mysqlPassword = $_POST["mysql-password"];
    } else {
        $error .= "<li>Please enter the MySQL Password</li>";
    }
    /*
     * Administrator Settings
     */

    if(isset($_POST["admin-username"]) && trim($_POST["admin-username"]) != "") {
        $adminUser = $_POST["admin-username"];
    } else {
        $error .= "<li>Please enter a Username for the Administrator account.</li>";
    }
    if(isset($_POST["admin-password"]) && trim($_POST["admin-password"]) != "") {
        $adminPassword = $_POST["admin-password"];
    } else {
        $error .= "<li>Please enter a Password for the Administrator account.</li>";
    }
    if(isset($_POST["admin-password-repeat"]) && trim($_POST["admin-password-repeat"]) != "") {
        $adminPasswordRepeat = $_POST["admin-password-repeat"];
    } else {
        $error .= "<li>Please repeat the Password for the Administrator account.</li>";
    }
    if($adminPassword != $adminPasswordRepeat) {
        $error .= "<li>The two admin passwords do not Match!</li>";
    }

    if($error == "") {
        Sql::initialize($mysqlServer,$mysqlUsername,$mysqlPassword);
        try {
            Sql::connect($mysqlDatabase);
        } catch (Exception $e) {
            $error .= '<li>Could not connect to MySQL Database using the given Server Login!</li>';
        }
        if($error == "") {

            $htaccessFile = $_SERVER['DOCUMENT_ROOT']."/.htaccess";
            if(file_exists($htaccessFile)){
                unlink($htaccessFile);
            }
            //Create .htaccess File
            $htaccessContent = file_get_contents($_SERVER['DOCUMENT_ROOT']."/setup/.htaccess_template");
            file_put_contents($htaccessFile, $htaccessContent);

            $mysqlSettingsFile = $_SERVER['DOCUMENT_ROOT']."/mysql-settings.php";
            if(file_exists($mysqlSettingsFile)){
                unlink($mysqlSettingsFile);
            }
            //Create .htaccess File
            $mysqlSettingsFileContents = file_get_contents($_SERVER['DOCUMENT_ROOT']."/setup/mysql-settings-template.php");
            $mysqlSettingsFileContents = str_replace('$server',"'".$mysqlServer."'", $mysqlSettingsFileContents);
            $mysqlSettingsFileContents = str_replace('$username',"'".$mysqlUsername."'", $mysqlSettingsFileContents);
            $mysqlSettingsFileContents = str_replace('$password',"'".$mysqlPassword."'", $mysqlSettingsFileContents);
            $mysqlSettingsFileContents = str_replace('$database',"'".$mysqlDatabase."'", $mysqlSettingsFileContents);
            file_put_contents($mysqlSettingsFile, "<?php\n".$mysqlSettingsFileContents."\n?>");

            //Check Tables are empty!
            $tables = Sql::executeQueryFast("SHOW TABLES");
            if(count($tables) != 0) {
                $error .= '<li>The connected database is not empty. Not starting Setup! (Make sure to connect to a blank MySQL Database before continuing)</li>';
            } else {
                //Create Tables in database
                $sqlSetupScripts = file_get_contents($_SERVER['DOCUMENT_ROOT']."/setup/database.sql");
                //var_dump($sqlSetupScripts);
                Sql::executeMultiQuery($sqlSetupScripts);

                //echo "Completed initial init. Running Updates...";
                Utilities::RunUpdates(true);

                try {
                    User::createNew($adminUser,$adminPassword);
                } catch (Exception $e) {
                    //This should not happen because the Users table is empty after installing.
                    echo $e;
                }

                Session::logIn(1);


                $user = Session::getUser();

                try {
                    $user->addRights(array("administrator","login_backend","edit_users","edit_pages","edit_settings","edit_media"));
                } catch (Exception $e) {
                    echo "Could not add admins rights: ".$e;
                }

                header('Location: /');
            }
        }
    }
}

if($error != "" || count($_POST) == 0) {
    include $_SERVER['DOCUMENT_ROOT'].'/setup/setupForm.php';
}