<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <title>Fastend initial Setup</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Fastend Initial Setup</h1>
            <p>Fastend does not seem to be installed yet. Please complete this configuration Form to connect Fastend to the Servers Database and create an Administrator User.</p>
            <?php
            if($error != "") {
                echo '<p class="text-danger">There where Errors in the Configuration:<br><ul class="text-danger">'.$error.'</ul></p>';
            }
            ?>
            <form action="/setup/setup.php" method="post" autocomplete="off">
                <h2>MySQL Database</h2>
                <div class="form-group">
                    <label for="mysql-server">MySQL Server</label>
                    <input class="form-control" type="text" id="mysql-server" name="mysql-server" placeholder="localhost" value="<?php if(isset($_POST['mysql-server'])) echo $_POST['mysql-server'];?>">
                </div>
                <div class="form-group">
                    <label for="mysql-database">MySQL Database</label>
                    <input class="form-control" type="text" id="mysql-database" name="mysql-database" placeholder="fastend-db" value="<?php if(isset($_POST['mysql-database'])) echo $_POST['mysql-database'];?>">
                </div>
                <div class="form-group">
                    <label for="mysql-username">MySQL User</label>
                    <input class="form-control" type="text" id="mysql-username" name="mysql-username" placeholder="fastend-db-user" value="<?php if(isset($_POST['mysql-username'])) echo $_POST['mysql-username'];?>">
                </div>
                <div class="form-group">
                    <label for="mysql-password">MySQL Password</label>
                    <input class="form-control" autocomplete="new-password" type="password" id="mysql-password" name="mysql-password" placeholder="fastend-db-password" value="<?php if(isset($_POST['mysql-password'])) echo $_POST['mysql-password'];?>">
                </div>
                <h2>Admin Data</h2>
                <div class="form-group">
                    <label for="admin-username">Administrator Username</label>
                    <input class="form-control" type="text" id="admin-username" name="admin-username" placeholder="Administrator Username" value="<?php if(isset($_POST['admin-username'])) echo $_POST['admin-username'];?>">
                </div>
                <div class="form-group">
                    <label for="admin-password">Administrator Password</label>
                    <input class="form-control" autocomplete="new-password" type="password" id="admin-password" name="admin-password" placeholder="Administrator Password" value="<?php if(isset($_POST['admin-password'])) echo $_POST['admin-password'];?>">
                </div>
                <div class="form-group">
                    <label for="admin-password-repeat">Repeat Password</label>
                    <input class="form-control" autocomplete="new-password" type="password" id="admin-password-repeat" name="admin-password-repeat" placeholder="Repeat password" value="<?php if(isset($_POST['admin-password-repeat'])) echo $_POST['admin-password-repeat'];?>">
                </div>
                <button class="btn btn-success w-100" type="submit">Configure Fastend</button>
            </form>
        </div>
    </div>
</div>
<?php  Fastend::GetScripts(); ?>
</body>
</html>