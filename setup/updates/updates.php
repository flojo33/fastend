<?php
/*
 * This File contains all required updates for each version.
 * Important: NEVER delete any elements in this array! Only add new elements to the End!
 */
$updates = array(
    function() {
        //echo 'Updating to Version 1.0.0.0<br>';
        Sql::executeCommandFast("CREATE TABLE `system` ( `id` VARCHAR(128) NOT NULL , `value` VARCHAR(1024) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('latest_update_id','0');");
    },
    function() {
        //echo 'This is a test message to show that running Updates actually works now!<br>';
    },
    function() {
        //echo '"Hello World" from new Updater!<br>';
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('standard_language_id','1');");
        //echo 'Added standard Language to system settings.<br>';
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `language` (`id`,`name`,`short_name`) VALUES (null,'English','EN');");
        //echo 'Added English as the standard language.<br>';
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `page` (`id`, `name`, `url_name`, `parent_id`, `visible_in_navigation`) VALUES (NULL, 'index', 'index', '0', '1');");
        Sql::executeCommandFast("INSERT INTO `page_content` (`page_id`, `language_id`, `content`) VALUES ('1', '1', '');");
        //echo 'Added Initial Index Page<br>';
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('index_page','1');");
        //echo 'Added English as the standard language.<br>';
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('site_name','My Fastend Page');");
        //echo 'Added Site name.<br>';
    },
    function() {
        Sql::executeCommandFast("CREATE TABLE `user` ( `id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(256) NOT NULL , `password_hash` VARCHAR(256) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        //echo 'Added User Table.<br>';
    },
    function() {
        Sql::executeCommandFast("CREATE TABLE `session_right` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(256) NOT NULL , `type` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        Sql::executeCommandFast("CREATE TABLE `user_session_right` ( `user_id` INT NOT NULL , `session_right_id` INT NOT NULL , PRIMARY KEY (`user_id`, `session_right_id`)) ENGINE = InnoDB;");
        Sql::executeCommandFast("ALTER TABLE `user_session_right` ADD CONSTRAINT `user_session_right_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        Sql::executeCommandFast("ALTER TABLE `user_session_right` ADD CONSTRAINT `user_session_right_session_right_id` FOREIGN KEY (`session_right_id`) REFERENCES `session_right`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'login_backend', '1')");
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'administrator', '1')");
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'edit_users', '1')");
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'edit_pages', '1')");
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'edit_settings', '1')");
    },
    function() {
        Sql::executeCommandFast("ALTER TABLE `page` ADD `order_id` INT NOT NULL DEFAULT 0 AFTER `visible_in_navigation`;");
    },
    function() {
        Sql::executeCommandFast("INSERT INTO `session_right` (`id`, `name`, `type`) VALUES (NULL, 'edit_media', '1')");
    },
    function() {
        Sql::executeCommandFast("CREATE TABLE `image` ( `id` VARCHAR(128) NOT NULL , `name` VARCHAR(128) NOT NULL , `description` VARCHAR(1024) NOT NULL , `upload_date` DATETIME NOT NULL , `location_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
    },
    function() {
        Sql::executeCommandFast("CREATE TABLE `image_crop` ( `image_id` VARCHAR(128) NOT NULL , `crop_id` INT NOT NULL , `creation_date` DATETIME NOT NULL , PRIMARY KEY (`image_id`,`crop_id`)) ENGINE = InnoDB;");
        Sql::executeCommandFast("ALTER TABLE `image_crop` ADD CONSTRAINT `image_crops_image_id` FOREIGN KEY (`image_id`) REFERENCES `image`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    },
    function() {
        Sql::executeCommandFast("CREATE TABLE `location` ( `id` INT NOT NULL , `name` VARCHAR(1024) NOT NULL , `parent` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
    }, function() {
        Sql::executeCommandFast("ALTER TABLE `location` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;");
    }, function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('selected_theme','default');");
    }, function() {
        Sql::executeCommandFast("RENAME TABLE `image` TO `file`;");
    }, function() {
        Sql::executeCommandFast("ALTER TABLE `file` ADD `type` VARCHAR(128) NOT NULL DEFAULT 'image' AFTER `location_id`, ADD `extension` VARCHAR(128) NOT NULL DEFAULT 'png' AFTER `type`;");
    }, function() {
        Sql::executeCommandFast("CREATE TABLE `mail_server` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(256) NOT NULL , `email` VARCHAR(256) NOT NULL , `username` VARCHAR(256) NOT NULL , `password` VARCHAR(256) NOT NULL , `server` VARCHAR(256) NOT NULL , `port` INT NOT NULL , `type` VARCHAR(256) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
    }, function() {
        Sql::executeCommandFast("ALTER TABLE `page_content` ADD `name` VARCHAR(256) NOT NULL DEFAULT '' AFTER `language_id`, ADD `description` VARCHAR(2048) NOT NULL DEFAULT 'Page Description' AFTER `name`;");
    }, function() {
        Sql::executeCommandFast("UPDATE `page_content`,`page` SET `page_content`.`name` = `page`.`name` WHERE `page_content`.`page_id` = `page`.`id`;");
    }, function() {
        Sql::executeCommandFast("ALTER TABLE `page` DROP `name`");
    }, function() {
        Sql::executeCommandFast("CREATE TABLE `footer_content` ( `language_id` INT NOT NULL , `content` TEXT NOT NULL ) ENGINE = InnoDB;");
        Sql::executeCommandFast("ALTER TABLE `footer_content` ADD CONSTRAINT `footer_content_language_id` FOREIGN KEY (`language_id`) REFERENCES `language`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }, function() {
        Sql::executeCommandFast("ALTER TABLE `page_content` CHANGE `content` `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;");
    }, function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('google_analytics_tag','');");
    }, function() {
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('navbar_simple','true');");
        Sql::executeCommandFast("INSERT INTO `system` (`id`,`value`) VALUES ('navbar_visible','true');");
    }, function() {
        Sql::executeCommandFast("CREATE TABLE `navbar_items` ( `id` INT NOT NULL , `parent_id` INT NOT NULL , `name` VARCHAR(1024) NOT NULL , `page_id` INT NOT NULL , `anchor` VARCHAR(1024) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
    }
);