<?php
$root = $_SERVER['DOCUMENT_ROOT'];
require_once($root. '/php/classes/Autoload.php');
$tempDir = $root.'/temp';
$tempZip = $tempDir.'download.zip';
$backupDir = $root."/backup";
$updateDir = $tempDir.'/update';
$mediaFolder = "media";
$mediaDir = $root."/".$mediaFolder;
echo 'Downloading Files...<br>';
$updateData = file_get_contents('https://gitlab.com/flojo33/fastend/-/archive/master/fastend-master.zip');
file_put_contents($tempZip,$updateData);
echo 'Unzipping...<br>';
$zip = new ZipArchive;
$res = $zip->open($tempZip);
$ok = false;
if ($res === TRUE) {
    $zip->extractTo($updateDir);
    $zip->close();
    $ok = true;
}
echo 'Copying Files...<br>';
if(file_exists($tempZip)){
    unlink($tempZip);
}
if(is_dir($backupDir)){
    Utilities::RemoveDirectory($backupDir);
}
mkdir($backupDir);
Utilities::CopyDirectory($root,$backupDir, array($backupDir, $tempDir, $mediaDir));
$subdirectories = scandir($root);
$keptFiles = array("backup", ".", "..", "temp", ".htaccess", "mysql-settings.php", "theme", $mediaFolder);
foreach ($subdirectories as $subdirectory) {
    if(in_array($subdirectory, $keptFiles)) continue;
    if(is_dir($root.'/'.$subdirectory)){
        Utilities::RemoveDirectory($root.'/'.$subdirectory);
    }else {
        unlink($root.'/'.$subdirectory);
    }
}
Utilities::CopyDirectory($updateDir.'/fastend-master',$root);
Utilities::RemoveDirectory($tempDir);

//Copy custom themes...
$subdirectories = scandir($backupDir.'/themes');
$ignoredThemes = array('.','..','default','sleek');

foreach ($subdirectories as $subdirectory) {
    if(in_array($subdirectory, $ignoredThemes)) continue;
    Utilities::CopyDirectory($backupDir.'/themes/'.$subdirectory, $root.'/themes/'.$subdirectory);
}

echo 'Installing Updates...<br>';
Utilities::RunUpdates();
echo 'Done!<br>';