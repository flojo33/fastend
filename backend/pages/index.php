<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(isset($_POST['order-data'])) {
    $parsed = json_decode($_POST['order-data'],true);
    foreach ($parsed as $key => $value) {
        Sql::executeCommand("UPDATE `page` SET `order_id` = ?, `parent_id` = ?, `visible_in_navigation` = ? WHERE `id` = ?;",'iiii',$value['index'],$value['parent'],$value['visibleInNavigation'],$key);
    }
}
if(isset($_POST['new-page-name'])) {
    $newPageName = $_POST['new-page-name'];
    $newPageDescription = $_POST['new-page-description'];
    $newUrlName = PageHandler::CreateUrlName($newPageName);
    $append = "";
    while(count(Sql::executeQuery('SELECT * FROM `page` WHERE `parent_id` = 0 AND `url_name` = ?','s',$newUrlName.$append)) > 0) {
        if($append == "") {
            $append = "-1";
        } else {
            $append = "-".(str_replace("-","",$append) + 1);
        }
    }
    Sql::executeCommand('INSERT INTO `page` (`id`,`url_name`,`parent_id`,`visible_in_navigation`,`order_id`) VALUES (NULL,?,0,0,0);','s',$newUrlName.$append);
    Sql::executeCommand('INSERT INTO `page_content` (`page_id`,`language_id`,`name`,`description`,`content`) VALUES (?,?,?,?,?);','iisss',Sql::insertId(),LanguageHandler::GetCurrentLanguage()->id,$newPageName,$newPageDescription,"");
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Website Pages</title>
    <link rel="stylesheet" href="/backend/css/sortable.css?dev">
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Pages</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm btn-outline-success" id="add-page-button"><i class="fa fa-plus"></i> Add Page</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>The pages of your website can be edited here. To change the ordering of the pages and create sub-pages simply drag and drop the desired page in the hierarchy. Attention: This is not supported on mobile devices! Please sort pages on desktops only.<br><span class="text-muted">Please note that a group of subpages may only contain pages with different urls!</span></p>
                    <form action="index.php" method="post" id="order-form">
                        <div class="page-order-edit">
                        <div class="d-flex flex-nowrap justify-content-between page-order-edit-header page-order-edit-element">
                            <div class="page-name">Page Name (In current language)</div>
                            <div class="page-visibility">Visible in Navigation</div>
                            <div class="page-url">Page Url</div>
                            <div class="page-edit">Edit</div>
                        </div>
                        <div class="sortable-container">
                            <ol class="sortable">
                                <?php
                                function RenderPageTable($page, $depth) {
                                    $visibleCheck = '
                                    <div class="custom-control custom-switch">
                                      <input type="checkbox" class="custom-control-input" '.($page->visibleInNavigation?"checked":"").' id="page-'.$page->id.'-visible">
                                      <label class="custom-control-label" for="page-'.$page->id.'-visible"></label>
                                    </div>
                                    ';
                                    echo '
                                    <li>
                                        <div class="content d-flex flex-nowrap justify-content-between page-order-edit-element" data-page-id="'.$page->id.'">
                                            <div class="page-name" style="width: '.(300-$depth*40).'px;">'.$page->name.'</div>
                                            <div class="page-visibility">'.$visibleCheck.'</div>
                                            <div class="page-url">'.$page->urlName.'</div>
                                            <div class="page-edit"><a class="btn btn-primary btn-sm" href="/backend/pages/edit?id='.$page->id.'"><i class="fa fa-edit"></i> Edit</a></div>
                                        </div>';

                                    if(count($page->children) > 0) {
                                        echo '<ol>';
                                        foreach ($page->children as $child) {
                                            RenderPageTable($child, $depth+1);
                                        }
                                        echo '</ol>';
                                    }
                                    echo '</li>';
                                }

                                $pages = PageHandler::getAllPages(LanguageHandler::GetCurrentLanguage());
                                foreach ($pages as $page) {
                                    RenderPageTable($page,0);
                                }
                                ?>
                            </ol>
                        </div>
                        </div>
                        <input type="hidden" name="order-data" id="order-data">
                        <button class="btn btn-success w-100" id="save-order-button" type="submit">Save Page Order</button>
                    </form>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="new-page-modal" tabindex="-1" role="dialog" aria-labelledby="new-page-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="new-page-modal-label">New Page</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="index.php" id="new-page-form" method="post">
                    <div class="form-group">
                        <label for="new-page-name">New Page Name</label>
                        <input type="text" class="form-control" name="new-page-name" id="new-page-name">
                    </div>
                    <div class="form-group">
                        <label for="new-page-description">New Page Description</label>
                        <textarea class="form-control" name="new-page-description" id="new-page-description" maxlength="2048"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success" id="save-new-page">Add Page</button>
            </div>
        </div>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
<script src="/backend/js/jquery-ui-sortable.min.js"></script>
<script src="/backend/js/jquery.mjs.nestedSortable.min.js"></script>
<script>
    $(document).ready(function(){
        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            placeholder: 'placeholder',
            maxLevels: 4,
            isAllowed: checkPlacement,
            relocate: function () {
                updateAllDepths();
            }
        });
        $('#save-order-button').click(function(e) {
            e.preventDefault();
            let newPageParents = {};
            $('.sortable').find('.content').each(function() {
                let thisId = $(this).attr('data-page-id');
                let parent = $(this).parent();
                let index = parent.index();
                let visible = $(this).find('.custom-control-input').is(":checked")?"1":"0";
                if(parent.parent().hasClass('sortable')) {
                    newPageParents[thisId] = {parent:0,index:index,visibleInNavigation:visible};
                } else {
                    let parentId = parseInt(parent.parent().parent().find(' > div').attr('data-page-id'));
                    newPageParents[thisId] = {parent:parentId,index:index,visibleInNavigation:visible};
                }
            });
            $('#order-data').val(JSON.stringify(newPageParents));
            $('#order-form').submit();
        });
    });

    let updateAllDepths = function() {
        updateDepths($('.sortable-container'),0);
    };

    let updateDepths = function(element, currentDepth) {
        element.find("> ol").find("> li:not(.placeholder):not(.ui-sortable-helper)").each(function() {
            console.log($(this)[0]);
            $(this).children('.content').children('.page-name').css("width",(300-currentDepth*40)+"px");
            updateDepths($(this),currentDepth+1);
        });
    };

    function checkPlacement(placeholder, placeholderParent, currentItem) {
        let currentDiv = currentItem.find('> .content');
        let currentUrl = currentDiv.find('.page-url').html();
        let ok = true;
        placeholder.parent().find('> li > .content').each(function() {
            if(!$(this).is(currentDiv)) {
                if($(this).find('.page-url').html() === currentUrl) {
                    ok = false;
                }
            }
        });
        return ok;
    }
    $('#new-page-modal').on('shown.bs.modal',function() {
        $('#new-page-name').focus();
    });
    $('#add-page-button').click(function() {
        $('#new-page-name').val("");
        $('#new-page-modal').modal('show');
    });
    $('#save-new-page').click(function() {
        $('#new-page-form').submit();
    });
</script>
</body>
</html>