<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if (!Session::isLoggedIn()) {
    header('Location: /backend/login');
    exit;
}
$pageDataQuery = Sql::executeQuery('SELECT * FROM `page` WHERE `id` = ?;', 'i', $_GET['id']);
if (count($pageDataQuery) != 1) {
    header('Location: /backend/rights');
    exit;
}
$pageData = $pageDataQuery[0];
if (isset($_GET['language'])) {
    $language = $_GET['language'];
} else {
    $language = Fastend::getSetting('standard_language_id');
}
$pageContentQuery = Sql::executeQuery('SELECT * FROM `page_content` WHERE `page_id` = ? AND `language_id` = ?;', 'ii', $_GET['id'], $language);
if (count($pageContentQuery) != 1) {
    $pageContent = "";
    //TODO add the content of the most relevant existing page when creating contents for new languages!!
} else {
    $pageContent = $pageContentQuery[0]['content'];
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/backend/elements/head.php'); ?>
    <link rel="stylesheet" href="/backend/jquery-cropper/cropper.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <style>
        .img-container img {
            max-width: 100%;
        }
    </style>
    <link rel="stylesheet" href="/backend/css/page-editor.css?<?php echo uniqid(); ?>">
    <link rel="stylesheet" href="/backend/quill/quill.bubble.css"/>
    <title>Edit Page "<?php echo $pageContentQuery[0]["name"]; ?>"</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/backend/elements/navigation.php'); ?>


<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-md-4 px-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Edit Page "<?php echo $pageContentQuery[0]['name']; ?>"</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button type="button" id="delete-page-button" class="btn btn-sm btn-outline-danger"><i
                                    class="fa fa-trash-alt"></i> Remove Page
                        </button>
                    </div>
                </div>
            </div>
            <div id="page-editor">
                <input type="hidden" id="page-id" value="<?php echo $_GET['id'];?>">
                <input type="hidden" id="language-id" value="<?php echo $language;?>">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="page-name">Page Name</label>
                            <input type="text" class="form-control" id="page-name"
                                   value="<?php echo $pageContentQuery[0]['name']; ?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="page-url">Page Url</label>
                            <input type="text" class="form-control" id="page-url"
                                   value="<?php echo $pageData['url_name']; ?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="page-description">Page Description <small class="text-muted">(<span id="current-description-length"></span>)</small></label>
                            <textarea type="text" class="form-control" maxlength="2048" id="page-description"><?php echo $pageContentQuery[0]['description']; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="main-bar-container fixed" id="main-toolbar">
                            <div class="inner-container py-1">
                                <div class="mx-1 mx-xl-2 text-center">
                                    <span class="d-none d-xl-block">Media Size</span>
                                    <div id="size-buttons"></div>
                                </div>
                                <div class="mx-1 mx-xl-2 text-center">
                                    <span class="d-none d-xl-block">Language</span>
                                    <div class="btn-group">
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="d-xl-none">EN</span><span class="d-none d-xl-inline-block">English</span>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">English</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx-1 mx-xl-2 text-center">
                                    <span class="d-none d-xl-block">Save</span>
                                    <div class="btn-group" role="group">
                                        <button class="btn btn-success btn-sm" type="button" id="save-button">
                                            <i class="fa fa-save"></i> <span class="d-none d-xl-inline-block">Save</span>
                                        </button>
                                        <button class="btn btn-outline-success btn-sm" type="button" id="save-view-button">
                                            <i class="fa fa-eye"></i> <span class="d-none d-xl-inline-block">View</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="edit-bar">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12 col-lg-6 block-container py-1 d-none d-md-block"></div>
                                        <div class="col-12 col-lg-6 module-container py-1 d-none d-md-block"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-page-content">
                    <div class="page-edit-container-parent">
                        <div class="page-edit-container">
                            <?php
                            $content = '' . $pageContent . '';
                            $theme = FastendTheme::GetSelectedTheme();
                            $theme->RenderPage($pageData['id'], $pageData['url_name'], $pageContentQuery[0]['name'], $pageContentQuery[0]['description'], $content, true);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
<script src="/backend/js/media-browser.js"></script>
<script src="/backend/jquery-cropper/cropper.min.js"></script>
<script src="/backend/jquery-cropper/jquery-cropper.min.js"></script>
<script src="/backend/quill/quill.js"></script>
<script>
    Quill.debug('error');
</script>
<script src="/backend/js/quill-extentions.js"></script>
<script type="module" src="/backend/js/page-editor.js"></script>
</body>
</html>