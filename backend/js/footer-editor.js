import { PageSizeController } from './modules/fastend/page-size';
import { DynamicToolbar } from './modules/fastend/interfaces/dynamic-toolbar';
import { BlockController } from "./modules/fastend/blocks/block-controller.js";
import { ModuleController } from "./modules/fastend/modules/module-controller";
import { Utilities } from "./modules/fastend/utilities.js";
import { InputLengthCounter } from "./modules/fastend/interfaces/input-length-counter";
import { PageEditorModuleToolbar } from "./modules/fastend/modules/module-toolbar";
import { PageEditorBlockToolbar } from "./modules/fastend/blocks/block-toolbar";

/**
 * The main container that holds all blocks.
 * @type {jQuery}
 */
const mainWindow = $('.page-edit-container');

/**
 * The inner Page content that can all be edited
 * @type {jQuery}
 */
const pageContent = mainWindow.find('.page-footer');

const mainToolbar = $('#main-toolbar');

//Initialize Size Buttons
let sizeButtons = new PageSizeController($('#size-buttons'), mainWindow, function() {
    //TODO reimplement this if necessary
    //clearSelection();
});
sizeButtons.setSelectedSize((mainWindow.parent().width() < 400)?0:3);

let moduleController = new ModuleController(mainWindow, function() {
    moduleToolbar.update();
    mainToolbar.checkTopScroll();
});

let blockController = new BlockController(mainWindow, pageContent, moduleController, function() {
    blockToolbar.update();
    mainToolbar.checkTopScroll();
});

let blockToolbar = new PageEditorBlockToolbar(blockController,$('.block-container'));
blockToolbar.update();

let moduleToolbar = new PageEditorModuleToolbar(moduleController,$('.module-container'),sizeButtons);
moduleToolbar.update();

DynamicToolbar.Initialize(mainToolbar);

let addBlockButton = $('<button class="btn btn-success btn-add-block editor-element"><i class="fa fa-plus"></i> Add Block</button>');
addBlockButton.on('click', function () {
    blockController.showSelectModal("atEnd");
});
addBlockButton.appendTo(pageContent.parent());

let languageIdInput = $('#language-id');

let save = function (view) {
    //Deselect all currently selected elements
    moduleController.deselectModule();
    blockController.deselectBlock();

    //Clone the entire page content
    let clone = pageContent.clone();

    //Destroy all unnecessary data on the modules
    clone.find('.module').each(function () {
        moduleController.prepareForSaving($(this));
    });

    //Destroy all unnecessary data on the blocks
    clone.find('.block').each(function () {
        blockController.prepareForSaving($(this));
    });

    //Clean up the generated code to have nicer spacing.
    Utilities.cleanElementCode(clone);

    let newContent = clone.html().replace("\n", "");
    clone.remove();

    let request = $.ajax({
        url: "/backend/ajax/save-footer.php",
        method: "POST",
        data: {
            languageId: languageIdInput.val(),
            content: newContent
        }
    });
    /**
     * @param data.pageName
     * @param data.pageUrl
     * @param data.reason
     * @param data.urlErrors
     * @param data.fullPageLink
     */
    request.done(function (data) {
        if (data.status !== "success") {
            alert("Could not save Footer: " + data.reason);
        } else {
            if (view) {
                window.location.href = '/';
            }
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Saving failed: " + textStatus);
    });
};

$('#save-button').on("click", function () {
    save(false);
});
$('#save-view-button').on("click", function () {
    save(true);
});