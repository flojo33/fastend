/*
This is a placeholder file to mute errors in PhPStorm. Ignore.
 */
class Quill {
    constructor(moduleElement, options) {

    }

    /**
     * @param name string
     * @return Toolbar
     */
    getModule(name) {
        return new Toolbar();
    }

    getFormat() {

    }

    format() {}

    setSelection(param, SILENT) {
        
    }

    insertText(index, s, lineFormats, USER) {
        
    }

    formatLine(param, number, formattingChanges, USER) {
        
    }
}
class Toolbar {

    constructor() {

    }

    addHandler(name, handler) {

    }
}

class jQuery {
    each(fn) {}
    modal(fn) {}
}