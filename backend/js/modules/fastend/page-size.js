const ScreenSizes = [
    {
        short:"XS",
        long:"Extra Small",
        editorSize: {
            width:"calc(100% - 8px)",
            minWidth:"0px",
            maxWidth:"350px",
            fullWidth: false
        },
        minimumColumnSize: 6,
        classPrefix: ""
    },
    {
        short:"S",
        long:"Small",
        editorSize: {
            width:"576px",
            minWidth:"576px",
            maxWidth:"576px",
            fullWidth: false
        },
        minimumColumnSize: 4,
        classPrefix: "-sm"
    },
    {
        short:"M",
        long:"Medium",
        editorSize: {
            width:"768px",
            minWidth:"768px",
            maxWidth:"768px",
            fullWidth: false
        },
        minimumColumnSize: 3,
        classPrefix: "-md"
    },
    {
        short:"L",
        long:"Large",
        editorSize: {
            width:"992px",
            minWidth:"992px",
            maxWidth:"992px",
            fullWidth: false
        },
        minimumColumnSize: 1,
        classPrefix: "-lg"
    },
    {
        short:"XL",
        long:"Extra Large",
        editorSize: {
            width:"1142px",
            minWidth:"1142px",
            maxWidth:"1142px",
            fullWidth: false
        },
        minimumColumnSize: 1,
        classPrefix: "-xl"
    },
    {
        short:"VW",
        long:"Full Width",
        editorSize: {
            width:"calc(100% - 8px)",
            minWidth:"1142px",
            maxWidth:"calc(100% - 8px)",
            fullWidth: true
        },
        minimumColumnSize: 1,
        classPrefix: "-xl"
    }
];

class PageSizeController {
    /**
     * Creates a size buttons selection that changes classes of the given controllingElement to
     * resize an editor window
     * @param {jQuery} element The buttongroup will be placed inside this element
     * @param {jQuery} controllingElement the buttongroup will control this elements size
     * @param {function} onChangeFunction function that is called when the selected size changes.
     */
    constructor(element, controllingElement, onChangeFunction) {



        this.onChangeFunction = onChangeFunction;
        this.controllingElement = controllingElement;
        this.buttonGroup = $('<div class="btn-group" role="group">');

        this.buttons = [];

        for(let i = 0; i < ScreenSizes.length; i++) {
            let screenSize = ScreenSizes[i];
            this.buttons[i] = PageSizeController.createButton(screenSize.short,screenSize.long);
            this.buttonGroup.append(this.buttons[i]);
        }

        for(let i = 0; i < ScreenSizes.length; i++) {
            this.buttons[i].on('click', () => {
                this.setSelectedSize(i);
            });
        }
        this.setSelectedSize(0);

        element.append(this.buttonGroup);
    }

    setSelectedSize(activeSize) {
        this.selectedSize = activeSize;
        let screenSize = ScreenSizes[activeSize];
        //Set the size of the main window
        for (let i = 0; i < ScreenSizes.length; i++) {
            this.controllingElement.removeClass("size-" + i);
        }
        this.controllingElement.parent().toggleClass("full-width",screenSize.editorSize.fullWidth);
        this.controllingElement.addClass("size-" + activeSize);
        this.controllingElement.css('max-width', screenSize.editorSize.maxWidth)
            .css('width', screenSize.editorSize.width)
            .css('min-width',screenSize.editorSize.minWidth);

        for(let i = 0; i < ScreenSizes.length; i++) {
            this.buttons[i].toggleClass("btn-success",activeSize===i);
            this.buttons[i].toggleClass("btn-secondary",activeSize!==i);
        }

        this.controllingElement.attr("data-current-size", activeSize);
        this.onChangeFunction();
        this.buttons[activeSize].blur();
    }

    getCurrentSize() {
        return this.selectedSize;
    }


    /**
     * Creates a button jQuery object for the Size select input.
     * @param {string} nameShort
     * @param {string} nameLong
     * @returns {jQuery}
     */
    static createButton(nameShort, nameLong) {
        return $([
            '<button type="button" class="btn btn-secondary btn-sm screen-size-select-button">',
            '  <span class="d-xl-none">'+nameShort+'</span>',
            '  <span class="d-none d-xl-block">'+nameLong+'</span>',
            '</button>'
        ].join('\n'));
    }
}

export {ScreenSizes, PageSizeController}