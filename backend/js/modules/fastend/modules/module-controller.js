import * as ModuleType from './module-types.js';
import { Modal } from '../interfaces/modal';
import {ModuleBase} from "./module-types.js";

class ModuleController {
    /**
     *
     * @param {jQuery} parentContainer
     * @param {function} onSelectionChange
     */
    constructor(parentContainer, onSelectionChange) {
        this.onSelectionChange = onSelectionChange;
        this.typeClasses = {};
        /**
         *
         * @type {Module|null}
         */
        this.selectedModule = null;
        this.parentContainer = parentContainer;
        this.modules = [];
        let controller = this;

        let modalContent = $('<div class="module-select">');

        for(let type in ModuleType) {
            let className = 'module-'+ModuleType[type].getType();
            this.typeClasses[className] = type;
            if(ModuleType[type].getBuildable()) {
                let addButton = $('<button class="btn btn-success w-100 mb-2">'+ModuleType[type].getName()+'<br><small>'+ModuleType[type].getDescription()+'</small></button>');
                addButton.on('click',() => this.finishModuleSelection(ModuleType[type].getType()));
                modalContent.append(addButton);
            }
            ModuleType[type].setupInitial();
        }

        this.selectModal = new Modal("moduleSelectModal")
            .setTitle("Select Module")
            .setContent(modalContent);

        parentContainer.parent().on("click", function (e) {
            let target = $(e.target);
            if(!target.parents('.row-editable .col').length && !(target.hasClass('col') && target.parent().hasClass('row-editable'))) {
                controller.deselectModule();
            }
        });
    };

    finishModuleSelection(type) {
        this.selectModal.hide();
        if(this.selectedModule === null) return;
        this.selectedModule.column.replaceModule(type);
    }

    showSelectModal(insertionType) {
        this.selectModal.show();
    }

    addModule(module) {
        this.modules.push(module);
    }

    deselectModule() {
        if(this.selectedModule !== null) {
            this.selectedModule.onDeselect();
            this.selectedModule = null;
            this.onSelectionChange();
        }
    }

    setSelectedModule(module) {
        if(this.selectedModule === module) return;
        if(this.selectedModule !== null) {
            this.selectedModule.onDeselect();
        }
        this.selectedModule = module;
        if(this.selectedModule === null) return;
        module.onSelect();
        this.onSelectionChange();
    }

    prepareForSaving(moduleClone) {
        let controller = this;
        for(let classKey in controller.typeClasses) {
            if(moduleClone.hasClass(classKey)) {
                ModuleType[controller.typeClasses[classKey]].prepareForSaving(moduleClone);
            }
        }
    }

    getSelected() {
        return this.selectedModule;
    }

    /**
     *
     * @param {Column} column
     * @param {boolean} initial
     * @returns {null}
     */
    initializeExisting(column, initial) {
        let controller = this;
        let module = null;
        column.element.find('.module:not(.setup)').each(function() {
            module = controller.createModuleWithClass($(this), column, initial);
        });
        return module;
    }

    createModuleWithClass(element, column, initial) {
        let controller = this;
        let classes = element[0].className;
        if(classes === "module") {
            element.addClass("module-base");
            //Create the select module only!
            return new ModuleBase(column, element, true);
        } else {
            for(let classKey in controller.typeClasses) {
                if(element.hasClass(classKey)) {
                    return new ModuleType[controller.typeClasses[classKey]](column, element, initial);
                }
            }
            console.warn("An invalid or unknown module type was found! Cannot initialize it.");
            return null;
        }
    }
}

export { ModuleController }