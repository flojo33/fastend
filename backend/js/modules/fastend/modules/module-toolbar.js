import {PageEditorToolbar} from "../interfaces/page-editor-toolbar";
import {ModalAlert} from "../interfaces/modal-alert";
import {Modal} from "../interfaces/modal";
import {PillTabContainer} from "../interfaces/pill-tab-container";
import {FormUtilities} from "../interfaces/form-utilities";
import {ScreenSizes} from "../page-size";

export class PageEditorModuleToolbar {
    /**
     *
     * @param {ModuleController} controller
     * @param container
     * @param {PageSizeController} pageSizeController
     */
    constructor(controller, container, pageSizeController) {
        this.controller = controller;
        this.container = container;
        this.element = $('<div class="row justify-content-center">');

        let editModalTabs = new PillTabContainer('module-edit-modal');
        let moduleTab = editModalTabs.addTab("Module");
        let sizeTab = editModalTabs.addTab("Size");
        let orderTab = editModalTabs.addTab("Order");
        let spacingTab = editModalTabs.addTab("Spacing");

        let editModal = new Modal("moduleEditModal").setTitle("Edit Module settings").setContent(editModalTabs.element);

        this.buttons = [];

        this.buttons.push(PageEditorToolbar.createButton('Increase Module Size',$('<i class="fa fa-expand-arrows-alt"></i>'),function() {
            controller.getSelected().column.increaseSize(pageSizeController);
        }));

        this.buttons.push(PageEditorToolbar.createButton('Decrease Module Size',$('<i class="fa fa-compress-arrows-alt"></i>'),function() {
            controller.getSelected().column.decreaseSize(pageSizeController);
        }));

        this.buttons.push(PageEditorToolbar.createButton('Move Module left',$('<i class="fa fa-chevron-left"></i>'),function() {
            controller.getSelected().column.switchLeft();
        }));

        this.buttons.push(PageEditorToolbar.createButton('Move Module right',$('<i class="fa fa-chevron-right"></i>'),function() {
            controller.getSelected().column.switchRight();
        }));

        this.buttons.push(PageEditorToolbar.createButton('Add Module', $('<i class="fa fa-plus"></i>'),function() {
            controller.getSelected().column.insertNew();
        }));

        this.buttons.push(PageEditorToolbar.createButton('Remove Module', $('<i class="fa fa-trash-alt"></i>'),function() {
            ModalAlert.confirm("Remove Module","Do you really want to remove this module?",function() {
                controller.getSelected().column.remove();
            }, () => {});
        }));

        this.buttons.push(PageEditorToolbar.createButton('Edit Module', $('<i class="fa fa-cog"></i>'),function() {
            let module = controller.getSelected();
            let column = module.element.parent(".col");

            //Module Tab
            moduleTab.html("");
            moduleTab.append(module.getEditModalContent());

            //Size Tab
            sizeTab.html("");
            let sizeTable = $('<table class="table table-borderless table-sm"><tr><th>Screen Size</th><th>Module Width</th></tr></table>');
            let sizeSelects = [];
            for (let i = 0; i < ScreenSizes.length-1; i++) {
                if (i !== 0) {
                    $('<tr><td></td><td class="text-center"><i class="fa fa-arrow-down"></i></td></tr>').appendTo(sizeTable);
                }
                let select = $('<tr><td>' + ScreenSizes[i].long + '</td><td class="text-center"><select class="form-control"></select></td></tr>');
                let selectInput = select.find("select");
                selectInput.on("change", function () {
                    let val = parseInt($(this).val());
                    for (let j = 1; j <= 12; j++) {
                        column.toggleClass('col' + ScreenSizes[i].classPrefix + "-" + j, val === j);
                    }
                    updateSelects();
                });
                sizeSelects.push(selectInput);
                select.appendTo(sizeTable);
            }

            let updateSelects = function () {
                for (let i = 0; i < sizeSelects.length; i++) {
                    sizeSelects[i].html("");
                    let selected = 0;
                    for (let j = 1; j <= 12; j++) {
                        if (column.hasClass('col' + ScreenSizes[i].classPrefix + "-" + j)) {
                            selected = j;
                        }
                    }
                    for (let j = i !== 0 ? 0 : 1; j <= 12; j++) {
                        let name;
                        if (j === 0) {
                            let size = module.column.findRelevantSizeClass(i - 1);
                            name = 'Inherit (' + size + ')';
                        } else {
                            name = j;
                        }
                        let option = $('<option value="' + j + '">' + name + '</option>');
                        option.prop("selected", selected === j);
                        option.appendTo(sizeSelects[i]);
                    }
                }
            };

            updateSelects();
            sizeTab.append(sizeTable);

            //Order Tab
            orderTab.html("");
            let orderTable = $('<table class="table table-borderless table-sm"><tr><th>Screen Size</th><th>Desired Position</th></tr></table>');
            let orderSelects = [];
            for (let i = 0; i < ScreenSizes.length-1; i++) {
                if (i !== 0) {
                    orderTable.append($('<tr><td></td><td class="text-center"><i class="fa fa-arrow-down"></i></td></tr>'));
                }
                let select = $('<tr><td>' + ScreenSizes[i].long + '</td><td class="text-center"><select class="form-control"></select></td></tr>');
                let selectInput = select.find("select");
                selectInput.on("change", function () {
                    let val = parseInt($(this).val());
                    for (let j = 1; j <= 12; j++) {
                        column.toggleClass('order' + ScreenSizes[i].classPrefix + "-" + j, val === j);
                    }
                    updateOrderSelects();
                });
                orderSelects.push(selectInput);
                select.appendTo(orderTable);
            }

            let updateOrderSelects = function () {
                for (let i = 0; i < orderSelects.length; i++) {
                    orderSelects[i].html("");
                    let selected = 0;
                    for (let j = 1; j <= 12; j++) {
                        if (column.hasClass('order' + ScreenSizes[i].classPrefix + "-" + j)) {
                            selected = j;
                        }
                    }
                    for (let j = 0; j <= 12; j++) {
                        let name;
                        if (j === 0) {
                            let size = module.column.findRelevantOrderClass(i - 1);
                            if (size === 0) {
                                size = "Layout";
                            }
                            name = 'Inherit (' + size + ')';
                        } else {
                            name = j;
                        }
                        let option = $('<option value="' + j + '">' + name + '</option>');
                        option.prop("selected", selected === j);
                        option.appendTo(orderSelects[i]);
                    }
                }
            };

            updateOrderSelects();
            orderTable.appendTo(orderTab);

            //Spacing Tab
            spacingTab.html("");
            spacingTab.append(FormUtilities.createClassSelectInput("Margin Top", column, "mt-", ["0", "1", "2", "3", "4", "5"], ["0", "1", "2", "3", "4", "5"], 0));
            spacingTab.append(FormUtilities.createClassSelectInput("Margin Bottom", column, "mb-", ["0", "1", "2", "3", "4", "5"], ["0", "1", "2", "3", "4", "5"], 0));

            //Finally, show the modal
            editModal.show();
        }));

        for(let i = 0; i < this.buttons.length; i++) {
            this.element.append(this.buttons[i]);
        }

        container.append(this.element);
    }

    update() {
        let active = this.controller.getSelected() !== null;
        for(let i = 0; i < this.buttons.length; i++) {
            this.buttons[i].prop("disabled", !active);
        }
        this.container.toggleClass("d-block", active);
    }
}