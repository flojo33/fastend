import {Module} from "../module";
import {ImageSelector} from "../../interfaces/image-selector";

export class ModuleSlideshow extends Module {

    static getName() {
        return "Slideshow";
    }

    static  getDescription() {
        return "Showas a slideshow of multiple items";
    }

    static getType() {
        return "slideshow";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        this.onDeselect();
    }

    getEditModalContent() {
        let self = this;
        let carousel = this.element.find(".carousel");
        let id = carousel.attr("id");
        let content = $('<div class="module-content-edit>"></div>');
        let elementContainer = $('<div class="slideshow-element-container"></div>');
        let element = this.element;
        let updateInputs = function() {
            element.find(".carousel-item:not(.setup)").each(function() {
                $(this).addClass("setup");
                //Image
                let element = $('<div class="slideshow-element"><div class="container-fluid"><div class="row"><div class="col-2 p-0 image-container"></div><div class="col-7 px-1 caption-inputs"></div><div class="col-3 px-0 edit-buttons"></div></div></div></div>');
                let img = $(this).find("img");
                let previewImage = img.clone();
                previewImage.addClass("w-100");
                previewImage.appendTo(element.find(".image-container"));

                previewImage.on('click',function() {
                    ImageSelector.show(1200,(type, id, cropId, raw) => {
                        if (type === "image") {
                            img.attr("src", raw);
                            previewImage.attr("src", raw);
                            img.attr("data-image", id);
                            img.attr("data-crop", cropId);
                            img.removeAttr("data-vector");
                        } else {
                            img.attr("src", "/media/uploads/vector/" + id + ".svg");
                            previewImage.attr("src", "/media/uploads/vector/" + id + ".svg");
                            img.removeAttr("data-image");
                            img.removeAttr("data-crop");
                            img.attr("data-vector", id);
                        }
                    })
                });

                //Inputs
                let title = $(this).find('.carousel-caption h5');
                let caption = $(this).find('.carousel-caption p');
                let titleInput = $('<input type="text" class="form-control">');
                titleInput.val(title.html().trim());
                let captionInput = $('<input type="text" class="form-control">');
                captionInput.val(caption.html().trim());

                captionInput.on('input',function() {
                    caption.html(captionInput.val());
                });
                titleInput.on('input',function() {
                    title.html(titleInput.val());
                });

                titleInput.appendTo(element.find(".caption-inputs"));
                captionInput.appendTo(element.find(".caption-inputs"));

                //Buttons
                let deleteButton = $('<button class="btn btn-danger"><i class="fa fa-trash-alt"></i></button>');

                let upDownContainer = $('<div class="up-down-container"></div>');

                let upButton = $('<button class="btn btn-primary"><i class="fa fa-arrow-up"></i></button>');
                let downButton = $('<button class="btn btn-primary"><i class="fa fa-arrow-down"></i></button>');

                let buttonContainer = element.find('.edit-buttons');
                deleteButton.appendTo(buttonContainer);
                upButton.appendTo(upDownContainer);
                downButton.appendTo(upDownContainer);
                upDownContainer.appendTo(buttonContainer);

                element.appendTo(elementContainer);
            });
        };
        updateInputs();

        let heightFormGroup = $('<div class="form-group"><label for="slideshow-height">Slideshow Height (Pixels)</label></div>');
        let heightInput = $('<input type="number" class="form-control" id="slideshow-height">');
        heightInput.on("input", function () {
            element.find(".carousel-inner").css("height",$(this).val()+"px");
        });
        heightInput.appendTo(heightFormGroup);
        heightFormGroup.appendTo(content);

        let initialHeight = this.element.find(".carousel-inner").css("height").replace("px", "");
        heightInput.val(initialHeight);

        let addImageButton = $('<button class="btn btn-success w-100 mb-2"><i class="fa fa-plus"></i> Add Images</button>');
        addImageButton.appendTo(content);
        addImageButton.on("click",function() {
            self.addItem();
            updateInputs();
        });
        elementContainer.appendTo(content);
        return content;
    }


    addItem() {
        let html = [
            '<div class="carousel-item"><div class="carousel-image-container">',
            '   <img src="/backend/icons/image_placeholder.png" class="d-block" alt="empty-slide"></div>',
            '   <div class="carousel-caption d-none d-md-block">',
            '       <h5>Slide label</h5>',
            '       <p>Slide Caption</p>',
            '   </div>',
            '</div>'
        ].join("\n");

        this.element.find('.carousel-inner').append(html);
        let indicator = '<li data-target="#'+this.element.find('.carousel').attr("id")+'" data-slide-to="0" class="active"></li>';
        this.element.find('.carousel-indicators').append(indicator);
        ModuleSlideshow.updateIndicators(this.element);
    };

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        let baseId = 'carousel-';
        let currentId = 1;
        while($('#'+baseId+currentId).length) {
            currentId ++;
        }

        let id = baseId+currentId;
        console.log(id);
        this.element.html([
            '<div id="'+id+'" class="carousel slide" data-ride="carousel" data-pause="true">',
            '  <ol class="carousel-indicators"></ol>',
            '  <div class="carousel-inner" style="height: 300px;"></div>',
            '  <a class="carousel-control-prev" href="#'+id+'" role="button" data-slide="prev">',
            '    <span class="carousel-control-prev-icon" aria-hidden="true"></span>',
            '    <span class="sr-only">Previous</span>',
            '  </a>',
            '  <a class="carousel-control-next" href="#'+id+'" role="button" data-slide="next">',
            '    <span class="carousel-control-next-icon" aria-hidden="true"></span>',
            '    <span class="sr-only">Next</span>',
            '  </a>',
            '</div>'].join('\n'));
        this.addItem();
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        module.find("img").each(function () {
            if ($(this).attr("data-vector") !== undefined) {
                $(this).attr("src",'/media/uploads/vector/' +  $(this).attr("data-vector") + '.svg');
            } else {
                if ($(this).attr("data-image") !== undefined) {
                    if ($(this).attr("data-crop") !== undefined) {
                        $(this).attr("src",'/media/uploads/image/crop_' + $(this).attr("data-image") + '_' + $(this).attr("data-crop") + '.png');
                    } else {
                        $(this).attr("src",'/media/uploads/image/' + $(this).attr("data-image") + '.png');
                    }
                } else {
                    $(this).attr("src","/backend/icons/image_placeholder.png");
                }
            }
        });
        module.find('.setup').each(function() {
            $(this).removeClass('setup');
        });
        this.updateIndicators(module);
    }

    static updateIndicators(module) {
        module.find('.carousel-indicators').children().each(function(id) {
            $(this).attr('data-slide-to',id).toggleClass("active",id===0);
        });
        module.find('.carousel-inner').children().each(function(id) {
            $(this).toggleClass("active",id===0);
        });
    }
}