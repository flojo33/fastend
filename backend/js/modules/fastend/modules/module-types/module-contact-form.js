import {Module} from "../module";
import {Modal} from "../../interfaces/modal";
import {TextEditor} from "../../interfaces/text-editor/text-editor";

export class ModuleContactForm extends Module {

    static getName() {
        return "Contact Form";
    }

    static  getDescription() {
        return "Creates a contact form with custom inputs";
    }

    static getType() {
        return "contact-form";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {
        this.contactFormModuleElementEditorModal = new Modal('contactFormModuleElementModal').setTitle('Setup Form Input');
    }

    constructor(column, element, initial) {
        super(column, element, initial);
        new TextEditor(this.element.find(".title-text-editor"),column.row.controller.parentContainer);
        let form = element.find(".custom-contact-form");
        if (element.find(".custom-contact-form").attr("id") === undefined) {
            let currentMaxId = 0;
            column.row.controller.parentContainer.find(".custom-contact-form").each(function () {
                if (!form.is($(this))) {
                    currentMaxId = Math.max(currentMaxId, parseInt($(this).find(".form-id").val()));
                }
            });
            let id = currentMaxId + 1;
            let formSuccessText = $('<div class="alert alert-success form-success-text">Your information has been sent successfully! Please check your emails for a confirmation!</div>');
            let formErrorText = $('<div class="alert alert-danger form-error-text">An error occurred. Please check all inputs. The fields marked with * must be filled!</div>');
            let formReceiverText = $('<div class="form-receiver-text">A new Contact form request has been sent:</div>');
            let formSenderText = $('<div class="form-sender-text">Thank you for your request! We have received the following information from you:</div>');
            let formMailTitleText = $('<div class="form-mail-title-text">Contact Form Confirmation</div>');
            let formIdInput = $('<input class="form-control form-control-hidden form-id" type="hidden" name="form-' + id + '-id">');
            let captchaResponseInput = $('<input class="form-control form-control-hidden captcha-response" type="hidden" name="form-' + id + '-captcha-response">');
            let formReceiverInput = $('<input class="form-control form-control-hidden form-receiver" type="hidden" name="form-' + id + '-receiver">');
            let inputEmail = $('<div class="form-group form-group-email required fixed main-mail"><label for="form-' + id + '-email">E-Mail Address</label><input class="form-control input-element" type="email" id="form-' + id + '-email" name="form-' + id + '-email"></div>');
            let inputMessage = $('<div class="form-group form-group-textarea required"><label for="form-' + id + '-custom-element-1">Message</label><textarea rows="3" class="form-control input-element" id="form-' + id + '-custom-element-1" name="form-' + id + '-custom-element-1"></textarea></div>');
            let captchaContainer = $('<div class="captchaContainer"></div>');
            renderCaptcha(captchaContainer);
            let submitButton = $('<button class="btn btn-success btn-submit" type="submit">Submit</button>');
            form.attr("id", "custom-form-" + id);
            formIdInput.val(id);
            captchaResponseInput.appendTo(form);
            formMailTitleText.appendTo(form);
            formSuccessText.appendTo(form);
            formErrorText.appendTo(form);
            formReceiverText.appendTo(form);
            formSenderText.appendTo(form);
            formIdInput.appendTo(form);
            formReceiverInput.appendTo(form);
            inputEmail.appendTo(form);
            inputMessage.appendTo(form);
            captchaContainer.appendTo(form);
            submitButton.appendTo(form);
        }
        let module = this;
        form.find(".form-group").each(function () {
            module.configureFormGroup(form, $(this));
        });
        this.onDeselect();
    }

    configureFormGroup(form, formGroup) {
        let module = this;
        let editControls = $('<div class="edit-controls"></div>');
        let editButton = $('<button class="btn btn-primary btn-sm editor-element" type="button"><i class="fa fa-edit"></i></button>');
        editButton.on("click", function () {
            module.showEditFormElementModal(form, formGroup);
        });
        editButton.appendTo(editControls);
        let moveUpButton = $('<button class="btn btn-secondary btn-sm editor-element" type="button"><i class="fa fa-arrow-up"></i></button>');
        moveUpButton.on('click', function () {
            let prev = formGroup.prev();
            if (prev.hasClass("form-group") && !prev.hasClass("form-group-hidden")) {
                formGroup.insertBefore(prev);
            }
        });
        moveUpButton.appendTo(editControls);
        let moveDownButton = $('<button class="btn btn-secondary btn-sm editor-element" type="button"><i class="fa fa-arrow-down"></i></button>');
        moveDownButton.on('click', function () {
            let next = formGroup.next();
            if (next.hasClass("form-group") && !next.hasClass("form-group-hidden")) {
                formGroup.insertAfter(next);
            }
        });
        moveDownButton.appendTo(editControls);
        let addButton = $('<button class="btn btn-success btn-sm editor-element" type="button"><i class="fa fa-plus"></i></button>');
        addButton.on("click", function () {
            module.addFormElement(form, formGroup);
        });
        addButton.appendTo(editControls);
        if (!formGroup.hasClass("fixed")) {
            let deleteButton = $('<button class="btn btn-danger btn-sm editor-element" type="button"><i class="fa fa-trash-alt"></i></button>');
            deleteButton.on("click", function () {
                formGroup.remove();
            });
            deleteButton.appendTo(editControls);
        }

        editControls.appendTo(formGroup);
    };

    /**
     *
     * @param {jQuery} form
     * @param {jQuery} sender
     */
    addFormElement (form, sender) {
        let id = form.attr("id").replace("custom-form-", "");
        let newName = "custom-element-";
        let ending = "1";
        while (form.find("#form-" + id + "-" + newName + ending).length !== 0) {
            ending = (parseInt(ending) + 1) + "";
        }
        newName = newName + ending;
        let newInput = $('<div class="form-group form-group-text"><label for="form-' + id + '-' + newName + '">New Form input</label><input class="form-control input-element" type="text" id="form-' + id + '-' + newName + '" name="form-' + id + '-' + newName + '"></div>');
        this.configureFormGroup(form, newInput);
        if (sender !== undefined) {
            newInput.insertAfter(sender);
        } else {
            newInput.insertBefore(form.find("button.btn.btn-submit"));
        }
    };

    /**
     *
     * @param {jQuery} form
     * @param {jQuery} formGroup
     */
    showEditFormElementModal(form, formGroup) {
        let module = this;
        let body = $('<div>');

        let fixed = false;
        let formType = "unknown";
        let values = formGroup[0].classList;
        let required = false;
        for (let i = 0; i < values.length; i++) {
            let classAttribute = values[i];
            if (classAttribute === "hidden") {
                console.error("This form-group should not be editable!")
            }
            if (classAttribute === "fixed") {
                fixed = true;
                required = true;
            }
            if (classAttribute === "required") {
                required = true;
            }
            if (classAttribute.startsWith("form-group-")) {
                formType = classAttribute.replace("form-group-", "");
            }
        }
        let nameInputGroup = $('<div class="form-group"><label for="input-name">Input Name</label><input type="text" class="form-control"></div>');
        let nameInput = nameInputGroup.find(".form-control");
        nameInput.val(formGroup.find("label").html());
        nameInput.on("change", function () {
            formGroup.find("label").html($(this).val());
        });
        nameInputGroup.appendTo(body);

        let requiredCheckbox = $('<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input"' + (fixed ? " disabled" : "") + (required ? " checked" : "") + ' id="form-group-required-switch"><label class="custom-control-label" for="form-group-required-switch">Required</label></div>');
        requiredCheckbox.find(".custom-control-input").on("change", function () {
            formGroup.toggleClass("required", $(this).is(":checked"))
        });
        requiredCheckbox.appendTo(body);

        let formInputTypeSelectGroup = $('<div class="form-group"><label for="input-type-select">Input Type</label></div>');
        let formInputTypeSelect = $('<select' + (fixed ? " disabled" : "") + ' id="input-type-select" class="form-control"></select>');
        let formInputTypes = ["email", "text", "textarea", "checkbox", "select"];
        let formInputNames = ["E-Mail", "Simple Text", "Long Text", "Checkbox", "Selection"];
        for (let i = 0; i < formInputTypes.length; i++) {
            let currentType = formInputTypes[i];
            let currentName = formInputNames[i];
            let selected = formType === currentType ? " selected" : "";
            $('<option value="' + currentType + '"' + selected + '>' + currentName + '</option>').appendTo(formInputTypeSelect);
        }
        formInputTypeSelect.appendTo(formInputTypeSelectGroup);
        formInputTypeSelectGroup.appendTo(body);
        formInputTypeSelect.on('change', function () {
            let selectedType = formInputTypeSelect.val();
            formGroup.removeClass("form-group-" + formType);
            formType = selectedType;
            let currentFormControl = formGroup.find('.input-element');
            let currentId = currentFormControl.attr("id");
            let currentName = formGroup.find('label').html();
            formGroup.html("");
            switch (formType) {
                case "text":
                    $('<label for="' + currentId + '">' + currentName + '</label>').appendTo(formGroup);
                    $('<input class="form-control input-element" name="' + currentId + '" id="' + currentId + '" type="text">').appendTo(formGroup);
                    break;
                case "textarea":
                    $('<label for="' + currentId + '">' + currentName + '</label>').appendTo(formGroup);
                    $('<textarea class="form-control input-element" rows="3" name="' + currentId + '" id="' + currentId + '" type="text"></textarea>').appendTo(formGroup);
                    break;
                case "select":
                    $('<label for="' + currentId + '">' + currentName + '</label>').appendTo(formGroup);
                    $('<select class="form-control input-element" name="' + currentId + '" id="' + currentId + '" type="text"><option value="" selected class="default"></option></select>').appendTo(formGroup);
                    break;
                case "checkbox":
                    $('<div class="custom-control custom-checkbox"><input type="checkbox" class="input-element custom-control-input" name="' + currentId + '" id="' + currentId + '"><label class="custom-control-label" for="' + currentId + '">' + currentName + '</label></div>').appendTo(formGroup);
                    break;
                case "email":
                    $('<label for="' + currentId + '">' + currentName + '</label>').appendTo(formGroup);
                    $('<input class="form-control input-element" name="' + currentId + '" id="' + currentId + '" type="email">').appendTo(formGroup);
                    break;
                default:
                    console.error("Unknown Form Control Type " + formType + "!");
                    break;
            }
            formGroup.addClass("form-group-" + formType);
            showSpecificContent(selectedType);
            module.configureFormGroup(form, formGroup);
        });

        let showSpecificContent = function (type) {
            specificContent.html("");
            switch (type) {
                case "text":
                    break;
                case "textarea":
                    let lineCountInput = $('<select class="form-control" id = "line-count-select"></select>');
                    let currentRows = parseInt(formGroup.find('.input-element').attr("rows"));
                    for (let i = 1; i <= 10; i++) {
                        $('<option' + (currentRows === i ? " selected" : "") + ' value="' + i + '">' + i + '</option>').appendTo(lineCountInput);
                    }
                    lineCountInput.on("change", function () {
                        formGroup.find('.input-element').attr("rows", $(this).val());
                    });
                    let lineCountInputGroup = $('<div class="form-group"><label for="line-count-select">Number of Rows</label></div>');
                    lineCountInput.appendTo(lineCountInputGroup);
                    lineCountInputGroup.appendTo(specificContent);
                    break;
                case "select":
                    let select = formGroup.find('select.input-element');
                    specificContent.html('<label>Selection Options</label><div class="alert alert-warning small" role="alert"><i class="fa fa-exclamation-triangle"></i> The first option can not be removed because it is the on used to detect if the input was changed or not. Its name can be changed to something like "Default" or "Please Select..."</div>');
                    let optionsContainer = $('<div class="bg-light p-2"></div>');
                    optionsContainer.appendTo(specificContent);
                    let createNewOption = function () {
                        let option = $('<option value="option">option</option>');
                        option.appendTo(select);
                        return option;
                    };

                    let createOptionEdit = function (option, isDefault) {
                        let inputGroup = $(
                            '<div class="input-group mb-1">\n' +
                            '  <input type="text" class="form-control">\n' +
                            '  <div class="input-group-append">\n' +
                            '    <button class="btn btn-outline-danger" id="removeButton" type="button"><i class="fa fa-trash-alt"></i></button>\n' +
                            '  </div>\n' +
                            '</div>');
                        inputGroup.find("#removeButton").on("click", function () {
                            option.remove();
                            inputGroup.remove();
                        }).prop("disabled", isDefault);
                        inputGroup.find("input").on("change", function () {
                            let val = $(this).val();
                            if (!isDefault) {
                                option.val(val);
                            }
                            option.html(val);
                        }).val(option.html().trim());
                        inputGroup.appendTo(optionsContainer);
                    };
                    let addOptionButton = $('<button class="btn btn-success">Add Option</button>');
                    addOptionButton.on("click", function () {
                        let option = createNewOption();
                        createOptionEdit(option);
                    });

                    select.find("option").each(function () {
                        createOptionEdit($(this), $(this).hasClass("default"));
                    });
                    addOptionButton.appendTo(specificContent);
                    break;
                case "checkbox":
                    specificContent.html('<div class="alert alert-warning small" role="alert"><i class="fa fa-exclamation-triangle"></i> Required for checkboxes means that they must be checked in order for the form to be submitted. Checkboxes with the required tag are not shown in the resulting output!</div>');
                    break;
                case "email":
                    break;
                default:
                    console.error("Unknown Form Control Type " + type + "!");
                    break;
            }
        };
        let specificContent = $('<div class="specific-content"></div>');
        specificContent.appendTo(body);

        showSpecificContent(formType);
        ModuleContactForm.contactFormModuleElementEditorModal.setContent(body).show();
    };


    getEditModalContent() {
        let content = $('<div class="module-content-edit>"></div>');
        let emailInputGroup = $('<div class="form-group"><label for="contact-form-receiver-email-input">Mail Server</label></div>');
        let emailInput = $('<select class="form-control">');
        emailInput.appendTo(emailInputGroup);
        let formReceiverInput = this.element.find(".form-receiver");
        let noMailerWarning = $('<div class="alert alert-danger small" role="alert"><i class="fa fa-exclamation-triangle"></i> You have not selected a Mail server! Without this, no E-Mails can be sent from the form! New Mail servers can be added in the <a class="alert-link" href="/backend/mail-servers/" target="_blank">Mail Server Settings</a>.</div>');
        noMailerWarning.appendTo(content);
        $('<option value="0">Not Selected</option>').appendTo(emailInput);
        emailInput.on("change", function () {
            noMailerWarning.toggleClass("d-none", $(this).val() !== "0");
            formReceiverInput.val($(this).val());
        }).val("0");
        emailInputGroup.appendTo(content);

        $.ajax({
            type: 'POST',
            url: '/backend/ajax/get-mail-servers.php',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    let id = data[i].id;
                    let selected = formReceiverInput.val() === id ? " selected" : "";
                    $('<option' + selected + ' value="' + id + '">' + data[i].name + '</option>').appendTo(emailInput);
                }
                noMailerWarning.toggleClass("d-none", emailInput.val() !== "0");
            },
            error: function (data) {
                console.log(data);
            }
        });

        let createEditor = function (content, name, getSource, setSource, inputType) {
            let inputGroup = $('<div class="form-group"><label>' + name + '</label></div>');
            let input;
            // noinspection JSRedundantSwitchStatement //TODO Remove this?
            switch (inputType) {
                case "text":
                    input = $('<input type="text" class="form-control">');
                    break;
                //TODO add more input types here?
                default:
                    input = $('<textarea class="form-control"></textarea>');
                    break;
            }
            input.appendTo(inputGroup);
            input.on('input change keyup paste', function () {
                setSource(input.val().trim());
            });
            if (inputType === "textarea") {
                input.html(getSource().trim());
            } else {
                input.val(getSource().trim());
            }
            inputGroup.appendTo(content);
        };

        let formEmailTitleInput = this.element.find(".form-mail-title-text");
        createEditor(content, "E-Mail Subject", function () {
            return formEmailTitleInput.html();
        }, function (data) {
            formEmailTitleInput.html(data);
        }, "text");

        let formSuccessTextInput = this.element.find(".form-success-text");
        createEditor(content, "Success Text", function () {
            return formSuccessTextInput.html().replace("<br>", "\n");
        }, function (data) {
            formSuccessTextInput.html(data.replace("\n", "<br>"));
        }, 'textarea');

        let formErrorTextInput = this.element.find(".form-error-text");
        createEditor(content, "Error Text", function () {
            return formErrorTextInput.html().replace("<br>", "\n");
        }, function (data) {
            formErrorTextInput.html(data.replace("\n", "<br>"));
        }, 'textarea');

        let formSenderTextInput = this.element.find(".form-receiver-text");
        createEditor(content, "Receiver Mail Information", function () {
            return formSenderTextInput.html().replace("<br>", "\n");
        }, function (data) {
            formSenderTextInput.html(data.replace("\n", "<br>"));
        }, 'textarea');

        let formReceiverTextInput = this.element.find(".form-sender-text");
        createEditor(content, "User Mail Information", function () {
            return formReceiverTextInput.html().replace("<br>", "\n");
        }, function (data) {
            formReceiverTextInput.html(data.replace("\n", "<br>"));
        }, 'textarea');

        let submitButton = this.element.find(".btn-submit");
        createEditor(content, "Submit button text", function () {
            return submitButton.html();
        }, function (data) {
            submitButton.html(data);
        }, 'text');

        let CaptchaMessage = $('<div class="alert alert-warning" role="alert"><small><i class="fa fa-exclamation-triangle"></i> A <strong>Site-Key</strong> and <strong>Secret-Key</strong> is required to initialize the Captcha security check. Go to the <a href="https://www.google.com/recaptcha/admin/create" class="alert-link">Google captcha admin panel</a> to create a pair of keys.</small></div>');
        CaptchaMessage.appendTo(content);
        let captchaContainer = this.element.find(".captchaContainer");

        createEditor(content, "Captcha Website Key", function () {
            let val = captchaContainer.attr("data-site-key");
            if(val === undefined) val = "";
            return val;
        }, function (data) {
            captchaContainer.attr("data-site-key",data);
            renderCaptcha(captchaContainer);
        }, "text");
        createEditor(content, "Captcha Secret Key", function () {
            let val = captchaContainer.attr("data-secret-key");
            if(val === undefined) val = "";
            return val;
        }, function (data) {
            captchaContainer.attr("data-secret-key",data);
            renderCaptcha(captchaContainer);
        }, "text");

        return content;
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        this.element.html('<div class="title-text-editor mb-2"><div class="text-editor"><h3>Contact Form</h3><p>Description goes here and should describe the contact form.</p></div></div><form action="" method="post" class="custom-contact-form"></form>');
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        module.find(".edit-controls").each(function () {
            $(this).remove();
        });
        module.find(".captchaContainer").each(function () {
            $(this).html("");
        });
        TextEditor.prepareForSavingInChildren(module);
    }
}