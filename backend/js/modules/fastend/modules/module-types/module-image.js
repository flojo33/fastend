import {Module} from "../module";
import {ImageSelector} from "../../interfaces/image-selector";
import {FormUtilities} from "../../interfaces/form-utilities";

class ModuleImage extends Module {

    static getName() {
        return "Image Module";
    }

    static  getDescription() {
        return "Displays an Image";
    }

    static getType() {
        return "image";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(controller, element, initial) {
        super(controller, element, initial);
        this.image = element.find('img');

        let module = this;

        this.image.on("click", function () {
            if (!module.selected) return;
            ImageSelector.show(1200, (type, id, cropId, raw) => {
                if (type === "image") {
                    console.log(id);
                    element.attr("data-image", id);
                    element.attr("data-crop", cropId);
                    element.removeAttr("data-vector");
                    module.image.attr("src", raw);
                } else {
                    element.removeAttr("data-image");
                    element.removeAttr("data-crop");
                    element.attr("data-vector", id);
                    module.image.attr("src", "/media/uploads/vector/" + id + ".svg");
                }
            });
        });
    }

    getEditModalContent() {
        //TODO merge this with the one in text module!
        let content = $('<div class="module-content-edit>"></div>');
        content.append(FormUtilities.createClassSelectInput("Vertical Alignment", this.element, "text-align-", ["top", "center", "bottom"], ["Align Top", "Align Center", "Align Bottom"], 1));
        return content;
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        this.element.html(
            [
                '<img src="/backend/icons/image_placeholder.png" class="img-fluid align-self-center" alt="image">'
            ].join('\n')
        )
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        let image;
        if (module.attr("data-vector") !== undefined) {
            image = $('<img src="/media/uploads/vector/' + module.attr("data-vector") + '.svg" class="img-fluid" alt="image">');
        } else {
            if (module.attr("data-image") !== undefined) {
                if (module.attr("data-crop") !== undefined) {
                    image = $('<img src="/media/uploads/image/crop_' + module.attr("data-image") + '_' + module.attr("data-crop") + '.png" class="img-fluid" alt="image">');
                } else {
                    image = $('<img src="/media/uploads/image/' + module.attr("data-image") + '.png" class="img-fluid" alt="image">');
                }
            } else {
                image = $('<img src="/backend/icons/image_placeholder.png" class="img-fluid" alt="image">');
            }
        }
        module.html('');
        image.appendTo(module);
    }
}
export {ModuleImage}
