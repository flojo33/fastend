import {Module} from "../module";
import {TextEditor} from "../../interfaces/text-editor/text-editor.js";

class ModuleCards extends Module {

    static getName() {
        return "Cards Module";
    }

    static  getDescription() {
        return "Displays a list of cards.";
    }

    static getType() {
        return "cards";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        new TextEditor(this.element,column.row.controller.parentContainer);
    }

    getEditModalContent() {
        let content = $('<div class="module-content-edit>">TODO</div>');

        return content;
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        this.element.html(
            '<div class="card-deck"></div>'
        );
        this.addCard();
        this.addCard();
        this.addCard();
    }

    addCard() {
        let card = $(
            '<div class="card">\n' +
            '    <div class="card-body">\n' +
            '       <div class="text-editor"><h3>Card Title</h3><p>Card Content</p></div>\n' +
            '    </div>\n' +
            '  </div>');
        this.element.find(".card-deck").append(card);
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        TextEditor.prepareForSavingInChildren(module);
    }
}
export {ModuleCards}
