import {Module} from "../module";

export class ModuleBase extends Module {

    static getName() {
        return "Base Module";
    }

    static  getDescription() {
        return "This is the base module and should not be created by hand. It does not contain any functionality and is only used to select the real Module";
    }

    static getType() {
        return "base";
    }

    static getBuildable() {
        return false;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        this.element.html(
            [
                '<button class ="btn btn-setup editor-element"><i class="fa fa-plus"></i></button>',
                '<div class ="empty-display">Empty Module</div>'
            ].join('\n')
        );
        this.button = element.find('.btn-setup');
        this.text = element.find('.empty-display');
        this.button.on("click", function () {
            column.row.controller.showSelectModal();
        });
        this.onDeselect();
    }

    getEditModalContent() {
        return $('<div class="text-muted">Empty modules have no module-specific settings.</div>');
    }

    onSelect() {
        super.onSelect();
        this.button.show();
        this.text.hide();
    }

    buildHtml() {

    }

    onDeselect() {
        super.onDeselect();
        this.button.hide();
        this.text.show();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        //Clear the contents of the module
        module.html("");
    }
}