import {Module} from "../module";
import {FormUtilities} from "../../interfaces/form-utilities";
import {TextEditor} from "../../interfaces/text-editor/text-editor.js";

class ModuleText extends Module {

    static getName() {
        return "Text Module";
    }

    static  getDescription() {
        return "Displays a Rich text containing tables, links, buttons etc.";
    }

    static getType() {
        return "text";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        new TextEditor(this.element.find(".vertical-align"),column.row.controller.parentContainer);
    }

    getEditModalContent() {
        let content = $('<div class="module-content-edit>"></div>');
        content.append(FormUtilities.createClassSelectInput("Vertical Alignment", this.element, "text-align-", ["top", "center", "bottom"], ["Align Top", "Align Center", "Align Bottom"], 1));
        return content;
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        this.element.html(
            [
                '<div class="vertical-align">',
                '   <div class="text-editor">',
                '       <h3>Text Module</h3>',
                '       <p>The modules content can be edited using the keyboard. Selected text can be edited using the tool popup.</p>',
                '   </div>',
                '</div>'
            ].join('\n')
        )
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
        TextEditor.prepareForSavingInChildren(module);
    }
}
export {ModuleText}
