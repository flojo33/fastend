import {Module} from "../module";

export class ModuleMap extends Module {

    static getName() {
        return "Google Maps Module";
    }

    static  getDescription() {
        return "This module shows a google maps map.";
    }

    static getType() {
        return "map";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        this.onDeselect();
    }

    getEditModalContent() {
        let mapsUrl = 'https://www.google.com/maps/embed/v1/place?';
        let content = $('<div class="module-content-edit>"><div class="alert alert-warning" role="alert"><small><i class="fa fa-exclamation-triangle"></i> An API Key is required in order for the Maps module to function properly. For more information on how to obtain a Google Maps API Key, read the <a href="https://developers.google.com/maps/documentation/embed/get-api-key" class="alert-link">Google Maps developer documentation</a></small>.' +
            '</div></div>');

        let currentValues = this.element.find("iframe").attr('src').replace(mapsUrl, "").split("&");

        let location = "Munich";
        let apiKey = "";
        let zoom = 15;
        let type = "satellite";
        for (let i = 0; i < currentValues.length; i++) {
            if (currentValues[i].startsWith("q=")) {
                location = currentValues[i].replace("q=", "");
            }
            if (currentValues[i].startsWith("zoom=")) {
                zoom = currentValues[i].replace("zoom=", "");
            }
            if (currentValues[i].startsWith("maptype=")) {
                type = currentValues[i].replace("maptype=", "");
            }
            if (currentValues[i].startsWith("key=")) {
                apiKey = currentValues[i].replace("key=", "");
            }
        }

        let keyFormGroup = $('<div class="form-group"><label for="map-api-key">Api Key</label></div>');
        let keyInput = $('<input type="text" class="form-control" id="map-api-key">');
        keyInput.on("change", function () {
            updateIframe();
        });
        keyInput.val(apiKey);
        keyInput.appendTo(keyFormGroup);
        keyFormGroup.appendTo(content);

        let locationFormGroup = $('<div class="form-group"><label for="map-location">Map Location</label></div>');
        let locationInput = $('<input type="text" class="form-control" id="map-location">');
        locationInput.on("change", function () {
            updateIframe();
        });
        locationInput.val(location);
        locationInput.appendTo(locationFormGroup);
        locationFormGroup.appendTo(content);

        let zoomFormGroup = $('<div class="form-group"><label for="map-zoom">Map Zoom</label></div>');
        let zoomInput = $('<select class="form-control" id="map-zoom"></select>');
        for (let i = 0; i <= 21; i++) {
            let value = i;
            let selected = ((i + "") === ("" + zoom)) ? " selected" : "";
            if (i === 0) {
                value += " (Entire World)";
            }
            if (i === 21) {
                value += " (Individual Buildings)";
            }
            $('<option value="' + i + '"' + selected + '>' + value + '</option>').appendTo(zoomInput);
        }
        zoomInput.on("change", function () {
            updateIframe();
        });
        zoomInput.appendTo(zoomFormGroup);
        zoomFormGroup.appendTo(content);


        let typeFormGroup = $('<div class="form-group"><label for="map-type">Map Type</label></div>');
        let typeInput = $('<select class="form-control" id="map-type"></select>');
        let mapTypes = ["roadmap", "satellite"];
        for (let i = 0; i < mapTypes.length; i++) {
            let selected = mapTypes[i] === type ? " selected" : "";
            $('<option value="' + mapTypes[i] + '"' + selected + '>' + mapTypes[i] + '</option>').appendTo(typeInput);
        }
        typeInput.on("change", function () {
            updateIframe();
        });
        typeInput.appendTo(typeFormGroup);
        typeFormGroup.appendTo(content);

        let heightFormGroup = $('<div class="form-group"><label for="map-height">Map Height (Pixels)</label></div>');
        let heightInput = $('<input type="number" class="form-control" id="map-height">');
        heightInput.on("change", function () {
            updateIframe();
        });
        heightInput.appendTo(heightFormGroup);
        heightFormGroup.appendTo(content);

        //Location
        let updateIframe = () => {
            this.element.find("iframe").css("height", heightInput.val() + "px");
            let apiKey = keyInput.val();
            let location = locationInput.val();
            let zoom = zoomInput.val();
            let type = typeInput.val();
            let source = mapsUrl + "q=" + location + "&key=" + apiKey + "&zoom=" + zoom + "&maptype=" + type;
            this.element.find("iframe").attr("src", source);
        };

        let initialHeight = this.element.find("iframe").css("height").replace("px", "");
        heightInput.val(initialHeight);
        return content;
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {
        this.element.html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d101790.81200418652!2d11.564978921472472!3d48.11489496347512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e75f9a38c5fd9%3A0x10cb84a7db1987d!2zTcO8bmNoZW4!5e1!3m2!1sde!2sde!4v1579876201650!5m2!1sde!2sde" style="border:0; height: 500px; width: 100%;" allowfullscreen=""></iframe>');
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
    }
}