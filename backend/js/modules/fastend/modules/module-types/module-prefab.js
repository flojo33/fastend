import {Module} from "../module";

export class ModulePrefab extends Module {

    static getName() {
        return "NAME";
    }

    static  getDescription() {
        return "DESCRIPTION";
    }

    static getType() {
        return "TYPENAME";
    }

    static getBuildable() {
        return true;
    }

    static setupInitial() {

    }

    constructor(column, element, initial) {
        super(column, element, initial);
        this.element.html(
            [
                ''
            ].join('\n')
        );
        this.onDeselect();
    }

    getEditModalContent() {
        return $('<div></div>');
    }

    onSelect() {
        super.onSelect();
    }

    buildHtml() {

    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        Module.prepareForSaving(module);
    }
}