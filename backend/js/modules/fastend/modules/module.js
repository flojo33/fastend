class Module {

    static getName() {
        return "Abstract Module";
    }

    static getDescription() {
        return "This should not be visible to the user!";
    }

    static getType() {
        return "abstract";
    }

    static getBuildable() {
        return false;
    }

    static setupInitial() {

    }

    /**
     *
     * @param {Column} column
     * @param {jQuery} element
     * @param {boolean} initial
     */
    constructor(column, element, initial) {
        this.column = column;
        this.element = element;
        this.selected = false;
        if(initial) {
            this.buildHtml();
        }
        //Todo Check if this is required or not...
        element.data("module",this);
        element.on('click',() => this.onSelect());
    }

    getEditModalContent() {
        return $('<div></div>');
    }

    onSelect() {
        this.column.element.addClass("selected");
        this.selected = true;
    }

    onDeselect() {
        this.column.element.removeClass("selected");
        this.selected = false;
    }

    buildHtml() {
        this.element.html('');
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} module
     */
    static prepareForSaving(module) {
        module.parent().removeClass("setup"); //Remove setup from columns
        module.parent().parent().removeClass("setup"); //Remove setup from rows
    }
}

export {Module};