class InputLengthCounter {
    static create(input, display) {
        let updateDisplay = function() {
            display.html(input.val().length +"/"+ input.attr('maxLength'));
        };
        input.on('input',updateDisplay);
        updateDisplay();
    }
}

export { InputLengthCounter }