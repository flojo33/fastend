export class PillTabContainer {
    constructor(id) {
        this.id = id;
        this.tabCount = 0;
        this.element = $('<div>');
        this.tabs = $('<ul class="nav nav-pills mb-3" role="tablist"></ul>');
        this.tabs.attr("id",id);
        this.panes = $('<div class="tab-content"></div>');
        this.element.append(this.tabs);
        this.element.append(this.panes);
    }
    addTab(name) {
        this.tabCount ++;
        let selected = this.tabCount===1;
        let active = selected?' active':'';
        let selectedAria = selected?'true':'false';
        let tabId = 'pill-tab-container-'+this.id+'-'+this.tabCount+'-tab';
        let tabPaneId = 'pill-tab-container-'+this.id+'-'+this.tabCount+'';
        this.tabs.append($('<li class="nav-item"><a class="nav-link'+active+'" id="'+tabId+'" data-toggle="pill" href="#'+tabPaneId+'" role="tab" aria-controls="'+tabPaneId+'" aria-selected="'+selectedAria+'">'+name+'</a></li>'));
        let pane = $('<div class="tab-pane fade show'+active+'" id="'+tabPaneId+'" role="tabpanel" aria-labelledby="'+tabId+'"></div>');
        this.panes.append(pane);
        return pane;
    }
}