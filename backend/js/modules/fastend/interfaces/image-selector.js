import {Modal} from "./modal.js";
import {MediaBrowser} from "./media-browser";
import {ModalLoading} from "./modal-loading";
import {ModalAlert} from "./modal-alert";

class ImageSelector {

    static show(maxSize, returnFunction) {
        this.initialize();
        this.currentMaxSize = maxSize;
        this.currentReturnFunction = returnFunction;
        this.selectModal.show();
    }

    static initialize() {
        let selector = this;
        if(selector.initialized) return;
        selector.initialized = true;
        selector.currentImageId = "";
        selector.imageCropperSetupCompleted = false;
        let selectModalContent = $([
            '<div class="container-fluid">\n' +
            '    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">',
            '            <h5 id="current-name"></h5>',
            '            <div class="btn-toolbar mb-2 mb-md-0">',
            '                <div class="mr-2 d-inline-flex" id="uploader"></div>',
            '                <div class="btn-group mr-2">',
            '                    <button type="button" class="btn btn-sm btn-outline-success" id="add-folder-button"><i class="fa fa-folder-plus"></i> Create Folder</button>',
            '                </div>',
            '            </div>',
            '    </div>',
            '    <div class="row media-library"></div>',
            '</div>'
        ].join('\n'));

        let cropModalContent = $([
            '<div class="img-container"></div>'
        ].join('\n'));

        selector.selectModal = new Modal("imageSelectModal")
            .setTitle("Select Image")
            .setContent(selectModalContent)
            .setSize('xl');

        let cropperContainer;

        selector.cropModal = new Modal("imageCropModal")
            .setTitle("Crop Image")
            .setContent(cropModalContent)
            .setClickOutsideCloses(false)
            .setOnShown(() => {
                let src = '/media/uploads/image/' + selector.currentImageId + '.png';
                if (selector.imageCropperSetupCompleted) {
                    cropperContainer.cropper('replace', src);
                } else {
                    selector.imageCropperSetupCompleted = true;
                    cropperContainer = $('<img id="image" src="' + src + '" alt="crop-image">');
                    cropperContainer.appendTo(selector.cropModal.element.find('.img-container'));
                    cropperContainer.cropper({
                        viewMode: 1,
                        autoCrop: false,
                        scalable: false,
                        movable: false,
                        zoomable: false
                    });
                }
            });
        let closeButton = $('<button class="btn btn-secondary">Cancel</button>');
        let saveButton = $('<button class="btn btn-primary">Save</button>');
        let footer = $('<div>');
        closeButton.on("click", () => selector.cropModal.hide());
        saveButton.on("click", () => {
            let canvas = cropperContainer.cropper("getCroppedCanvas", {
                maxWidth: selector.currentMaxSize,
                maxHeight: selector.currentMaxSize,
                imageSmoothingEnabled: true,
                imageSmoothingQuality: 'high'
            });
            selector.cropModal.hide();
            ModalLoading.show("Uploading Crop");
            canvas.toBlob(function (cropData) {
                let formData = new FormData();

                formData.append("cropBlob", cropData);
                formData.append("imageId", selector.currentImageId);

                jQuery.ajax({
                    url: '/backend/ajax/upload-image-crop.php',
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    /**
                     *
                     * @param {{crop_id:number}} data
                     */
                    success: function (data) {
                        selector.currentReturnFunction("image", selector.currentImageId, data.crop_id, canvas.toDataURL("image/png"));
                    },
                    complete: function() {
                        setTimeout(ModalLoading.hide,900);
                    }
                });
            });
        });
        footer.append(saveButton).append(closeButton);
        selector.cropModal.setFooter(footer);

        MediaBrowser.build(selectModalContent.find('.media-library'), selectModalContent.find('#current-name'), selectModalContent.find('#uploader'), null, selectModalContent.find('#add-folder-button'), function (itemId, itemType) {
            if (itemType === "image") {
                selector.currentImageId = itemId;
                selector.selectModal.hide();
                selector.cropModal.show();
            } else {
                if (itemType === "vector") {
                    selector.currentReturnFunction("vector", itemId);
                    selector.selectModal.hide();
                } else {
                    alert("Please select an image or Vector Graphic!");
                }
            }
        });

        console.log("initialized image selector module");
    }
}

export {ImageSelector}