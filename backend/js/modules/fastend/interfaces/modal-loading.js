import {ModalAlert} from "./modal-alert.js";

/**
 * Implements static functions to show and hide blocking loading modals.
 */
export class ModalLoading {

    /**
     * Show a loading popover modal that can be hidden with the hide() function
     * @param {string} text the text to show above the loading widget
     */
    static show(text) {
        ModalAlert.showBlock($('<div class="text-center"><h5 class="modal-title mb-3">'+text+'</h5><p><i class="fas fa-circle-notch fa-spin fa-6x"></i></p></div>'));
    }

    /**
     * hide the loading modal
     */
    static hide() {
        ModalAlert.hide();
    }
}