export class FormUtilities {
    static createClassSelectInput(name, targetElement, prefix, options, names, defaultValue) {
        let currentValue = defaultValue;
        let id = prefix + "class-select";
        let formGroup = $('<div class="form-group"><label for="' + id + '">' + name + '</label></div>');
        let select = $('<select class="form-control" id="' + id + '"></select>');
        for (let i = 0; i < options.length; i++) {
            if (targetElement.hasClass(prefix + options[i])) {
                currentValue = i;
            }
        }
        for (let i = 0; i < options.length; i++) {
            let selected = "";
            if (currentValue === i) {
                selected = " selected"
            }
            $('<option value="' + i + '"' + selected + '>' + names[i] + '</option>').appendTo(select);
        }
        select.on('change', function () {
            for (let i = 0; i < options.length; i++) {
                if (options[i] === "") continue;
                targetElement.toggleClass(prefix + options[i], i === parseInt($(this).val()));
            }
        });
        select.appendTo(formGroup);
        return formGroup;
    };
}