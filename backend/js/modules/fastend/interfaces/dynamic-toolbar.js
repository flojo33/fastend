class DynamicToolbar {
    static Initialize(element) {
        //TODO check if the class is really necessary... We are adjusting the height by js anyways.
        let mainBarPlaceholder = $('<div class="main-bar-container-placeholder">').insertAfter(element);
        element.checkTopScroll = function() {
            setTimeout(function() {
                let fixed = $("html").scrollTop() > element.parent().offset().top - 48;
                let wantedHeight = 0;
                if(fixed) {
                    wantedHeight = element.height();
                }
                mainBarPlaceholder.css("height",wantedHeight);
                element.toggleClass("fixed",fixed);
            }, 50);
        };
        element.checkTopScroll();
        $(window).scroll(function() {
            element.checkTopScroll();
        });
    }
}

export {DynamicToolbar}