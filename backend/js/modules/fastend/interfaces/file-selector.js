import {Modal} from "./modal.js";
import {MediaBrowser} from "./media-browser";

class FileSelector {

    static show(returnFunction) {
        this.initialize();
        this.currentReturnFunction = returnFunction;
        this.selectModal.show();
    }

    static initialize() {
        let selector = this;
        if(selector.initialized) return;
        selector.initialized = true;
        selector.currentImageId = "";
        selector.imageCropperSetupCompleted = false;
        let selectModalContent = $([
            '<div class="container-fluid">\n' +
            '    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">',
            '            <h5 id="current-name"></h5>',
            '            <div class="btn-toolbar mb-2 mb-md-0">',
            '                <div class="mr-2 d-inline-flex" id="uploader"></div>',
            '                <div class="btn-group mr-2">',
            '                    <button type="button" class="btn btn-sm btn-outline-success" id="add-folder-button"><i class="fa fa-folder-plus"></i> Create Folder</button>',
            '                </div>',
            '            </div>',
            '    </div>',
            '    <div class="row media-library"></div>',
            '</div>'
        ].join('\n'));

        selector.selectModal = new Modal("imageSelectModal")
            .setTitle("Select Image")
            .setContent(selectModalContent)
            .setSize('xl');

        MediaBrowser.build(selectModalContent.find('.media-library'), selectModalContent.find('#current-name'), selectModalContent.find('#uploader'), null, selectModalContent.find('#add-folder-button'), function (itemId, itemType) {
            selector.selectModal.hide();
            selector.currentReturnFunction(itemId, itemType);
        });

        console.log("initialized file selector module");
    }
}

export {FileSelector}