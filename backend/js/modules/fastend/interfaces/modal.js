export class Modal {
    /**
     * Create a bootstrap modal and append it to the body of the page.
     * @param {string} id
     */
    constructor(id) {
        //Build the html structure of the modal
        this.element = $('<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>');
        this.element.attr('id',id);
        this.element.attr('aria-labelledby',id+"Label");
        this.dialog = $('<div class="modal-dialog" role="document"></div>');
        this.element.append(this.dialog);
        this.content = $('<div class="modal-content"></div>');
        this.dialog.append(this.content);

        this.onClose = function() {};
        this.onShown = function() {};

        //Build all modal header elements
        this.header = $('<div class="modal-header"></div>');
        this.headerText = $('<h5 class="modal-title">Modal title</h5>');
        this.headerText.attr('id',id+"Label");
        this.header.append(this.headerText);
        this.headerCloseButton = $('<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        this.headerCloseButton.on('click', () => this.hide());
        this.header.append(this.headerCloseButton);
        this.content.append(this.header);

        //Build all modal body elements
        this.body = $('<div class="modal-body"></div>');
        this.content.append(this.body);

        //Build all modal footer elements
        this.footer = $('<div class="modal-footer">');
        this.content.append(this.footer);

        this.element.on('hidden.bs.modal',() => {
            if(this.onClose !== null) {
                this.onClose();
            }
        });

        this.element.on('shown.bs.modal', () => {
            if(this.onShown !== null) {
                this.onShown();
            }
        });

        this.setFooter($('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'));

        $('body').append(this.element);
    }

    show() {
        this.element.modal("show");
        return this;
    }

    hide() {
        this.element.modal("hide");
        return this;
    }

    setTitle(content) {
        this.headerText.html("");
        if(content === "") {
            this.header.hide();
        } else {
            this.header.show();
            this.headerText.append(content);
        }
        return this;
    }

    setOnClose(closeFunction) {
        this.onClose = closeFunction;
        return this;
    }

    setOnShown(shownFunction) {
        this.onShown = shownFunction;
        return this;
    }

    /**
     * Set if the modal can be closed by clicking outside the window
     * @param {boolean} closes
     */
    setClickOutsideCloses(closes) {
        if(!closes) {
            this.element.modal({backdrop: 'static', keyboard: false, show:false})
        } else {

            this.element.modal({backdrop: 'true', keyboard: true, show:false})
        }
        return this;
    }

    /**
     * Set if the modal can be closed by clicking outside the window
     * @param {boolean} visible
     */
    setCrossCloseButtonVisible(visible) {
        if(visible) {
            this.headerCloseButton.show();
        } else {
            this.headerCloseButton.hide();
        }
        return this;
    }

    setContent(content) {
        this.body.html("");
        this.body.append(content);
        return this;
    }

    setSize(size) {
        this.dialog.toggleClass('modal-xl',size==="xl");
        return this;
    }

    setFooter(content) {
        this.footer.html("");
        if(content === "") {
            this.footer.hide();
        } else {
            this.footer.show();
            this.footer.append(content);
        }
        return this;
    }
}