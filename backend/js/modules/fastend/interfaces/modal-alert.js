import {Modal} from "./modal";

/**
 * A class that creates similar functions to alert and confirm using bootstrap modals to blend nicer with the concept.
 */
export class ModalAlert {

    /**
     * Initializes the modal that will be reused for all different types of alerts.
     */
    static init() {
        if (!this.initialized) {
            this.initialized = true;
            this.modal = new Modal("alertModal")
                .setCrossCloseButtonVisible(true);
        }
    }

    /**
     * Show a modal that can be hidden by the user
     * @param {jQuery|string} title The title of the Modal
     * @param {jQuery|string} content The content of the Modal
     * @param {function} complete The function that is executed when the modal is closed either by clicking outside, or on the close buttons
     */
    static alert(title, content, complete) {
        this.init();
        this.modal
            .setTitle(title)
            .setContent(content)
            .setFooter($('<button class="btn btn-secondary" data-dismiss="modal">Ok</button>'))
            .setClickOutsideCloses(true)
            .setOnClose(() => complete())
            .show();
    }

    /**
     * Shows a modal to either select yes or no for a given title and content.
     * @param {jQuery|string} title The title of the Modal
     * @param {jQuery|string} content The content of the Modal
     * @param {function} yes The function that is executed when yes is clicked
     * @param {function} no The function that is executed when no is clicked
     * @param {jQuery|string} yesContent The text or content of the yes button
     * @param {jQuery|string} noContent The text or content of the no button
     */
    static confirm(title, content, yes, no, yesContent = "Yes", noContent = "No") {
        this.init();

        let modal = this.modal;

        //Build the Yes button
        let yesButton = $('<button class="btn btn-success mr-2" data-dismiss="modal"></button>');
        yesButton.on('click', function () {
            yes();
            modal.hide();
        }).append(yesContent);

        //Build the No button
        let noButton = $('<button class="btn btn-danger" data-dismiss="modal"></button>');
        noButton.on('click', function () {
            no();
            modal.hide();
        }).append(noContent);

        let footer = $("<div>").append(yesButton).append(noButton);

        modal.setTitle(title)
            .setContent(content)
            .setFooter(footer)
            .setClickOutsideCloses(true)
            .setOnClose(null)
            .show();
    }

    /**
     * Show a blocking modal that can only be hidden using the hide() function.
     * @param {jQuery|string} content The content of the Modal
     */
    static showBlock(content) {
        this.init();
        this.modal
            .setTitle("")
            .setContent(content)
            .setFooter("")
            .setClickOutsideCloses(false)
            .setOnClose(null)
            .show();
    }

    /**
     * Hide the blocking modal
     */
    static hide() {
        this.init();
        this.modal.hide();
    }
}