import {TextEditor} from "./text-editor.js";

/**
 * The actual quill text editor elements. Internally the TextEditor class creates multiple of these to allow tables
 * and other custom elements to be inserted between the text editors.
 */
export class TextEditorChild {
    /**
     *
     * @param {TextEditor} parent
     */
    static create(parent) {
        let element = $('<div class="text-editor"></div>');
        return new TextEditorChild(element, parent);
    }

    /**
     * Convert all quill classes in the element to custom text-editor / bootstrap.
     * @param {jQuery} element
     */
    static prepareForSaving(element) {
            element.find('.ql-tooltip').remove();
            element.find('.ql-tooltip-editor').remove();
            element.find('.ql-clipboard').remove();
            element.find('.ql-table-tooltip').remove();
            element.removeClass('ql-container');
            element.removeClass('ql-bubble');
            element.removeClass('ql-disabled');
            element.find('.ql-editor').contents().appendTo(element);
            element.find('.ql-editor').remove();
            for (let i = 0; i < TEXT_EDITOR_REPLACEMENT_CLASSES.length; i++) {
                TextEditorChild.replaceClasses(element, TEXT_EDITOR_REPLACEMENT_CLASSES[i][0], TEXT_EDITOR_REPLACEMENT_CLASSES[i][1]);
            }
    }

    /**
     * Replace all classes "initialClass" with "replacementClass" in the children of "parent"
     * @param {jQuery} parent
     * @param {string} initialClass
     * @param {string} replacementClass
     */
    static replaceClasses(parent, initialClass, replacementClass) {
        parent.find('.' + initialClass).each(function () {
            $(this).removeClass(initialClass).addClass(replacementClass);
        });
    };

    /**
     *
     * @param {jQuery} element
     * @param {TextEditor} parent
     */
    constructor(element, parent) {
        this.removed = false;
        this.element = element;
        this.parent = parent;
        this.initialize();
        parent.children.push(this);
    }

    remove() {
        this.removed = true;
        this.element.remove();
    }

    select() {
        TextEditor.tableTooltip.show(this);
    }

    initialize() {
        let self = this;
        let isTableCell = self.element.parent().is("th,td");
        for (let i = 0; i < TEXT_EDITOR_REPLACEMENT_CLASSES.length; i++) {
            TextEditorChild.replaceClasses(self.element, TEXT_EDITOR_REPLACEMENT_CLASSES[i][1], TEXT_EDITOR_REPLACEMENT_CLASSES[i][0]);
        }

        //Override default bindings of backspace and delete to remove quills between tables
        let bindings = {
            backspace: {
                key: 8,
                handler: function() {
                    if(self.element.find('.ql-editor').text() === "" && !isTableCell) {
                        self.remove();
                        self.parent.checkEmpty();
                        return false;
                    }
                    return true;
                }
            },
            delete: {
                key: 46,
                handler: function() {
                    if(self.element.find('.ql-editor').text() === "" && !isTableCell) {
                        self.remove();
                        self.parent.checkEmpty();
                        return false;
                    }
                    return true;
                }
            },
            'header shift enter': {
                key: ENTER_KEY,
                shiftKey: true,
                collapsed: true,
                format: ['header'],
                suffix: /^$/,
                handler: function(range, context) {
                    self.insertNewline(range, context, {'header': false, 'pspacing': '0px'});
                    return false;
                }
            },
            'header enter': {
                key: ENTER_KEY,
                shiftKey: false,
                collapsed: true,
                format: ['header'],
                suffix: /^$/,
                handler: function(range, context) {
                    self.insertNewline(range, context, {'header': false, 'pspacing': false});
                    return false;
                }
            },
            'shift enter': {
                key: ENTER_KEY,
                shiftKey: true,
                handler: function (range, context) {
                    self.insertNewline(range, context, {'pspacing': '0px'});
                    return false;
                }
            },
            'enter': {
                key: ENTER_KEY,
                shiftKey: false,
                handler: function (range, context) {
                    self.insertNewline(range, context, {'pspacing': false});
                    return false;
                }
            }
        };

        self.quill = new Quill(self.element[0], {
            placeholder: '',
            theme: 'bubble',
            modules: {
                toolbar: TEXT_EDITOR_TOOLBAR,
                keyboard: {
                    bindings: bindings
                }
            },
            bounds: self.parent.boundaryContainer[0]
        });

        self.element.data("text-editor-child", self);

        let toolbar = self.quill.getModule("toolbar");
        toolbar.addHandler('link', function () {
            TextEditor.showLinkModal(self.quill);
        });

        self.quill.on("selection-change", () => {
            TextEditor.tableTooltip.show(self);
        });
        self.quill.on("text-change", function() {
            TextEditor.tableTooltip.hide();
        });

        $( window ).resize(() => {
            TextEditor.tableTooltip.hide();
        });
        self.quill.root.setAttribute('spellcheck', "false");
    }

    insertNewline(range, context, formattingChanges) {
        // Based on modules/keyboard.js handleEnter
        if (range.length > 0) {
            this.quill.scroll.deleteAt(range.index, range.length);  // So we do not trigger text-change
        }
        let lineFormats = Object.keys(context.format).reduce(function(lineFormats, format) {
            if (Parchment.query(format, Parchment.Scope.BLOCK) && !Array.isArray(context.format[format])) {
                lineFormats[format] = context.format[format];
            }
            return lineFormats;
        }, {});
        this.quill.insertText(range.index, '\n', lineFormats, Quill.sources.USER);
        // Earlier scroll.deleteAt might have messed up our selection,
        // so insertText's built in selection preservation is not reliable
        this.quill.setSelection(range.index + 1, Quill.sources.SILENT);
        this.quill.focus();
        Object.keys(context.format).forEach((name) => {
            if (lineFormats[name] != null) return;
            if (Array.isArray(context.format[name])) return;
            if (name === 'link') return;
            this.quill.format(name, context.format[name], Quill.sources.USER);
        });
        this.quill.formatLine(range.index + 1, 1, formattingChanges, Quill.sources.USER);
    };
}

/**
 * All quill classes that must be converted to bootstrap classes and vice-versa
 * @type {string[][]}
 */
const TEXT_EDITOR_REPLACEMENT_CLASSES = [
    ["ql-align-left", "text-left"],
    ["ql-align-right", "text-right"],
    ["ql-align-center", "text-center"],
    ["ql-align-justify", "text-justify"],
    ["ql-indent-1", "text-indent-1"],
    ["ql-indent-2", "text-indent-2"],
    ["ql-indent-3", "text-indent-3"],
    ["ql-indent-4", "text-indent-4"],
    ["ql-indent-5", "text-indent-5"],
    ["ql-indent-6", "text-indent-6"],
    ["ql-indent-7", "text-indent-7"],
    ["ql-indent-8", "text-indent-8"],
    ["ql-indent-9", "text-indent-9"],
    ["ql-indent-10", "text-indent-10"],
];

/**
 * Declaration of the toolbar used in the quill text editors.
 */
const TEXT_EDITOR_TOOLBAR = [
    ['bold', 'italic', 'underline', /*'strike'*/],      // toggled buttons
    ['blockquote', 'code-block'],

    //[{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{'list': 'ordered'}, {'list': 'bullet'}],
    //[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{'indent': '-1'}, {'indent': '+1'}],               // outdent/indent
    //[{ 'direction': 'rtl' }],                         // text direction

    //[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{'header': [1, 2, 3, 4, 5, 6, false]}],

    //[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    //[{ 'font': [] }],
    [{'align': []}],
    ['link'],

    ['clean']                                           // remove formatting button

];

/**
 * The enter key for rebinding quill enter actions.
 * @type {number}
 */
const ENTER_KEY = 13;