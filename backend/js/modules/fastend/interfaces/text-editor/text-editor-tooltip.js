import {TextEditorChild} from "./text-editor-child";

export class TextEditorTooltip {
    constructor() {
        this.element = $('<div class="ql-table-tooltip"><div class="arrow"></div><div class="content"></div></div>');
    }

    hide() {
        this.element.remove();
        this.element.hide();
    }

    /**
     *
     * @param {TextEditorChild} child
     */
    show(child) {
        this.currentEditingChild = child;
        child.element.append(this.element);
        this.update();
    }

    update() {
        let tooltip = this;
        let element = this.element.parent();
        let isTableCell = element.parent().is("th,td");

        let selection = window.getSelection();
        if(!selection.isCollapsed) {
            this.hide();
            return;
        }
        let anchorNode = $(selection.anchorNode);

        if(element.has(anchorNode).length === 0) {
            this.hide();
            return;
        }
        let content = this.element.find(".content");
        content.html("");
        if(isTableCell) {
            //Render the table manipulation buttons
            content.css("width","150px");
            content.css("left","-75px");
            let toggleRowButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_set-row-td.svg"></button>');
            toggleRowButton.appendTo(content);
            toggleRowButton.on("click",function() {
                let cell = element.parent();
                let row = cell.parent();
                let isHeader = cell.is("th");
                tooltip.setTableRowHeader(row,!isHeader);
                let previousCell = element.parent();
                let quill = previousCell.find('.text-editor').data("text-editor-child").quill;
                let length = quill.getLength();
                quill.setSelection(length-1, length-1, "user");
            });

            let addRowUpButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_add-row-up.svg"></button>');
            addRowUpButton.appendTo(content);
            addRowUpButton.on("click",function() {
                let row = element.parent().parent();
                let newRow = tooltip.duplicateRow(row);
                newRow.insertBefore(row);
            });

            let addRowDownButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_add-row-down.svg"></button>');
            addRowDownButton.appendTo(content);
            addRowDownButton.on("click",function() {
                let row = element.parent().parent();
                let newRow = tooltip.duplicateRow(row);
                newRow.insertAfter(row);
            });

            let addRowLeftButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_add-column-left.svg"></button>');
            addRowLeftButton.appendTo(content);
            addRowLeftButton.on("click",function() {
                tooltip.hide();
                let cell = element.parent();
                let index = cell.index();
                cell.parent().parent().children().each(function() {
                    let oldCell = $(this).children().eq(index);
                    let newCell = oldCell.clone();
                    newCell.find(".text-editor").remove();
                    let newEditor = $('<div class="text-editor"></div>');
                    newEditor.html("Cell");
                    newEditor.appendTo(newCell);
                    new TextEditorChild(newEditor, tooltip.currentEditingChild.parent);
                    newCell.insertBefore(oldCell);
                });
            });

            let addRowRightButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_add-column-right.svg"></button>');
            addRowRightButton.appendTo(content);
            addRowRightButton.on("click",function() {
                let cell = element.parent();
                let index = cell.index();
                cell.parent().parent().children().each(function() {
                    let oldCell = $(this).children().eq(index);
                    let newCell = oldCell.clone();
                    newCell.find(".text-editor").remove();
                    let newEditor = $('<div class="text-editor"></div>');
                    newEditor.html("Cell");
                    newEditor.appendTo(newCell);
                    new TextEditorChild(newEditor, tooltip.currentEditingChild.parent);
                    newCell.insertAfter(oldCell);
                });
            });

            let removeRowButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_delete-row.svg"></button>');
            removeRowButton.appendTo(content);
            removeRowButton.on("click",function() {
                let row = element.parent().parent();
                let previousRow = row.prev();
                if(previousRow === undefined || previousRow.length === 0) {
                    previousRow = row.next();
                }
                let id = element.parent().index();
                let table = row.parent();
                row.find(".text-editor").data("text-editor-child").remove();
                row.remove();
                if(table.children().length === 0) {
                    let tableContainer = table.parents(".table-container");
                    tooltip.removeTable(tableContainer);
                } else {
                    let quill = previousRow.children().eq(id).find('.text-editor').data("text-editor-child").quill;
                    let length = quill.getLength();
                    quill.setSelection(length-1, length-1, "user");
                }
            });

            let removeColumnButton = $('<button class="btn editor-element"><img alt="icon" class="custom-icon" src="/backend/icons/table-icons_delete-column.svg"></button>');
            removeColumnButton.appendTo(content);
            removeColumnButton.on("click",function() {
                let cell = element.parent();
                let previousCell = cell.prev();
                if(previousCell === undefined || previousCell.length === 0) {
                    previousCell = cell.next();
                }
                let index = cell.index();
                if(cell.parent().children().length === 1) {
                    let tableContainer = cell.parents(".table-container");
                    tooltip.removeTable(tableContainer);
                } else {
                    cell.parent().parent().children().each(function() {
                        let oldCell = $(this).children().eq(index);
                        oldCell.find(".text-editor").data("text-editor-child").remove();
                        oldCell.remove();
                    });
                }
                if(previousCell !== undefined && previousCell.length !== 0) {
                    let quill = previousCell.find('.text-editor').data("text-editor-child").quill;
                    let length = quill.getLength();
                    quill.setSelection(length-1, length-1, "user");
                }
            });
            let removeTableButton = $('<button class="btn editor-element"><i class="fa fa-trash-alt"></i></button>');
            removeTableButton.appendTo(content);
            removeTableButton.on("click",function() {
                let tableContainer = element.parents(".table-container");
                tooltip.removeTable(tableContainer);
            });
        } else {
            //Render the single creat table button only.
            content.css("width","48px");
            content.css("left","-24px");
            let addTableButton = $('<button class="btn editor-element"><i class="fas fa-table"></i></button>');
            addTableButton.appendTo(content);
            addTableButton.on("click",function() {
                let selection = window.getSelection();
                let anchorNode = $(selection.anchorNode);
                let editor = element.find(".ql-editor");
                let contents = editor.children();

                let index = editor.children().has(anchorNode).index();
                let wasEmpty = false;
                if(index === -1) {
                    index = editor.find(anchorNode).index();
                    wasEmpty = true;
                }
                let a = contents.slice(0,index + 1);
                let b = contents.not(a);

                let newContent = b.clone();

                b.remove();
                if(newContent.html() !== undefined && newContent.html() !== "") {
                    let newEditor = $('<div class="text-editor"></div>');
                    newContent.appendTo(newEditor);
                    newEditor.insertAfter(editor.parent());
                    new TextEditorChild(newEditor, tooltip.currentEditingChild.parent);

                }
                //Create the new Table
                let table = tooltip.createTable();
                table.insertAfter(editor.parent());
                table.find('.text-editor').each(function() {
                    new TextEditorChild($(this), tooltip.currentEditingChild.parent);
                });
                if(wasEmpty) {
                    if(editor.children().length === 1) {
                        editor.parent().data("text-editor-child").remove();
                    } else {
                        editor.children().eq(index).remove();
                    }
                }
            });
        }
        if(anchorNode.html() === undefined) {
            let range = selection.getRangeAt(0); //get the text range
            let position = range.getBoundingClientRect();
            let height = position.bottom - position.top;
            this.element.css("top",position.top - (element.offset().top - $(window).scrollTop()) + height + 10);
            this.element.css("left",position.left - element.offset().left);//position.left);
        } else {
            let position = anchorNode.position();
            let width = anchorNode.outerWidth();
            let height = anchorNode.height();
            this.element.css("top",position.top+height + 10);
            let alignment = anchorNode.css('textAlign');
            let offsetAlignment;
            switch (alignment) {
                case "right":
                    offsetAlignment = width;
                    break;
                case "center":
                    offsetAlignment = width/2;
                    break;
                default:
                    offsetAlignment = 0;
                    break;
            }
            this.element.css("left",position.left + offsetAlignment);
        }
        this.element.show();
    }



    setTableRowHeader(row, isHeader) {
        row.children("td,th").each(function() {
            let type = isHeader?"th":"td";
            let newElement = $('<'+type+"></"+type+">");
            newElement.insertBefore($(this));
            $(this).contents().appendTo(newElement);
            $(this).remove();
        });
    }

    duplicateRow(row) {
        let tooltip = this;
        tooltip.hide();
        let newRow = row.clone();
        newRow.find(".text-editor").each(function() {
            $(this).html("Cell");
        });
        tooltip.setTableRowHeader(newRow,false);
        newRow.find(".text-editor").each(function() {
            new TextEditorChild($(this),tooltip.currentEditingChild.parent);
        });
        return newRow;
    }

    removeTable(tableContainer) {
        let tooltip = this;
        tableContainer.find(".text-editor").each(function() {
            $(this).data("text-editor-child").remove();
        });
        tableContainer.remove();
        tooltip.currentEditingChild.parent.checkEmpty();
    }

    createTable() {
        return $('<div class="table-container table-responsive"><table class="table table-striped"><tr><th><div class="text-editor"><p>Cell</p></div></th><th><div class="text-editor"><p>Cell</p></div></th></tr><tr><td><div class="text-editor"><p>Cell</p></div></td><td><div class="text-editor"><p>Cell</p></div></td></tr></table></div>');
    }
}