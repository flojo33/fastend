import {Modal} from "../modal";
import {TextEditorChild} from "./text-editor-child";
import {TextEditorTooltip} from "./text-editor-tooltip";
import {FileSelector} from "../file-selector";

/**
 * Custom Text editor class built using the quill editor. To allow tables to be implemented, multiple quill editors
 * are created internally. These are stored in the TextEditorChild component.
 */
class TextEditor {

    /**
     * Initialize the required editors like a link modal and the table editor.
     */
    static initialize() {
        if(this.initialized) return;
        this.initialized = true;
        /**
         * Stores a link to the current text modules quill element to save link values correctly.
         * @type {Quill|null}
         */
        this.currentQuill = null;

        this.tableTooltip = new TextEditorTooltip();

        this.initializeLinkModal();
    }

    static initializeLinkModal() {
        let body = $('<div>');

        let selectFormGroup = $('<div class="form-group"><label for="textModuleLinkModuleSelectInput"></label></div>');
        let selectInput = $('<select class="form-control" id="textModuleLinkModuleSelectInput"><option value="0">Web Link</option><option value="1">Page Link</option><option value="2">File Download</option></select>');
        selectInput.appendTo(selectFormGroup);
        selectFormGroup.appendTo(body);

        let normalFormGroup = $('<div class="form-group"><label for="textModuleLinkModuleNormalInput">Destination URL</label></div>');
        let normalInput = $('<input type="text" class="form-control" id="textModuleLinkModuleNormalInput" placeholder="https://link.com">');
        normalInput.appendTo(normalFormGroup);
        normalFormGroup.appendTo(body);

        let selectPageFormGroup = $('<div class="form-group"><label for="textModuleLinkModulePageSelectInput">Internal Page</label></div>');
        let selectPageInput = $('<select class="form-control" id="textModuleLinkModulePageSelectInput"></select>');
        selectPageInput.appendTo(selectPageFormGroup);
        selectPageFormGroup.appendTo(body);

        let selectFileFormGroup = $('<div class="form-group"><label for="textModuleLinkModuleFileSelectInput">File to Download</label></div>');
        let selectFilePreviewInput = $('<div class="file-preview">No File Selected</div>');
        selectFilePreviewInput.on("click",function() {
            FileSelector.show(function(id, type) {
                selectFileInput.val(id);
                $.ajax({
                    type: 'POST',
                    url: '/backend/ajax/get-file-data.php',
                    data: {id:id},
                    success: function (data) {
                        if(data !== undefined) {
                            selectFilePreviewInput.html("<i class='fa fa-"+type+"'></i> "+data.name);
                        } else {
                            selectFilePreviewInput.html("Error getting File Information.");
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            })
        });
        selectFilePreviewInput.appendTo(selectFileFormGroup);
        let selectFileInput = $('<input class="form-control" type="hidden" id="textModuleLinkModuleFileSelectInput">');
        selectFileInput.appendTo(selectFileFormGroup);
        selectFileFormGroup.appendTo(body);

        let selectTypeFormGroup = $('<div class="form-group"><label for="textModuleLinkModuleTypeSelectInput">Style</label></div>');
        let selectTypeInput = $('<select class="form-control" id="textModuleLinkModuleTypeSelectInput"><option value="0">Link</option><option value="1">Primary Button</option><option value="2">Secondary Button</option></select>');
        selectTypeInput.appendTo(selectTypeFormGroup);
        selectTypeFormGroup.appendTo(body);

        $.ajax({
            type: 'POST',
            url: '/backend/ajax/get-pages.php',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    addPage(data[i], "");
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
        /**
         * @param page.id integer
         * @param page.urlName string
         * @param page.children page
         */
        let addPage = function (page, root) {
            $('<option value="' + page.id + '">' + root + page.urlName + '</option>').appendTo(selectPageInput);
            for (let i = 0; i < page.children.length; i++) {
                addPage(page.children[i], root + page.urlName + "/");
            }
        };

        let checkSelectInput = function () {
            let value = parseInt(selectInput.val());
            normalFormGroup.toggle(value === 0);
            selectPageFormGroup.toggle(value === 1);
            selectFileFormGroup.toggle(value === 2);
        };

        selectInput.on("change", function () {
            checkSelectInput();
        });

        checkSelectInput();

        let footer = $('<div>');
        //Buttons for canceling and saving the link
        let closeButton = $('<button class="btn btn-default">Cancel</button>');
        let closeButtonTitle = $('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        let removeButton = $('<button class="btn btn-danger">Remove Link</button>');
        let saveButton = $('<button class="btn btn-success">Save</button>');
        closeButton.appendTo(footer);
        removeButton.appendTo(footer);
        saveButton.appendTo(footer);

        closeButtonTitle.on("click", function () {
            TextEditor.linkModal.hide();
        });

        removeButton.on("click", function () {
            TextEditor.currentQuill.format('link', "");
            TextEditor.linkModal.hide();
        });

        closeButton.on("click", function () {
            TextEditor.linkModal.hide();
        });

        saveButton.on("click", function () {
            let buttonPrefix = selectTypeInput.val() === "1" ? "button-1:" : (selectTypeInput.val() === "2" ? "button-2:" : "");
            switch (parseInt(selectInput.val())) {
                case 0:
                    if (normalInput.val().startsWith("https://") || normalInput.val().startsWith("http://") || normalInput.val().startsWith("www.")) {
                        if (normalInput.val() === "") buttonPrefix = "";
                        TextEditor.currentQuill.format('link', buttonPrefix + normalInput.val());
                        TextEditor.linkModal.hide();
                    } else {
                        alert('The link must start with "http://" "https://" or "www."!');
                    }
                    break;
                case 1:
                    TextEditor.currentQuill.format('link', buttonPrefix + "internal:" + selectPageInput.val());
                    TextEditor.linkModal.hide();
                    break;
                case 2:
                    TextEditor.currentQuill.format('link', buttonPrefix + "file:" + selectFileInput.val());
                    TextEditor.linkModal.hide();
                    break;
            }
        });

        this.linkModal = new Modal("textModuleLinkModal")
            .setTitle("Setup Link")
            .setContent(body)
            .setFooter(footer);
    }

    /**
     *
     * @param {Quill} quill
     */
    static showLinkModal(quill) {
        //Trim selection to simplify link selection with double click!
        let selection = window.getSelection();

        //Trim the current Selection to contain start and end with text. this is helpful for finding links correctly

        while(selection.toString().trim() !== "" && selection.toString().trimRight() !== selection.toString()) {
            //Trim right!
            let startNode = selection.getRangeAt(0).startContainer;
            let startOffset = selection.getRangeAt(0).startOffset;
            let endNode = selection.getRangeAt(selection.rangeCount-1).endContainer;
            let endOffset = selection.getRangeAt(selection.rangeCount-1).endOffset - 1;
            while(endOffset < 0) {
                let newEndNode = endNode.previousSibling;
                //Climb out
                while(newEndNode == null) {
                    endNode = endNode.parentNode;
                    newEndNode = endNode.previousSibling;
                }
                //Climb in
                while(newEndNode.childNodes.length > 0) {
                    newEndNode = newEndNode.lastChild;
                }
                endNode = newEndNode;
                if(endNode.nodeValue == null) {
                    endOffset = -1;
                } else {
                    endOffset = endNode.nodeValue.length;
                }
            }
            selection.setBaseAndExtent(startNode, startOffset, endNode, endOffset);
        }


        while(selection.toString().trim() !== "" && selection.toString().trimLeft() !== selection.toString()) {
            //Trim Left!
            let startNode = selection.getRangeAt(0).startContainer;
            let startOffset = selection.getRangeAt(0).startOffset + 1;
            let endNode = selection.getRangeAt(selection.rangeCount-1).endContainer;
            let endOffset = selection.getRangeAt(selection.rangeCount-1).endOffset;
            if(startNode.nodeValue === null) startOffset = -1;
            while(startOffset === -1 || startOffset > startNode.nodeValue.length) {
                let newStartNode = startNode.nextSibling;
                //Climb out
                while(newStartNode == null) {
                    startNode = startNode.parentNode;
                    newStartNode = startNode.nextSibling;
                }
                //Climb in
                while(newStartNode.childNodes.length > 0) {
                    newStartNode = newStartNode.lastChild;
                }
                startNode = newStartNode;
                if(newStartNode.nodeValue == null) {
                    startOffset = -1;
                } else {
                    startOffset = 0;
                }
            }
            selection.setBaseAndExtent(startNode, startOffset, endNode, endOffset);
        }

        if (selection.toString().trim() === "") return;

        let aNode = $(selection.focusNode).parent("a");
        let currentValue = "";
        console.log(aNode);
        if(aNode.length) {
            currentValue = aNode.attr("href");
        } else {
            let aNode = $(selection.focusNode).prev();
            if(aNode !== null) {
                currentValue = aNode.attr("href");
            }
        }
        /*
        Check the current type of link
         */
        TextEditor.currentQuill = quill;
        let modalElement = this.linkModal.element;
        if (currentValue !== undefined) {
            let split = currentValue.split(":");
            if (split.includes("internal")) {
                modalElement.find('#textModuleLinkModuleSelectInput').val("1");
                modalElement.find('#textModuleLinkModulePageSelectInput').val(split[(split.includes("button-1") || split.includes("button-2")) ? 2 : 1]);
            } else {
                if (split.includes("file")) {
                    let file = split[(split.includes("button-1") || split.includes("button-2")) ? 2 : 1];
                    modalElement.find('#textModuleLinkModuleFileSelectInput').val(file);

                    $.ajax({
                        type: 'POST',
                        url: '/backend/ajax/get-file-data.php',
                        data: {id:file},
                        success: function (data) {
                            if(data !== undefined) {
                                modalElement.find('.file-preview').html(data.name);
                            } else {
                                modalElement.find('.file-preview').html("Error getting File Information.");
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });

                    modalElement.find('#textModuleLinkModuleSelectInput').val("2");
                } else {
                    modalElement.find('#textModuleLinkModuleNormalInput').val(currentValue.replace("button-1:", "").replace("button-2:", ""));
                    modalElement.find('#textModuleLinkModuleSelectInput').val("0");
                }
            }
            modalElement.find('#textModuleLinkModuleTypeSelectInput').val(split.includes("button-1") ? 1 : (split.includes("button-2") ? 2 : 0));
        } else {
            modalElement.find('#textModuleLinkModuleNormalInput').val(currentValue);
            modalElement.find('#textModuleLinkModuleSelectInput').val("0");
            modalElement.find('#textModuleLinkModuleTypeSelectInput').val("0");
        }
        modalElement.find('#textModuleLinkModuleSelectInput').trigger("change");
        this.linkModal.show();
    };

    /**
     *
     * @param {jQuery} parentContainer
     */
    static prepareForSavingInChildren(parentContainer) {
        parentContainer.find(".text-editor").each(function() {
            TextEditorChild.prepareForSaving($(this));
        });
    }

    constructor(element, boundaryContainer) {
        TextEditor.initialize();
        let textEditor = this;
        textEditor.element = element;
        textEditor.boundaryContainer = boundaryContainer;
        textEditor.children = [];
        textEditor.element.find('.text-editor').each(function() {
            new TextEditorChild($(this),textEditor);
        });

        this.checkEmpty();
    }

    checkRemoved() {
        let newChildren = [];
        for(let i = 0; i < this.children.length; i++) {
            if(!this.children[i].removed) {
                newChildren.push(this.children[i]);
            }
        }
        this.children = newChildren;
    }

    checkEmpty() {
        this.checkRemoved();
        if(this.children.length === 0) {
            console.log("Creating new text editor because no more where left!");
            let newChild = TextEditorChild.create(this);
            this.element.append(newChild.element);
        }
    }

}

export { TextEditor }