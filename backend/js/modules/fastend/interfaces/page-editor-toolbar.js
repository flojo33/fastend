export class PageEditorToolbar {
    static createButton(tooltip, content, click) {
        let button = $('<button class="btn btn-sm"></button>');
        button.append(content);
        button.attr('data-toggle',"tooltip");
        button.attr('data-placement',"top");
        button.attr('title',tooltip);
        button.on('click',() => {
            button.tooltip('hide');
            click()
        });
        button.tooltip();
        return button;
    }
}