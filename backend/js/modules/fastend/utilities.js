class Utilities {
    /**
     * remove all unnecessary spaces between elements
     * @param {jQuery} el
     */
    static minify(el) {
        el.html(el.html().replace(/\t/ig, "").replace(/\n/ig, "").replace(/\s\s+/ig, ""));
    };

    /**
     * prettify html the content of an element
     * @param {jQuery} el
     */
    static prettify(el) {
        let utilities = this;
        if (el.parent().length > 0 && el.parent().data('assign')) {
            el.data('assign', el.parent().data('assign') + 1);
        } else {
            el.data('assign', 1);
        }

        if (el.children().length > 0) {
            el.children().each
            (
                function () {
                    if ($(this).is("a")) return;
                    let tbc = '';

                    for (let i = 0; i < $(this).parent().data('assign'); i++) {
                        tbc += '\t';
                    }
                    $(this).before('\n' + tbc);
                    if ($(this).is("textarea") || $(this).is("label")) return;
                    $(this).prepend('\t');
                    $(this).append('\n' + tbc);
                    utilities.prettify($(this));
                }
            );
        } else {
            let tbc = '';

            for (let i = 0; i < el.parent().data('assign'); i++) {
                tbc += '\t';
            }
            el.prepend('\n' + tbc);
        }
    };

    /**
     * Use Prettify and Minify to clean up a section of html
     * @param {jQuery} element
     */
    static cleanElementCode(element) {
        this.minify(element);
        this.prettify(element);
    };
}
export { Utilities }