import {Column} from './column.js';

class Row {
    /**
     *
     * @param {jQuery} parent
     * @param {ModuleController} moduleController
     */
    static build(parent, moduleController) {
        parent.find('.row.row-editable:not(.setup)').each(function() {
            $(this).row = new Row($(this), moduleController);
        });
    }

    /**
     *
     * @param {jQuery} element
     * @param {ModuleController} moduleController
     */
    constructor(element, moduleController) {
        this.element = element;
        this.controller = moduleController;
        element.addClass("setup");
        this.update();
    }

    update() {
        if(!this.element.children(".col").length) {
            this.element.append('<div class="col col-12 col-md-12"><div class="module"></div>');
        }
        Column.build(this);
    }
}

export {Row}