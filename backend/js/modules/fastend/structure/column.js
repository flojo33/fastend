import {ScreenSizes} from "../page-size";

class Column {
    /**
     *
     * @param {Row} row
     */
    static build(row) {
        row.element.find('.col:not(.setup)').each(function() {
            $(this).data("column",new Column($(this), row));
        });
    }

    /**
     *
     * @param element
     * @param {Row} row
     */
    constructor(element, row) {
        element.addClass("setup");
        this.element = element;
        this.row = row;
        this.module = null;
        element.on('click',() => row.controller.setSelectedModule(this.module));
        this.reloadModule(false);
    }

    reloadModule(initial) {
        this.module = this.row.controller.initializeExisting(this, initial);
    }

    replaceModule(type) {
        this.element.html("");
        this.element.append($('<div class="module module-'+type+'"></div>'));
        this.reloadModule(true);
        this.row.controller.setSelectedModule(this.module);
    }

    remove() {
        this.element.remove();
        this.row.update();
        this.row.controller.deselectModule();
    }

    switchLeft() {
        let prev = this.element.prev();
        if(prev !== null) {
            prev.insertAfter(this.element);
        }
    }

    switchRight() {
        let next = this.element.next();
        if(next !== null) {
            next.insertBefore(this.element);
        }
    }

    insertNew() {
        let column = $('<div class="col col-12 col-md-6"><div class="module"></div></div>');
        column.insertAfter(this.element);
        Column.build(this.row);
        column.data("column").row.controller.setSelectedModule(column.data("column").module);
    }

    /**
     *
     * @param {PageSizeController} pageSizeController
     */
    increaseSize(pageSizeController) {
        let size = this.findRelevantSizeClass(pageSizeController.getCurrentSize());
        this.setSize(pageSizeController, size + 1);
    }

    /**
     *
     * @param {PageSizeController} pageSizeController
     */
    decreaseSize(pageSizeController) {
        let size = this.findRelevantSizeClass(pageSizeController.getCurrentSize());
        this.setSize(pageSizeController, size - 1);
    }

    setSize(pageSizeController, size) {
        let currentPageSize = ScreenSizes[pageSizeController.getCurrentSize()];
        let currentSize = this.findRelevantSizeClass(pageSizeController.getCurrentSize());
        if (size <= 12 && size >= currentPageSize.minimumColumnSize) {
            this.element.removeClass("col" + currentPageSize.classPrefix + "-" + currentSize);
            this.element.addClass("col" + currentPageSize.classPrefix + "-" + size);
        }
    }

    /**
     * Returns the currently relevant order of a column given its classes and a specified page Size (0 = xs, 4 = xl)
     * @param {number} pageSize
     * @returns {number}
     */
    findRelevantOrderClass(pageSize) {
        let classes = this.element.prop("classList");
        for (let i = pageSize; i >= 0; i--) {
            for (let j = 12; j > 0; j--) {
                if (classes.contains("order" + ScreenSizes[i].classPrefix + "-" + j)) {
                    return j;
                }
            }
        }
        return 0;
    };

    /**
     * Returns the currently relevant size of a column given its classes and a specified page Size (0 = xs, 4 = xl)
     * @param {number} pageSize
     * @returns {number}
     */
    findRelevantSizeClass(pageSize) {
        let classes = this.element.prop("classList");
        for (let i = pageSize; i >= 0; i--) {
            for (let j = 12; j > 0; j--) {
                if (classes.contains("col" + ScreenSizes[i].classPrefix + "-" + j)) {
                    return j;
                }
            }
        }
        return 0;
    };
}

export {Column}