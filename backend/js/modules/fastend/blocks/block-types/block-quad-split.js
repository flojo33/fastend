import {Block} from "../block";

class BlockQuadSplit extends Block {

    static getName() {
        return "Quad Split Block";
    }

    static getDescription() {
        return "A block containing four columns next to each other. DEPRECATED!";
    }

    static getType() {
        return "quad-split";
    }

    constructor(controller, element, initial) {
        super(controller, element, initial);
        this.element.addClass("block-quad-split");
    }

    buildHtml() {
        this.element.html([
            '<div class="container">',
            '   <div class="row">',
            '       <div class="col col-12 col-md-6 col-lg-3">',
            '          <div class="mb-2"><div class="module module-image">',
            '          </div></div>',
            '          <div><div class="module module-text">',
            '              <div class="vertical-align">',
            '                  <h3 class="text-center">Title</h3><p class="text-center">Message</p>',
            '              </div>',
            '          </div></div>',
            '       </div>',
            '       <div class="col col-12 col-md-6 col-lg-3">',
            '          <div class="mb-2"><div class="module module-image">',
            '          </div></div>',
            '          <div><div class="module module-text">',
            '              <div class="vertical-align">',
            '                  <h3 class="text-center">Title</h3><p class="text-center">Message</p>',
            '              </div>',
            '          </div></div>',
            '       </div>',
            '       <div class="col col-12 col-md-6 col-lg-3">',
            '          <div class="mb-2"><div class="module module-image">',
            '          </div></div>',
            '          <div><div class="module module-text">',
            '              <div class="vertical-align">',
            '                  <h3 class="text-center">Title</h3><p class="text-center">Message</p>',
            '              </div>',
            '          </div></div>',
            '       </div>',
            '       <div class="col col-12 col-md-6 col-lg-3">',
            '          <div class="mb-2"><div class="module module-image">',
            '          </div></div>',
            '          <div><div class="module module-text">',
            '              <div class="vertical-align">',
            '                  <h3 class="text-center">Title</h3><p class="text-center">Message</p>',
            '              </div>',
            '          </div>',
            '       </div>',
            '    </div>',
            '</div>'
        ].join('\n'));
    }

    onDeselect() {
        super.onDeselect();
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} block
     */
    static prepareForSaving(block) {
        if (block.attr("data-background-image") !== undefined && block.hasClass("background-image")) {
            let image = block.attr("data-background-image");
            let crop = block.attr("data-background-image-crop");
            block.css("background-image", 'Url("/media/uploads/image/crop_' + image + '_' + crop + '.png")');
        }
    }
}

export { BlockQuadSplit }