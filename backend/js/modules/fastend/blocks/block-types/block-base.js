import {Block} from "../block";

class BlockBase extends Block {

    static getName() {
        return "Base Block";
    }

    static  getDescription() {
        return "A block used to create the most types of website elements. Its vertical and horizontal size and spacing can be adjusted to fit most requirements.";
    }

    static getType() {
        return "base";
    }

    constructor(controller, element, initial) {
        super(controller, element, initial);
        this.element.addClass("block-base");
    }

    buildHtml() {
        this.element.html([
            '<div class="container">',
            '    <div class="row row-editable"></div>',
            '</div>'
        ].join('\n'));
    }

    onDeselect() {
        super.onDeselect();
    }

    getEditModalContent() {
        let content = super.getEditModalContent();

        return content;
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} block
     */
    static prepareForSaving(block) {
        if (block.attr("data-background-image") !== undefined && block.hasClass("background-image")) {
            let image = block.attr("data-background-image");
            let crop = block.attr("data-background-image-crop");
            block.css("background-image", 'Url("/media/uploads/image/crop_' + image + '_' + crop + '.png")');
        }
        block.find(".particle-container").each(function() {
            $(this).html("");
            console.log("cleared!");
        });
    }
}

export { BlockBase }