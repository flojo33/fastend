import {PageEditorToolbar} from "../interfaces/page-editor-toolbar";
import {ModalAlert} from "../interfaces/modal-alert";
import {Modal} from "../interfaces/modal";

export class PageEditorBlockToolbar {
    /**
     *
     * @param {BlockController} controller
     * @param container
     */
    constructor(controller, container) {
        this.controller = controller;
        this.container = container;
        this.element = $('<div class="row justify-content-center">');

        let editModal = new Modal("blockEditModal").setTitle("Edit Block settings");

        this.buttons = [];


        this.buttons.push(PageEditorToolbar.createButton('Move Block up',$('<i class="fa fa-chevron-up"></i>'),function() {
            controller.getSelected().switchUp();
        }));

        this.buttons.push(PageEditorToolbar.createButton('Move Block down',$('<i class="fa fa-chevron-down"></i>'),function() {
            controller.getSelected().switchDown();
        }));

        this.buttons.push(PageEditorToolbar.createButton('Add Block', $('<i class="fa fa-plus"></i>'),function() {
            controller.showSelectModal("after");
        }));

        this.buttons.push(PageEditorToolbar.createButton('Remove Block', $('<i class="fa fa-trash-alt"></i>'),function() {
            ModalAlert.confirm("Remove Block","Do you really want to remove this block?",function() {
                controller.getSelected().remove();
            }, () => {});
        }));

        this.buttons.push(PageEditorToolbar.createButton('Edit Block', $('<i class="fa fa-cog"></i>'),function() {
            editModal.setContent(controller.getSelected().getEditModalContent()).show();
        }));

        for(let i = 0; i < this.buttons.length; i++) {
            this.element.append(this.buttons[i]);
        }
        container.append(this.element);
    }

    update() {
        let active = this.controller.getSelected() !== null;
        for(let i = 0; i < this.buttons.length; i++) {
            this.buttons[i].prop("disabled", !active);
        }
        this.container.toggleClass("d-block", active);
    }
}