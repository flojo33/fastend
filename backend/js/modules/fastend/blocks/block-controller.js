import * as BlockType from './block-types.js';
import { Modal } from '../interfaces/modal';
import { Row } from "../structure/row";

class BlockController {
    /**
     *
     * @param {jQuery} parentContainer
     * @param {ModuleController} moduleController
     * @param {function} onSelectionChange
     */
    constructor(parentContainer, contentContainer, moduleController, onSelectionChange) {
        this.contentContainer = contentContainer;
        this.onSelectionChange = onSelectionChange;
        this.moduleController = moduleController;
        this.typeClasses = {};
        /**
         *
         * @type {block|null}
         */
        this.selectedBlock = null;
        this.parentContainer = parentContainer;
        this.blocks = [];
        let controller = this;

        let modalContent = $('<div class="block-select">');

        for(let type in BlockType) {
            let className = 'block-'+BlockType[type].getType();
            this.typeClasses[className] = type;
            let addButton = $('<button class="btn btn-success w-100 mb-2">'+BlockType[type].getName()+'<br><small>'+BlockType[type].getDescription()+'</small></button>');
            addButton.on('click',() => this.finishBlockSelection(this.createBlockWithType(BlockType[type].getType())));
            modalContent.append(addButton);
        }

        this.selectModal = new Modal("blockSelectModal")
            .setTitle("Add new Block")
            .setContent(modalContent);

        this.initializeExisting();

        parentContainer.parent().on("click", function (e) {
            let target = $(e.target);
            if (!target.parents('.block').length && !target.hasClass('block')) {
                controller.deselectBlock();
            }
        });
    };

    finishBlockSelection(block) {
        this.selectModal.hide();
        switch (this.currentBlockInsertionType) {
            case "after":
                block.insertAfter(this.getSelected().element);
                break;
            case "before":
                block.insertBefore(this.getSelected().element);
                break;
            case "atEnd":
                block.appendTo(this.contentContainer);
                break;
        }
        this.setSelectedBlock(block.data("block"));
    }

    showSelectModal(insertionType) {
        this.currentBlockInsertionType = insertionType;
        this.selectModal.show();
    }

    addBlock(block) {
        this.blocks.push(block);
        Row.build(block.element,this.moduleController);
    }

    deselectBlock() {
        if(this.selectedBlock !== null) {
            this.selectedBlock.onDeselect();
            this.selectedBlock = null;
            this.onSelectionChange();
        }
    }

    setSelectedBlock(block) {
        if(this.selectedBlock === block) return;
        if(this.selectedBlock !== null) {
            this.selectedBlock.onDeselect();
        }

        this.selectedBlock = block;
        block.onSelect();
        this.onSelectionChange();
    }

    /**
     *
     * @returns {block|null}
     */
    getSelected() {
        return this.selectedBlock;
    }

    initializeExisting() {

        let controller = this;
        this.contentContainer.find('.block').each(function() {
            controller.createBlockWithClass($(this), false);
        });
    }

    createBlockWithType(type) {
        let element = $('<div class="block block-'+type+'"></div>');
        this.createBlockWithClass(element, true);
        return element;
    }

    createBlockWithClass(element, initial) {
        let controller = this;
        for(let classKey in controller.typeClasses) {
            if(element.hasClass(classKey)) {
                new BlockType[controller.typeClasses[classKey]](controller, element, initial);
                return;
            }
        }
        console.error("An invalid or unknown block type was found! Cannot initialize it.");
    }

    prepareForSaving(blockClone) {
        let controller = this;
        for(let classKey in controller.typeClasses) {
            if(blockClone.hasClass(classKey)) {
                BlockType[controller.typeClasses[classKey]].prepareForSaving(blockClone);
            }
        }
    }

    updateBlocks() {
        //TODO!
    }
}

export { BlockController }