import {FormUtilities} from "../interfaces/form-utilities";
import {ImageSelector} from "../interfaces/image-selector";

class Block {

    static getName() {
        return "Abstract Block";
    }

    static getDescription() {
        return "This should not be visible to the user!";
    }

    static getType() {
        return "abstract";
    }

    /**
     *
     * @param {BlockController} controller
     * @param {jQuery} element
     * @param {boolean} initial
     */
    constructor(controller, element, initial) {
        this.controller = controller;
        this.element = element;
        if(initial) {
            this.buildHtml();
        }
        //Todo Check if this is required or not...
        element.data("block",this);
        element.on('click',() => this.onClick());
        this.controller.addBlock(this);
    }

    getEditModalContent() {
        let content = $('<div class="edit-block-content">');

        content.append($('<h6>General Settings</h6>'));

        content.append(FormUtilities.createClassSelectInput("Vertical Height", this.element, "vertical-size-", ["content", "page"], ["Fit Content", "Full Page Height"], 0));
        content.append(FormUtilities.createClassSelectInput("Vertical Alignment", this.element, "vertical-align-", ["", "center", "bottom"], ["Top", "Center", "Bottom"], 0));
        content.append(FormUtilities.createClassSelectInput("Vertical Padding", this.element, "padding-vertical-", ["0", "1", "2", "3"], ["No Padding", "Small", "Medium", "Large"], 2));
        content.append(FormUtilities.createClassSelectInput("Horizontal Size", this.element.find(".container,.container-fluid"), "", ["container","container-fluid"], ["Container","Full Width"], 2));
        content.append(FormUtilities.createClassSelectInput("Horizontal Alignment", this.element.find(".row.row-editable"), "justify-content-", ["left", "center", "right"], ["Left", "Center", "Right"], 0));
        
        let backgroundSelectGroup = FormUtilities.createClassSelectInput("Background Type", this.element, "background-", ["blank", "jumbotron", "image", "particles"], ["Blank", "Jumbotron", "Image", "Particles"], 0);
        content.append(backgroundSelectGroup);
        content.append(FormUtilities.createClassSelectInput("Background Fade", this.element, "", ["", "background-fade"], ["No fade", "Fade"], 0));

        let idInput = $('<input class="form-control" type="text">');

        let idInputGroup = $('<div class="form-group"><label>Anchor ID</label></div>');
        idInputGroup.append(idInput);
        idInput.val(this.element.attr('id'));
        idInput.change(() => {
            this.element.attr('id', idInput.val());
        });

        content.append(idInputGroup);

        let backgroundSelect = backgroundSelectGroup.find('select');

        let backgroundEditContainer = $('<div id="background-edit"></div>');
        let getCurrentImageBackgroundSource = () => {
            let source;
            if (this.element.attr("data-background-vector") !== undefined) {
                let vector = this.element.attr("data-background-vector");
                source = "/media/uploads/vector/" + vector + ".svg";
            } else {
                if (this.element.attr("data-background-image") !== undefined) {
                    let image = this.element.attr("data-background-image");
                    let crop = this.element.attr("data-background-image-crop");
                    source = "/media/uploads/image/crop_" + image + "_" + crop + ".png";
                } else {
                    source = "/backend/icons/image_placeholder.png";
                }
            }
            return source;
        };
        let updateBackgroundView = () => {
            backgroundEditContainer.html("");
            this.element.find(".particle-container").each(function() {
                $(this).remove();
            });
            switch (backgroundSelect.val()) {
                case "0":
                    this.element.css('background-image', '');
                    break;
                case "1":
                    this.element.css('background-image', '');
                    backgroundEditContainer.append(FormUtilities.createClassSelectInput("Jumbotron Type", this.element, "jumbotron-", ["", "2", "3"], ["Type 1", "Type 2", "Type 3"], 0));
                    break;
                case "2":
                    let formGroup = $('<div class="form-group"><label for="header-background-image-select">Select Background Image</label></div>');
                    let currentSource = getCurrentImageBackgroundSource();
                    let imageSelect = $('<img src="' + currentSource + '" alt="header-background-image-select" id="header-background-image-select" style="display: block; height: 200px;">');
                    this.element.css('background-image', 'Url("' + currentSource + '")');
                    imageSelect.on('click', () => {
                        ImageSelector.show(2560, (type, id, cropId, raw) => {
                            if (type === "image") {
                                imageSelect.attr("src", raw);
                                this.element.attr("data-background-image", id);
                                this.element.attr("data-background-image-crop", cropId);
                                this.element.removeAttr("data-background-vector");
                                this.element.css('background-image', 'Url("' + raw + '")');
                            } else {
                                this.element.removeAttr("data-background-image");
                                this.element.removeAttr("data-background-image-crop");
                                this.element.attr("data-background-vector", id);
                                imageSelect.attr("src", "/media/uploads/vector/" + id + ".svg");
                                this.element.css('background-image', 'Url("/media/uploads/vector/' + id + '.svg")');
                            }
                        });
                    });
                    imageSelect.appendTo(formGroup);
                    backgroundEditContainer.append(formGroup);
                    break;
                case "3":
                    //Particles
                    this.element.css('background-image', '');
                    //backgroundEditContainer.append(FormUtilities.createClassSelectInput("Jumbotron Type", this.element, "jumbotron-", ["", "2", "3"], ["Type 1", "Type 2", "Type 3"], 0));
                    initParticles(this.element);
                    break;
                default:
                    this.element.css('background-image', '');
                    break;
            }
        };

        backgroundSelect.on("change", updateBackgroundView);

        updateBackgroundView();

        backgroundEditContainer.appendTo(content);

        return content;
    }

    onClick() {
        this.controller.setSelectedBlock(this);
    }

    onSelect() {
        this.element.addClass("selected");
    }

    onDeselect() {
        this.element.removeClass("selected");
    }

    buildHtml() {
        this.element.html('');
    }

    remove() {
        this.element.remove();
        this.controller.deselectBlock();
        this.controller.updateBlocks();
        this.controller.moduleController.deselectModule();
    }
    switchUp() {
        let prev = this.element.prev();
        if(prev !== null) {
            prev.insertAfter(this.element);
        }
    }

    switchDown() {
        let next = this.element.next();
        if(next !== null) {
            next.insertBefore(this.element);
        }
    }

    /**
     * Prepare a block for saving.
     * @param {jQuery} block
     */
    static prepareForSaving(block) {
    }
}

export {Block};