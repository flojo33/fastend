$.fn.mediaBrowser = function (locationLabel, uploadContainer, actionContainer, addLocationButton, selectFunction) {

    //region Global Variables
    const mediaLibrary = $(this);
    let currentLocation = 0;
    const hasUploadContainer = uploadContainer !== undefined && uploadContainer !== null;
    const hasActionContainer = actionContainer !== undefined && actionContainer !== null;
    const hasAddLocationButton = addLocationButton !== undefined && addLocationButton !== null;
    let addLocationModal;
    let currentFolderNames = [];
    //endregion Global Variables

    //region uploader
    let buildUploadForm = function(){

        let uploadInput = $('<input hidden id="fileupload" type="file" name="uploads[]" multiple >');
        let idInput = $('<input hidden type="number" id="locationIdInput" name="locationId" value="0">');

        let uploadButton = $('<button type="button" class="btn btn-sm btn-outline-success" id="upload-button"><i class="fa fa-upload"></i> Upload Files</button>');

        let progressContainer = $('<div class="progress" style="width: 200px; height: 32px"></div>');
        let progressTitle = $('<div class="progress-title">Upload Status</div>');
        progressTitle.appendTo(progressContainer);
        let progressBar = $('<div class="progress-bar bg-success" id="progressbar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>');
        progressBar.appendTo(progressContainer);
        let status = $('<div id="uploadStatus"></div>');
        progressContainer.appendTo(status);

        let form = $('<form method="post" action="/backend/ajax/upload-files.php" id=\'imageuploadform\' enctype="multipart/form-data"></form>');

        uploadInput.appendTo(form);
        idInput.appendTo(form);
        uploadButton.appendTo(form);
        status.appendTo(form);

        uploadButton.click(function(e) {

            uploadInput.click();
            e.preventDefault();

        });


        uploadInput.change(function (e) {

            form.submit();
            e.preventDefault();
        });

        form.submit(function(e) {

            uploadButton.hide();
            status.show();
            idInput.val(currentLocation);
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: '/backend/ajax/upload-files.php',
                data:formData,
                xhr: function() {
                    let myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                cache:false,
                contentType: false,
                processData: false,

                success:function(data){
                    console.log(data);
                    for(let i = 0; i < data.length; i++) {
                        if(data[i]["status"] !== "success") {
                            alert(data[i]["status"]);
                        }
                    }
                    //location.reload();
                    uploadButton.show();
                    status.hide();
                    reloadLocation();
                },

                error: function(data){
                    console.log(data);
                    uploadButton.show();
                    status.hide();
                    reloadLocation();
                }
            });

            e.preventDefault();

        });
        uploadButton.show();
        status.hide();
        function progress(e){

            progressContainer.show();
            if(e.lengthComputable){
                let max = e.total;
                let current = e.loaded;

                let Percentage = (current * 100)/max;
                progressBar.css('width', Percentage+'%').attr('aria-valuenow', Percentage);
            }
        }
        return form;
    };
    //endregion uploader

    let loadLocation = function(locationId) {
        currentLocation = locationId;
        $.ajax({
            type : 'POST',
            url : '/backend/ajax/get-media-library.php',
            data: {locationId:locationId},
            success : function (data) {
                rebuildLocations(data);
            },
            error : function(data) {
                console.log(data);
            }
        });
    };

    let reloadLocation = function() {
        loadLocation(currentLocation);
    };

    let rebuildLocations = function(data) {
        //console.log(data);
        mediaLibrary.html('');
        locationLabel.html('');
        setCheckedButtons();
        let backButton = $('<a href="#"><i class="fa fa-chevron-left"></i></a>');
        backButton.on('click',function() {
            loadLocation(data.currentLocation.parent);
        });
        if(data.currentLocation.id !== 0) {
            backButton.appendTo(locationLabel);
        }
        currentFolderNames = [];
        $('<span> '+data.currentLocation.name+'</span>').appendTo(locationLabel);
        for(let i = 0; i < data.locations.length; i++) {
            buildElement(data.locations[i], "location").appendTo(mediaLibrary);
            currentFolderNames.push(data.locations[i].name);
        }
        for(let i = 0; i < data.files.length; i++) {
            buildElement(data.files[i], data.files[i].type).appendTo(mediaLibrary);
        }
    };

    let buildElement = function(data, type) {
        let element = $('<div class="col-6 col-md-4 col-lg-3 col-xl-2"></div>');
        let locationItem = $('<div class="location-item"></div>');
        locationItem.attr('data-id',data.id);
        locationItem.attr('data-type',type);
        locationItem.appendTo(element);
        let inner;
        if(type === "location"){
            inner = $('<div class="inner"><div class="icon"><i class="fa fa-folder"></i></div><div class="name">'+data.name+'</div></div>');
        } else {
            inner = $('<div class="inner"><div class="icon">'+data.thumbnail+'<div class="name">'+data.name+'</div></div>');
        }
        inner.appendTo(locationItem);
        if(hasActionContainer) {
            let checkbox = $('<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="checkbox-image-'+data.id+'"><label class="custom-control-label" for="checkbox-image-'+data.id+'"></label></div>');
            checkbox.appendTo(locationItem);
            checkbox.on("change",function() {
                setCheckedButtons();
            });
        }
        //TODO make this nicer...
        inner.on('click',function() {
            if(type === "location") {
                loadLocation(data.id);
            } else {
                selectFunction(data.id, type);
            }
        });
        return element;
    };

    let setCheckedButtons = function() {
        if(!hasActionContainer) {
            return;
        }
        let count = 0;
        mediaLibrary.find(':checked').each(function() {
            count ++;
        });
        actionContainer.find('.selected-counter').html(count);
        actionContainer.toggleClass("hidden",count <= 0);
    };

    let setupAddLocationModal = function() {
        addLocationModal = $(
            '<div class="modal fade" id="addLocationModal" tabindex="-1" role="dialog" aria-labelledby="addLocationModalLabel" aria-hidden="true">\n' +
            '  <div class="modal-dialog" role="document">\n' +
            '    <div class="modal-content">\n' +
            '      <div class="modal-header">\n' +
            '        <h5 class="modal-title" id="addLocationModalLabel">Create Folder</h5>\n' +
            '        <button type="button" class="close" id="cancel-button" aria-label="Close">\n' +
            '          <span aria-hidden="true">&times;</span>\n' +
            '        </button>\n' +
            '      </div>\n' +
            '      <div class="modal-body"></div>\n' +
            '      <div class="modal-footer">\n' +
            '        <button type="button" id="cancel-button" class="btn btn-secondary">Cancel</button>\n' +
            '        <button type="button" id="add-button" class="btn btn-success">Create Folder</button>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '  </div>\n' +
            '</div>'
        );
        let body = addLocationModal.find('.modal-body');

        let formGroup = $('<div class="form-group"><label for="addLocationName">Folder Name</label></div>');

        let nameInput = $('<input type="text" class="form-control" id="location-name-input" placeholder="New Folder Name">');
        let invalidDisplay = $('<div class="invalid-feedback">A Folder with this name already exists! Please create a folder with a unique name.</div>');
        invalidDisplay.hide();
        let confirmButton = addLocationModal.find('#add-button');
        confirmButton.on("click",function() {
            if(currentFolderNames.includes(nameInput.val())) {
                nameInput.addClass('is-invalid');
                invalidDisplay.show();
            } else {
                addLocation(nameInput.val());
                addLocationModal.modal('hide');
            }
        });
        addLocationModal.find('#cancel-button').each(function() {
            $(this).on('click',function() {
                addLocationModal.modal('hide');
            });
        });

        nameInput.appendTo(formGroup);
        invalidDisplay.appendTo(formGroup);
        formGroup.appendTo(body);

        addLocationModal.appendTo(mediaLibrary.parent());
    };

    let addLocation = function (name) {
        $.ajax({
            type : 'POST',
            url : '/backend/ajax/add-location.php',
            data: {locationId:currentLocation, name:name},
            success : function (data) {
                reloadLocation();
            },
            error : function(data) {
                console.log(data);
            }
        });
    };

    let showAddLocationModal = function() {
        addLocationModal.find('#location-name-input').val("").removeClass('is-invalid');
        addLocationModal.find('.invalid-feedback').hide();
        addLocationModal.modal('show');
    };
    let setupActionButtons = function() {
        let deleteAllButton = actionContainer.find('#delete-all-button');
        deleteAllButton.on('click',function() {
            if(!confirm("Are you sure that you want to delete all selected items? This will include all files in all selected directories! If any of the files are used in the website, the website will not be able to display the images anymore!")) return;
            let elements = [];
            mediaLibrary.find(':checked').each(function() {
                let locationItem = $(this).parent().parent();
                elements.push({
                    id:locationItem.attr("data-id"),
                    type:locationItem.attr("data-type")
                });
            });
            $.ajax({
                type : 'POST',
                url : '/backend/ajax/remove-elements.php',
                data: {elements:JSON.stringify(elements)},
                success : function (data) {
                    console.log(data);
                    reloadLocation();
                },
                error : function(data) {
                    console.log(data);
                }
            });
        });
    };
    let bindEvents = function() {
        mediaLibrary.bind("reset",function() {
            loadLocation(0);
        });
    };

    let initialize = function() {
        if(hasAddLocationButton) {
            setupAddLocationModal();
            addLocationButton.on('click',function() {
                showAddLocationModal();
            });
        }
        if(uploadContainer !== undefined) {
            buildUploadForm().appendTo(uploadContainer);
        }
        if(hasActionContainer) {
            setupActionButtons();
            setCheckedButtons();
        }
        bindEvents();
        loadLocation(0);
    };

    initialize();
};