const Link = Quill.import('formats/link');
let setButton = function(button, node) {
    switch(button) {
        default:
            $(node).removeClass("btn");
            $(node).removeClass("btn-primary");
            $(node).removeClass("btn-secondary");
            break;
        case 1:
            $(node).addClass("btn");
            $(node).addClass("btn-primary");
            $(node).removeClass("btn-secondary");
            break;
        case 2:
            $(node).addClass("btn");
            $(node).addClass("btn-secondary");
            $(node).removeClass("btn-primary");
            break;
    }
};

let getLinkData = function(value) {
    let split = value.split(":");
    let button = 0;
    let internal = false;
    let file = false;
    let link = "";

    for(let i = 0; i < split.length; i++) {
        switch (split[i]) {
            case "button-1":
                button = 1;
                break;
            case "button-2":
                button = 2;
                break;
            case "internal":
                internal = true;
                break;
            case "file":
                file = true;
                break;
            default:
                link = split[i];
                break;
        }
    }
    let data = {full:value,button:button,file:file,internal:internal,link:link};
    console.log(data);
    return data;
};

let setAttributes = function(node, data) {
    if(data.internal) {
        node.setAttribute('href', data.full);
        node.removeAttribute('target');
        node.removeAttribute('download');
        node.removeAttribute('data-file-id');
        node.setAttribute('data-page-id',data.link);
    } else {
        if(data.file) {
            node.setAttribute('href', data.full);
            node.setAttribute('download',"");
            node.setAttribute('target',"_blank");
            node.setAttribute('data-file-id',data.link);
            node.removeAttribute('data-page-id');
        } else {
            node.setAttribute('href', data.full);
            node.setAttribute('target',"_blank");
            node.removeAttribute('download');
            node.removeAttribute('data-file-id');
            node.removeAttribute('data-page-id');
        }
    }
    setButton(data.button,node);
};

class linkType extends Link {
    static create (initialValue) {
        let node;
        let data = getLinkData(initialValue);

        if(data.internal) {
            node = super.create("/");
        } else {
            node = super.create(data.link);
        }

        setAttributes(node, data);
        return node;
    }
    format(name, initialValue) {
        if (name === 'link' && initialValue) {
            let data = getLinkData(initialValue);
            setAttributes(this.domNode,data);
        } else {
            super.format(name, initialValue);
        }
    }
}
Quill.register(linkType);

const Parchment = Quill.import('parchment');


// This could be implemented via Parchment.Attributor.Class as well with a few modifications.
const pspacing = new Parchment.Attributor.Style('pspacing', 'padding-top');
Quill.register(pspacing, true);