<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Fastend Backend</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Overview</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <!--<div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                        <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        This week
                    </button>-->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Welcome to the Fastend Backend, <?php echo Session::getUser()->username ?>!</p>
                    <h5 class="text-center">Actions</h5>
                    <h6 class="text-center">Settings</h6>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/" class="btn btn-success w-100">View Website</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/pages" class="btn btn-outline-success w-100">Edit Pages</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/languages.php" class="btn btn-outline-success w-100">Edit Languages</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/mail-servers" class="btn btn-outline-success w-100">Edit Mail Servers</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/navbar" class="btn btn-outline-success w-100">Edit Navigation</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/footer" class="btn btn-outline-success w-100">Edit Footer</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/settings" class="btn btn-outline-success w-100">Edit Settings</a>
                </div>
                <div class="col-12">
                    <h6 class="text-center">Analytics</h6>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/pageViews" class="btn btn-outline-info w-100">Show Overview</a>
                </div>
                <div class="col-12 col-md-4 text-center mb-2">
                    <a href="/backend/pageViews" class="btn btn-outline-info w-100">Show Page Views</a>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>