<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Website Languages</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Languages</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Add Language</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>The languages of your website can be edited here.</p>
                </div>
            </div>
            <table class="table table-striped">
                <tr>
                    <th>Language</th>
                    <th>Short</th>
                    <th style="width: 100px;">Edit</th>
                </tr>
                <?php
                $languages = LanguageHandler::getAllLanguages();
                $defaultLanguage = Fastend::getSetting('standard_language_id');
                foreach ($languages as $language) {
                    $default = $defaultLanguage==$language->id?'<span class="text-muted"> (default)</span>':"";
                    echo '<tr><td>'.$language->name.$default.'</td><td>'.$language->shortName.'</td><td><a href="/backend/languages/edit" class="btn btn-sm btn-outline-success">Edit</a></td></tr>';
                }
                ?>
            </table>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>