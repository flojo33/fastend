<?php
/**
 * Created By: florianbayer
 * Date: 13.01.20
 * Time: 14:20
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

header('Content-Type: application/json');
function fail($reason) {
    echo json_encode(array("status"=>"fail","reason"=>$reason));
    exit;
}
if(!Session::isLoggedIn()){
    fail("Not logged in");
}
if(!isset($_POST['pageId']) || !isset($_POST['languageId']) || !isset($_POST['content']) || !isset($_POST['pageName']) || !isset($_POST['pageUrl'])) {
    fail("Invalid arguments given");
}
$pageQuery = Sql::executeQuery('SELECT * FROM `page` WHERE `id` = ?;','i',$_POST['pageId']);
if(count($pageQuery) != 1) {
    fail("Page does not exist");
}

$pageName = $_POST['pageName'];
$pageDescription = $_POST['pageDescription'];
$pageUrl = $_POST['pageUrl'];
$urlErrors = "";
$checkedUrl = PageHandler::CreateUrlName($pageUrl);

//$cleanedContent = str_replace("&nbsp;","", $_POST["content"]);
$cleanedContent = $_POST["content"];
if($checkedUrl != $pageUrl) {
    $urlErrors = "Urls may only contain lowercase letters (a-z), numbers (0-9), and the underscore character (_). The entered url was automatically adjusted.";
}
if($checkedUrl != $pageQuery[0]['url_name'] && count(Sql::executeQuery('SELECT * FROM `page` WHERE `parent_id` = 0 AND `url_name` = ?','s',$checkedUrl)) > 0) {
    $urlErrors = "Another page in this folder already exists with the url '".$checkedUrl."'. Please choose a different url.";
    $checkedUrl = $pageQuery[0]['url_name'];
}
$languageQuery = Sql::executeQuery('SELECT * FROM `language` WHERE `id` = ?;','i',$_POST['languageId']);
if(count($languageQuery) != 1) {
    fail("Language does not exist");
}
$contentQuery = Sql::executeQuery('SELECT * FROM `page_content` WHERE `page_id` = ? AND `language_id` = ?;','ii',$_POST['pageId'], $_POST['languageId']);
if(count($contentQuery) == 1) {
    Sql::executeCommand('UPDATE `page_content` SET `content` = ?, `name` = ?, `description` = ? WHERE `page_id` = ? AND `language_id` = ?;','sssii',$cleanedContent, $pageName, $pageDescription, $_POST['pageId'], $_POST['languageId']);
} else {
    Sql::executeCommand('INSERT INTO `page_content` (`page_id`,`name`,`description`,`language_id`,`content`) VALUES (?,?,?,?,?);','issis',$_POST['pageId'], $pageName, $pageDescription, $_POST['languageId'],$cleanedContent);
}
Sql::executeCommand("Update `page` SET `url_name` = ? WHERE `id` = ?","si",$checkedUrl,$_POST['pageId']);
echo json_encode(array("status"=>"success", "pageUrl"=>$checkedUrl, "fullPageLink"=>PageHandler::GetPageLinkWithId($_POST['pageId']), "pageName"=>$pageName, "urlErrors"=>$urlErrors));