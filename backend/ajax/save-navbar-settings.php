<?php
/**
 * Created By: florianbayer
 * Date: 13.06.20
 * Time: 12:44
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

Sql::executeCommand('UPDATE `system` SET `value` = ? WHERE `id` = ?',"ss",$_POST['showNavbar'], 'navbar_visible');
Sql::executeCommand('UPDATE `system` SET `value` = ? WHERE `id` = ?',"ss",$_POST['simpleNavbar'], 'navbar_simple');
Sql::executeCommandFast('DELETE FROM `navbar_items` WHERE 1;');
function SaveNode($parent, $node) {
    echo $node['name'].'<br>';
    Sql::executeCommand('INSERT INTO `navbar_items` (`id`,`parent_id`,`name`,`page_id`,`anchor`) VALUES (?,?,?,?,?)','iisis',$node['id'],$parent,$node['name'],$node['link'],$node['anchor']);
    if(isset($node['children'])) {
        foreach ($node['children'] as $child) {
            SaveNode($node['id'], $child);
        }
    }
}
foreach ($_POST['customData'] as $rootNode) {
    SaveNode(0, $rootNode);
}