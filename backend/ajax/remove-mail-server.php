<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!isset($_POST['id'])) {
    echo "No elements given!";
    exit;
}
Sql::executeCommand('DELETE FROM `mail_server` WHERE `id` = ?;',"i",$_POST['id']);