<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!isset($_POST["elements"])) {
    echo "No elements given!";
    exit;
}
$elements = json_decode($_POST['elements'],true);
MediaHandler::RemoveElements($elements);