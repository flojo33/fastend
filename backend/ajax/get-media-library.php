<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!isset($_POST['locationId'])) {
    $locationId = 0;
} else {
    $locationId = $_POST['locationId'];
}
header('Content-Type: application/json');
echo json_encode(MediaHandler::GetMediaAtLocation($locationId));