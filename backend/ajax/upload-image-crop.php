<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
$returnJson = array();
$rootPath = Fastend::GetMediaUploadsRoot("image");
if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
{
    if(isset($_POST["imageId"]) && isset($_FILES["cropBlob"])) {
        $imageId = $_POST["imageId"];
        $img = $_FILES["cropBlob"];
        $currentCropQuery = Sql::executeQuery('SELECT `crop_id` FROM `image_crop` WHERE `image_id` = ? ORDER BY `crop_id` DESC LIMIT 1',"s",$imageId);
        if(count($currentCropQuery) == 0) {
            $cropId = 1;
        } else {
            $cropId = $currentCropQuery[0]['crop_id'] + 1;
        }
        Sql::executeCommand('INSERT INTO `image_crop` (`image_id`,`crop_id`,`creation_date`) VALUES (?,?,NOW());',"si",$imageId,$cropId);


        $file = $rootPath.'crop_'.$imageId.'_'.$cropId.'.png';

        move_uploaded_file($img['tmp_name'],$file);

        header('Content-Type: application/json');
        echo json_encode(array("crop_id"=>$cropId));
    }
}

