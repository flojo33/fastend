<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}

$returnJson = array();

if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
{
    if(isset($_POST["locationId"])) {
        $locationId = intval($_POST["locationId"]);
        if(isset($_FILES['uploads'])) {
            for($i=0;$i<count($_FILES['uploads']['tmp_name']);$i++)
            {
                try{
                    FileType::UploadFile($_FILES['uploads']['name'][$i],$_FILES['uploads']['tmp_name'][$i],$_FILES['uploads']['size'][$i],$locationId);
                } catch (Exception $e) {
                    $returnJson[] = array("status"=>"Upload Fail: ".$e);
                }
            }
        } else {
            $returnJson[] = array("status"=>"Upload Fail: No Files uploaded!");
        }
        header('Content-Type: application/json');
        echo json_encode($returnJson);
    }
}

