<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!isset($_POST['locationId'])){
    $locationId = 0;
} else {
    $locationId = $_POST['locationId'];
}
if(!isset($_POST['name'])){
    exit;
} else {
    $name = $_POST['name'];
}
Sql::executeCommand('INSERT INTO `location` (`id`,`name`,`parent`) VALUES (null, ?,?)','si',$name,$locationId);