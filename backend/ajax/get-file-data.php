<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    echo "Not Logged in!";
    exit;
}
header('Content-Type: application/json');
echo json_encode(File::GetWithId($_POST["id"]));