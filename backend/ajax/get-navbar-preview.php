<?php
/**
 * Created By: florianbayer
 * Date: 13.06.20
 * Time: 12:34
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    echo "Not Logged in!";
    exit;
}
echo '<div style="min-height: 56px">';
echo Fastend::GetNavbar('navbar-dark bg-dark');
echo '</div>';