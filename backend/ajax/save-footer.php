<?php
/**
 * Created By: florianbayer
 * Date: 13.01.20
 * Time: 14:20
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

header('Content-Type: application/json');
function fail($reason) {
    echo json_encode(array("status"=>"fail","reason"=>$reason));
    exit;
}
if(!Session::isLoggedIn()){
    fail("Not logged in");
}
if(!isset($_POST['languageId']) || !isset($_POST['content'])) {
    fail("Invalid arguments given");
}

$cleanedContent = $_POST["content"];

$languageQuery = Sql::executeQuery('SELECT * FROM `language` WHERE `id` = ?;','i',$_POST['languageId']);
if(count($languageQuery) != 1) {
    fail("Language does not exist");
}

$contentQuery = Sql::executeQuery('SELECT * FROM `footer_content` WHERE `language_id` = ?;','i',$_POST['languageId']);
if(count($contentQuery) == 1) {
    Sql::executeCommand('UPDATE `footer_content` SET `content` = ? WHERE `language_id` = ?;','si',$cleanedContent, $_POST['languageId']);
} else {
    Sql::executeCommand('INSERT INTO `footer_content` (`language_id`,`content`) VALUES (?,?);','is', $_POST['languageId'],$cleanedContent);
}

echo json_encode(array("status"=>"success"));