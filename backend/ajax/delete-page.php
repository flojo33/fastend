<?php
/**
 * Created By: florianbayer
 * Date: 13.01.20
 * Time: 14:20
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()) {
    echo "Not Logged in!";
    exit;
}
if(!isset($_POST['pageId'])) {
    echo "Invalid arguments given!";
    exit;
}
$pageQuery = Sql::executeQuery('SELECT * FROM `page` WHERE `id` = ?;','i',$_POST['pageId']);
if(count($pageQuery) != 1) {
    echo "Page does not exist!";
    exit;
}

if(Fastend::getSetting('index_page') == $pageQuery[0]['id']) {
    echo "You cannot delete the index Page! Please set a new index page in the settings menu before removing this page.";
    exit;
}

Sql::executeCommand('UPDATE `page` SET `parent_id` = ? WHERE `parent_id` = ?;',"ii",$pageQuery[0]["parent_id"], $pageQuery[0]["id"]);
Sql::executeCommand('DELETE FROM `page` WHERE `id` = ?','i',$_POST['pageId']);

echo "success";