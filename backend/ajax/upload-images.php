<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
$valid_exts = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$max_size = 30000 * 1024; // max file size in bytes

$returnJson = array();
$rootPath = Fastend::GetMediaUploadsRoot()."images/";
if(!is_dir($rootPath)) {
    mkdir($rootPath,0777,true);
}
if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
{
    if(isset($_POST["locationId"])) {
        $locationId = intval($_POST["locationId"]);
        if(isset($_FILES['image'])) {
            for($i=0;$i<count($_FILES['image']['tmp_name']);$i++)
            {
                $original_name = $_FILES['image']['name'][$i];
                if(is_uploaded_file($_FILES['image']['tmp_name'][$i]) )
                {
                    // get uploaded file extension
                    $ext = strtolower(pathinfo($original_name, PATHINFO_EXTENSION));
                    // looking for format and size validity
                    if (in_array($ext, $valid_exts) AND $_FILES['image']['size'][$i] < $max_size)
                    {
                        // unique file path
                        $uid = uniqid();
                        $date = date('Y-m-d-H-i-s');
                        $path = $rootPath ."image_" . $uid . ".png";
                        $thumbnail_path = $rootPath . "image_thumb_" . $uid . ".png";

                        $tempFile = $_FILES['image']['tmp_name'][$i];
                        $ok = true;
                        switch ($ext) {
                            case "jpeg":
                            case "jpg":
                                $source_image = imagecreatefromjpeg($tempFile);
                                $exif = exif_read_data($tempFile);
                                if (!empty($exif['Orientation'])) {
                                    switch ($exif['Orientation']) {
                                        case 3:
                                            $source_image = imagerotate($source_image, 180, 0);
                                            break;

                                        case 6:
                                            $source_image = imagerotate($source_image, -90, 0);
                                            break;

                                        case 8:
                                            $source_image = imagerotate($source_image, 90, 0);
                                            break;
                                    }
                                }
                                break;
                            case "png":
                                $simg = imagecreatefrompng($tempFile);
                                $width = imagesx($simg);
                                $height = imagesy($simg);

                                $source_image = imagecreatetruecolor($width, $height); // png ?: gif
                                $background = imagecolorallocate($source_image , 0, 0, 0);
                                imagecolortransparent($source_image, $background);
                                imagealphablending($source_image, false);
                                imagesavealpha($source_image, true);

                                imagecopyresampled($source_image,$simg,0,0,0,0, $width,$height,$width,$height);
                                break;
                            case "gif":
                                //TODO load iamges as in png
                                $source_image = imagecreatefromgif($tempFile);
                                break;
                            default:
                                $ok = false;
                                break;
                        }
                        if($ok) {
                            imagepng($source_image, $path);
                            Image::CreatePngThumbnail($source_image, $thumbnail_path, 200);
                            $name = $_FILES['image']['name'][$i];
                            $description = $name." description";
                            Sql::executeCommand('INSERT INTO `image` (`id`,`name`,`description`,`upload_date`,`location_id`) VALUES (?,?,?,NOW(),?)','sssi',$uid,$name, $description, $locationId);
                            $returnJson[] = array("status"=>"success","filepath"=>$path,"original_name"=>$original_name);
                        } else {
                            $returnJson[] = array("status"=>"Upload Fail: Invalid file for rotation check!","original_file"=>$original_name);
                        }
                    }
                    else {
                        $returnJson[] = array("status"=>"Upload Fail: Unsupported file format or It is too large to upload!","original_file"=>$original_name);
                    }
                }
                else {
                    $returnJson[] = array("status"=>"Upload Fail: File not uploaded!","original_file"=>$original_name);
                }
            }
        } else {
            $returnJson[] = array("status"=>"Upload Fail: No Files uploaded!");
        }

        header('Content-Type: application/json');
        echo json_encode($returnJson);
    }
}

