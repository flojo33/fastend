<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
    $error = "";
    if(isset($_POST['login-username']) || isset($_POST['login-password'])) {
        if(Session::ExecuteLogin()) {
            header('Location: /backend/');
            exit;
        } else {
            $error = "Wrong Username/Password Combination. Please try again!";
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <title>Backend Login</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Backend Login</h1>
            <?php
            if($error != "") {
                echo '<p class="text-danger">'.$error.'</p>';
            }
            ?>
            <form action="/backend/login" method="post">
                <div class="form-group">
                    <label for="login-username">Username</label>
                    <input class="form-control" type="text" id="login-username" name="login-username">
                </div>
                 <div class="form-group">
                    <label for="login-password">Password</label>
                    <input class="form-control" type="password" id="login-password" name="login-password">
                </div>
                <button type="submit" class="btn btn-success">Login</button>
            </form>
        </div>
    </div>
</div>
<?php Fastend::GetScripts(); ?>
</body>
</html>