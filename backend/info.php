<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');

if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}

function embedded_phpinfo()
{
    ob_start();
    phpinfo();
    $phpinfo = ob_get_contents();
    ob_end_clean();
    $phpinfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $phpinfo);
    echo "
        <style type='text/css'>
            #phpinfo {}
            #phpinfo pre {margin: 0; font-family: monospace;}
            #phpinfo a:link {color: #009; text-decoration: none; background-color: #fff;}
            #phpinfo a:hover {text-decoration: underline;}
            #phpinfo table {border-collapse: collapse; border: 0; width: 934px; box-shadow: 1px 2px 3px #ccc;}
            #phpinfo .center {text-align: center;}
            #phpinfo .center table {margin: 1em auto; text-align: left;}
            #phpinfo .center th {text-align: center !important;}
            #phpinfo td, th {border: 1px solid #666; font-size: 75%; vertical-align: baseline; padding: 4px 5px;}
            #phpinfo h1 {font-size: 150%;}
            #phpinfo h2 {font-size: 125%;}
            #phpinfo .p {text-align: left;}
            #phpinfo .e {background-color: #b6b6b6; width: 300px; font-weight: bold;}
            #phpinfo .h {background-color: #d7d7d7; font-weight: bold;}
            #phpinfo .v {background-color: #ddd; max-width: 300px; overflow-x: auto; word-wrap: break-word;}
            #phpinfo .v i {color: #999;}
            #phpinfo img {float: right; border: 0;}
            #phpinfo hr {width: 934px; background-color: #ccc; border: 0; height: 1px;}
        </style>
        <div id='phpinfo'>
            $phpinfo
        </div>
        ";
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Fastend PHP Info</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">PHP Information</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php embedded_phpinfo(); ?>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>