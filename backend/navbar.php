<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <link rel="stylesheet" href="/backend/css/navbar-editor.css">
    <title>Fastend Backend</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Navigation</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <!--<div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                        <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        This week
                    </button>-->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="shadow p-3 mb-3">
                        <p>Preview</p>
                        <div id="preview-placeholder"></div>
                    </div>
                    <input type="hidden" name="saved" value="true">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="showNavbar" name="showNavbar" <?php echo ((Fastend::getSetting('navbar_visible'))=="true"?"checked":""); ?>>
                        <label class="custom-control-label" for="showNavbar">Show Navbar</label>
                    </div>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="simpleNavbar" name="simpleNavbar" <?php echo ((Fastend::getSetting('navbar_simple'))=="true"?"checked":""); ?>>
                        <label class="custom-control-label" for="simpleNavbar">Create Navigation from Page Layout</label>
                    </div>
                    <div id="custom-navbar-settings" class="shadow mt-2 mb-2 p-2 navbar-editor">
                        <div class="sortable-container">
                            <ol class="sortable">
                                <?php
                                    function renderItem($item) {
                                        echo '<li>';
                                        echo '<div class="content"><span class="name">'.$item->name.'</span><span class="d-none link">'.$item->page_id.'</span><span class="d-none anchor">'.$item->anchor.'</span><a href="#" class="ml-2 edit-button"><i class="fa fa-pen"></i></a><a href="#" class="ml-2 remove-button"><i class="fa fa-trash-alt"></i></a></div>';
                                        if(count($item->children) > 0) {
                                            echo '<ol>';
                                            foreach ($item->children as $child) {
                                                renderItem($child);
                                            }
                                            echo '</ol>';
                                        }
                                        echo '</li>';
                                    }

                                    foreach(NavigationRenderer::GetCustomNavBarItems() as $rootItem) {
                                        renderItem($rootItem);
                                    }
                                ?>
                            </ol>
                        </div>
                        <button id="add-button" class="btn btn-outline-success w-100 btn-sm rounded-0" type="button"><i class="fa fa-plus"></i> Add Item</button>
                    </div>
                </div>
        </main>
    </div>
</div>

<div class="modal" id="editItemModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="item-name">Item Name</label>
                    <input class="form-control" type="text" id="item-name">
                </div>
                <div class="form-group">
                    <label for="item-page">Page</label>
                    <select class="form-control" id="item-page">
                        <?php
                        $pages = PageHandler::getAllPages(LanguageHandler::GetCurrentLanguage());
                        foreach ($pages as $page) {
                            echo '<option value="'.$page->id.'">'.$page->name.'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="item-anchor">Page Anchor<br><small>Enter the name of a section to scroll to when clicking this link. Empty to simply load the page only.</small></label>
                    <input class="form-control" type="text" id="item-anchor">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" id="save-item-button" class="btn btn-primary">Save Item</button>
            </div>
        </div>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
<script src="/backend/js/jquery-ui-sortable.min.js"></script>
<script src="/backend/js/jquery.mjs.nestedSortable.min.js"></script>
<script>
    $(document).ready(function(){

        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            relocate: function() {
                saveSettings();
            }
        });

        let checkCustomVisible = function() {
            $('#custom-navbar-settings').attr("hidden", $('#simpleNavbar').is(':checked'));
        };
        $('#simpleNavbar').on('change',() => {
            checkCustomVisible();
            saveSettings();
        });
        $('#showNavbar').on('change',() => {
            saveSettings();
        });
        checkCustomVisible();

        let reloadPreview = function() {
            $.post('/backend/ajax/get-navbar-preview.php', function(data) {
                $('#preview-placeholder').html(data);
                updateDropdownMenus();
            });
        };
        reloadPreview();

        let id = 0;
        let createCustomData = function(root, start) {
            let children = [];
            let childContainer = root.children('ol');
            let content = root.children('.content');
            childContainer.children('li').each(function() {
                children.push(createCustomData($(this), false));
            });
            if(start) {
                id = 0;
                return children;
            } else {
                id ++;
                return {id: id, name:content.children(".name").html(), link:content.children('.link').html(), anchor:content.children('.anchor').html(), children: children};
            }
        };

        let saveSettings = function() {

            let customData = createCustomData($('.sortable-container'), true);
            $.ajax('/backend/ajax/save-navbar-settings.php', {
                method: 'post',
                data: {
                    showNavbar: $('#showNavbar').is(':checked'),
                    simpleNavbar: $('#simpleNavbar').is(':checked'),
                    customData: customData
                }
            }).done(function(data) {
                reloadPreview();
            });
            reloadPreview();
        };

        let addItem = function() {
            let item = $('<li><div class="content"><span class="name">New Item</span><span class="d-none link"></span><span class="d-none anchor"></span><a href="#" class="ml-2 edit-button"><i class="fa fa-pen"></i></a><a href="#" class="ml-2 remove-button"><i class="fa fa-trash-alt"></i></a></div></li>');
            let editButton = item.find('.edit-button');
            editButton.click(function() {
                editItem($(this));
            });
            let removeButton = item.find('.remove-button');
            removeButton.click(function() {
                removeItem($(this));
            });
            $('.sortable').append(item);
            saveSettings();
            editItem(editButton);
        };

        $('#add-button').click(function() {
            addItem();
        });

        let currentItem;

        let editItem = function(item) {
            currentItem = item.parent();
            let name = currentItem.children('.name').html();
            let pageId = currentItem.children('.link').html();
            let pageAnchor = currentItem.children('.anchor').html();
            $('#item-name').val(name);
            $('#item-page').val(pageId);
            $('#item-anchor').val(pageAnchor);
            $('#editItemModal').modal('show');
        };


        let removeItem = function(item) {
            item.parent().parent().remove();
            saveSettings();
        };

        let saveItem = function() {
            currentItem.children('.name').html($('#item-name').val());
            currentItem.children('.link').html($('#item-page').val());
            currentItem.children('.anchor').html($('#item-anchor').val());
            saveSettings();
        };

        $('#save-item-button').click(function () {
            saveItem();
            $('#editItemModal').modal('hide');
        });

        $('.edit-button').click(function() {
            editItem($(this));
        });

        $('.remove-button').click(function() {
            removeItem($(this));
        });
    });
</script>
</body>
</html>