<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!Session::checkRights(array('edit_users'))){
    header('Location: /backend/rights');
    exit;
}
if(isset($_POST["server-id"])) {
    $name = $_POST["server-name"];
    $host = $_POST["server-host"];
    $username = $_POST["server-username"];
    $password = $_POST["server-password"];
    $type = $_POST["server-type"];
    $port = $_POST["server-port"];
    $email = $_POST["server-email"];

    if($_POST["server-id"] == "") {
        //Create new Mail Server
        Sql::executeCommand('INSERT INTO `mail_server` (`id`,`name`,`email`,`username`,`password`,`server`,`port`,`type`) VALUES (NULL,?,?,?,?,?,?,?);','sssssis',$name,$email,$username,$password,$host,$port,$type);
    } else {
        //Save existing mail server
        $id = $_POST["server-id"];
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Users</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Mail Servers</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm btn-outline-success" id="addServerButton"><i class="fa fa-plus"></i> Add Mail Server</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Mail servers are used to send E-Mails from Contact forms. To setup a new Mail Server, Simply click on the "Add Mail Server Button"</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Host</th>
                        <th>E-Mail Address</th>
                        <th>Username</th>
                        <th>Connection Type</th>
                        <th>Port</th>
                        <th>Edit</th>
                    </tr>
                    <?php
                    $servers = Sql::executeQueryFast('SELECT * FROM `mail_server`');
                    foreach ($servers as $server) {
                        echo '<tr id="server-'.$server['id'].'"><td>'.$server['id'].'</td><td>'.$server["name"].'</td><td>'.$server["server"].'</td><td>'.$server["email"].'</td><td>'.$server["username"].'</td><td>'.$server["type"].'</td><td>'.$server["port"].'</td><td class="text-left" style="white-space: nowrap; overflow: hidden;"><button class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</button> <button class="delete-server-button btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i> Remove</button></td></tr>';
                    }
                    ?>
                </table>
            </div>
        </main>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="mailModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form autocomplete="off" action="" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="mailModalTitle">Edit Mail Server</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <input id="server-id" name="server-id" type="hidden" value="">
                        <div class="form-group">
                            <label for="server-name">Name</label>
                            <input id="server-name" autocomplete="new-password" name="server-name" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="server-email">E-Mail Address</label>
                            <input id="server-email" autocomplete="new-password" name="server-email" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="server-host">Server Hostname</label>
                            <input id="server-host" autocomplete="new-password" name="server-host" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="server-username">Username</label>
                            <input id="server-username" autocomplete="new-password" name="server-username" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for="server-password">Password</label>
                            <input id="server-password" autocomplete="new-password" name="server-password" class="form-control" type="password">
                        </div>
                        <div class="form-group">
                            <label for="server-type">Connection Type</label>
                            <select class="form-control" name="server-type" id="server-type">
                                <option value="tls">TLS</option>
                                <option value="ssl">SSL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="server-port">Port</label>
                            <input id="server-port" autocomplete="new-port" name="server-port" class="form-control" type="number" value="587">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="saveButton">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
<script>
    $('#addServerButton').on("click",function() {
        $('#mailModal').find("#server-id").val("");
        $('#mailModal').find("#server-name").val("");
        $('#mailModal').find("#server-host").val("");
        $('#mailModal').find("#server-email").val("");
        $('#mailModal').find("#server-username").val("");
        $('#mailModal').find("#server-password").val("");
        $('#mailModal').find("#server-port").val("587");
        $('#mailModal').find("#server-type").val("tls");
        $('#mailModal').find("#mailModalTitle").html("Add new Mail Server");
        $('#mailModal').find("#saveButton").html("Add Mail Server");
        $('#mailModal').modal("show");
    });

    $('.delete-server-button').each(function() {
        $(this).on("click",function() {
            let id = $(this).parent().parent().attr("id").replace("server-","");
            if(confirm("Do you really want to remove server "+id+"? If it is used in a Form, the form will not work anymore!")) {
                jQuery.ajax({
                    url: '/backend/ajax/remove-mail-server.php',
                    type: "POST",
                    data: {id:id},
                    complete: function() {
                        window.location = window.location;
                    }
                });
            }
        });
    });
</script>
</body>
</html>