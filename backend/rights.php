<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Access Denied</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 text-danger">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Access Denied</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>You do not have enough rights to view this page!</p>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>