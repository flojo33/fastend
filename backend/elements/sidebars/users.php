<nav class="col-md-3 col-lg-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/backend/users/">
                    Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/users/add">
                    Add Users
                </a>
            </li>
        </ul>
    </div>
</nav>