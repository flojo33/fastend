<nav class="col-md-3 col-lg-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/backend/">
                    Overview
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/pages">
                    Pages
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/languages">
                    Languages
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/mail-servers">
                    Mail Servers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/navbar">
                    Navigation
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/footer">
                    Footer
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/backend/settings">
                    Settings
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Analytics</span>
            <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Page Views
                </a>
            </li>
        </ul>
    </div>
</nav>