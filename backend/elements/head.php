<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Standard Template CSS -->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/fastend.css?dev">
<link rel="stylesheet" href="/css/multi-dropdown.css">

<!-- Backend Specific CSS -->
<link rel="stylesheet" href="/backend/css/dashboard.css">
<link rel="stylesheet" href="/backend/fontawesome/css/all.min.css">
