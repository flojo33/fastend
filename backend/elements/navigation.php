<nav class="navbar-dashboard navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 pr-2 shadow">
    <a class="navbar-brand col col-md-3 col-lg-2 mr-0" href="/backend/">Fastend Backend</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse px-3 mb-3 mb-md-0" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/backend/"><i class="far fa-window-maximize"></i> Website</a>
            </li>
            <?php if(Session::checkRights(array('edit_users'))) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/backend/users/"><i class="fa fa-user"></i> Users</a>
                </li>
            <?php } ?>
            <?php if(Session::checkRights(array('edit_media'))) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/backend/media/"><i class="fa fa-images"></i> Media</a>
                </li>
            <?php } ?>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo Session::getUser()->username; ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Account Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/backend/logout.php">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>