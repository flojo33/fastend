<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(isset($_POST["saved"])) {
    foreach ($_POST as $key=>$value) {
        Sql::executeCommand('UPDATE `system` SET `value` = ? WHERE `id` = ?',"ss",$value,$key);
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Website Settings</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Settings</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>The settings of your website can be edited here.</p>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Main</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="updates-tab" data-toggle="tab" href="#updates" role="tab" aria-controls="updates" aria-selected="false">Updates</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active pt-2" id="main" role="tabpanel" aria-labelledby="main-tab">
                            <form action="/backend/settings" method="post">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <h4>Site Settings</h4>
                                        <div class="form-group">
                                            <label for="site-name">Site Name</label>
                                            <input type="text" class="form-control" name="site_name" value="<?php echo Fastend::getSetting('site_name'); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="main-language">Index Page</label>
                                            <select class="form-control" name="index_page">
                                                <?php
                                                $pages =  PageHandler::getAllPages(LanguageHandler::GetCurrentLanguage());
                                                $defaultPage= Fastend::getSetting('index_page');
                                                function printPage($defaultPage, $page, $currentRoot = "") {
                                                    $selected = $defaultPage == $page->id ? "selected":"";
                                                    echo '<option value="'.$page->id.'" '.$selected.'>'.$currentRoot.$page->urlName.'</option>';
                                                    foreach ($page->children as $child){
                                                        printPage($defaultPage, $child, $currentRoot.$page->urlName."/");
                                                    }
                                                }
                                                foreach ($pages as $page) {
                                                    printPage($defaultPage, $page);
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="main-language">Website Theme</label>
                                            <select class="form-control" name="selected_theme">
                                                <?php
                                                $themes = scandir($_SERVER['DOCUMENT_ROOT']."/theme");
                                                $selectedTheme = Fastend::getSetting('selected_theme');
                                                foreach ($themes as $theme) {
                                                    if(in_array($theme,array(".",".."))) continue;
                                                    $selected = $theme == $selectedTheme?"selected":"";
                                                    //TODO improve this by getting a clean theme name from a specific
                                                    // file in the themes folder?
                                                    echo '<option value="'.$theme.'" '.$selected.'>'.$theme.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4>Language Settings</h4>
                                        <div class="form-group">
                                            <label for="main-language">Default Language</label>
                                            <select class="form-control" name="standard_language_id">
                                                <?php
                                                $languages = LanguageHandler::getAllLanguages();
                                                $defaultLanguage = Fastend::getSetting('standard_language_id');
                                                foreach ($languages as $language) {
                                                    $selected = $language->id == $defaultLanguage?"selected":"";
                                                    echo '<option value="'.$language->id.'" '.$selected.'>'.$language->name.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4>Google Analytics</h4>
                                        <div class="form-group">
                                            <label for="site-name">Site Tracking-ID</label>
                                            <input type="text" class="form-control" name="google_analytics_tag" value="<?php echo Fastend::getSetting('google_analytics_tag'); ?>">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success w-100 text-center">Save Settings</button>
                                    </div>
                                </div>
                                <input type="hidden" name="saved">
                            </form>
                        </div>
                        <div class="tab-pane fade pt-2" id="updates" role="tabpanel" aria-labelledby="updates-tab">
                            <h2>Updates</h2>
                            <p>Installed UpdateId: <?php echo Fastend::getSetting("latest_update_id"); ?></p>
                            <button class="btn btn-success"><i class="fa fa-sync"></i> Check for Updates</button>
                            <a class="btn btn-warning" href="/setup/updates/runUpdatesOnly.php"><i class="fa fa-check"></i> Check Database</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>