<?php
/**
 * Created By: florianbayer
 * Date: 31.12.19
 * Time: 14:00
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
Session::logOut();
header('Location: /');
exit;