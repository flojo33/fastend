<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Media Library</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2" id="current-name"></h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="mr-2 d-inline-flex" id="uploader">

                        </div>
                        <div class="btn-group mr-2">
                            <button type="button" class="btn btn-sm btn-outline-success" id="add-folder-button"><i class="fa fa-folder-plus"></i> Create Folder</button>
                        </div>
                    </div>
            </div>
            <div class="row media-library"></div>
        </main>
    </div>
</div>
<div class="bulk-edit-container bg-dark" id="action-container">
    <div class="container-fluid">
        <div class="row">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/index.php'); ?>
            <div class="col-md-9 col-lg-10 ml-sm-auto px-4 text-white">
                <strong>Selected <span class="selected-counter"></span> Items</strong>
                <button class="btn btn-danger btn-sm ml-3" id="delete-all-button"><i class="fa fa-trash-alt"></i> Delete All</button>
            </div>
        </div>
    </div>
</div>


<?php Fastend::GetScripts(); ?>
<script src="/backend/js/media-browser.js"></script>
<script>
    $('.media-library').mediaBrowser($('#current-name'),$('#uploader'),$('#action-container'),$('#add-folder-button'),function(id) {
        alert("Selected "+id);
    });
</script>
</body>
</html>