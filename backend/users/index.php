<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!Session::isLoggedIn()){
    header('Location: /backend/login');
    exit;
}
if(!Session::checkRights(array('edit_users'))){
    header('Location: /backend/rights');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/head.php'); ?>
    <title>Users</title>
</head>
<body>

<?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/navigation.php'); ?>

<div class="container-fluid">
    <div class="row">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/backend/elements/sidebars/users.php'); ?>
        <main role="main" class="col-md-9 col-lg-10 ml-sm-auto px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Index</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>The users can be edited here.</p>
                </div>
            </div>
        </main>
    </div>
</div>

<?php Fastend::GetScripts(); ?>
</body>
</html>