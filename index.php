<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/classes/Autoload.php');
if(!file_exists($_SERVER['DOCUMENT_ROOT']."/mysql-settings.php")){
    include $_SERVER['DOCUMENT_ROOT']."/setup/setup.php";
    exit;
}

function showPage($id) {
    PageRenderer::RenderPage($id);
    exit;
}

function handle404($backend = false) {
    if($backend) {
        include $_SERVER['DOCUMENT_ROOT'].'/backend/404.php';
    } else {
        PageRenderer::RenderErrorPage("404 Not Found");
    }
    exit;
}

$url = rtrim(explode("?",ltrim($_SERVER['REQUEST_URI'],"/"))[0],"/");
if($url == "robots.txt") {
    echo Seo::GenerateRobots();
    exit;
}
$urlElements = explode("/",$url);
$pagesFolder = $_SERVER['DOCUMENT_ROOT'].'/php/Pages';
$backend = in_array($urlElements[0],array("backend", "setup"));
if($backend) {
    if(is_dir($_SERVER['DOCUMENT_ROOT'].'/'.$url)) {
        $wantedPage = $_SERVER['DOCUMENT_ROOT'].'/'. rtrim($url, '/') . "/index.php";
    } else {
        $wantedPage = $_SERVER['DOCUMENT_ROOT'].'/'.$url.".php";
    }
    if(file_exists($wantedPage)){
        include $wantedPage;
        exit;
    } else {
        $result = glob ($_SERVER['DOCUMENT_ROOT'].'/'.$url.'.*');
        if(count($result) == 1){
            header('Location: '.str_replace($_SERVER['DOCUMENT_ROOT'],'',$result[0]));
            exit;
        } else {
            handle404($backend);
        }
    }
} else {
    if($url == "") {
        $index = Sql::executeQueryFast("SELECT * FROM `system` WHERE `id` = 'index_page';")[0]['value'];
        PageRenderer::RenderPage($index);
        exit;
    } else {
        $nextPages = array();
        for($i = count($urlElements)-1; $i>= 0; $i--) {
            if($i == count($urlElements)-1) {
                $currentElement = $urlElements[$i];
                if($i != 0) {
                    $initialQuery = Sql::executeQuery('SELECT `id`,`parent_id` FROM `page` WHERE `url_name` = ?;','s',$currentElement);
                } else {
                    $initialQuery = Sql::executeQuery('SELECT `id`,`parent_id` FROM `page` WHERE `url_name` = ? AND `parent_id` = 0;','s',$currentElement);
                }
                foreach ($initialQuery as $initialPossibility){
                    $nextPages[] = array('id'=>$initialPossibility['id'],'parent'=>$initialPossibility['parent_id'],'initial'=>$initialPossibility['id']);
                }
            }
            //No more possible pages found!
            if(count($nextPages) == 0) {
                handle404();
            }
            $nextStagePages = array();
            foreach ($nextPages as $pageToCheck) {
                if($i == 0) {
                    //Check if any of the pages have no parent -> Then it is the page we are looking for!
                    if($pageToCheck['parent'] == 0){
                        showPage($pageToCheck['initial']);
                    }
                } else {
                    //Find new parent pages where the name of the parent page is $urlElements[$i - 1];
                    $results = Sql::executeQuery('SELECT ? as initial,`id`,`parent_id` FROM `page` WHERE `url_name` = ? AND `id` = ?;',"isi",$pageToCheck['initial'],$urlElements[$i - 1], $pageToCheck['parent']);
                    foreach ($results as $result) {
                        $nextStagePages[] = array('id'=>$result['id'],'parent'=>$result['parent_id'],'initial'=>$result['initial']);
                    }
                }
            }

            $nextPages = $nextStagePages;
        }
    }
    handle404();
}