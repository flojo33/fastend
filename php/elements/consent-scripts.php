<script>
    const ConsentCookieDefinitions = {
        base: {
            name: "Grundfunktionen",
            desc: "Beinhaltet alle Tags die verwendet werden um die Grundfunktionalität dieser Webseite zu gewährleitsen.",
            required: true
        },
        recaptcha: {
            url: 'https://www.google.com/recaptcha/*',
            blacklistId: /https:\/\/www.google.com\/recaptcha\/*/,
            name: "Google ReCaptcha",
            desc: "Recaptcha benötigt um bei Kontakformularen auf die Echtheit des Nutzers zu prüfen",
            required: false,
            inactiveParent: '.captchaContainer'
        },
        maps: {
            url: 'https://www.google.com/maps/*',
            blacklistId: /https:\/\/www.google.com\/maps\/*/,
            name: "Google Maps",
            desc: "Wird benötigt um Karten elemente von Google richtig anzeigen zu können",
            required: false,
            inactiveParent: '.module-map'
        },
        analytics: {
            url: 'https://www.googletagmanager.com/*',
            blacklistId: /https:\/\/www.googletagmanager.com\/*/,
            name: "Google Analytics",
            desc: "Wird verwendet um Informationen über die Besucherzahlen auf unserer Webseite zu erfassen",
            required: false
        }
    };

    YETT_BLACKLIST = [];
    for (let key in ConsentCookieDefinitions) {
        if(key === "base") continue;
        YETT_BLACKLIST.push(ConsentCookieDefinitions[key].blacklistId);
    }
</script>
<script src="/js/yett.min.js"></script>