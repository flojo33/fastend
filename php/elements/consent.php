<div class="consent">
    <div class="accept-view">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h5>Cookies</h5>
                    <p>Diese Seite nutzt Website Tracking-Technologien von Dritten, um ihre Dienste anzubieten und stetig zu verbessern. Diese werden nur durch Ihre Zustimmung gesetzt. Weitere Details finden Sie in unserer <a href="/datenschutz">Datenschutzerklärung</a></p>
                    <button class="btn btn-primary" onclick="consent.allowAll();">Allen Zustimmen</button><button class="btn btn-secondary" onclick="consent.showSettings();">Details</button>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-button">
        <button class="btn" onclick="consent.showSettings();"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="fingerprint" class="svg-inline--fa fa-fingerprint fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256.12 245.96c-13.25 0-24 10.74-24 24 1.14 72.25-8.14 141.9-27.7 211.55-2.73 9.72 2.15 30.49 23.12 30.49 10.48 0 20.11-6.92 23.09-17.52 13.53-47.91 31.04-125.41 29.48-224.52.01-13.25-10.73-24-23.99-24zm-.86-81.73C194 164.16 151.25 211.3 152.1 265.32c.75 47.94-3.75 95.91-13.37 142.55-2.69 12.98 5.67 25.69 18.64 28.36 13.05 2.67 25.67-5.66 28.36-18.64 10.34-50.09 15.17-101.58 14.37-153.02-.41-25.95 19.92-52.49 54.45-52.34 31.31.47 57.15 25.34 57.62 55.47.77 48.05-2.81 96.33-10.61 143.55-2.17 13.06 6.69 25.42 19.76 27.58 19.97 3.33 26.81-15.1 27.58-19.77 8.28-50.03 12.06-101.21 11.27-152.11-.88-55.8-47.94-101.88-104.91-102.72zm-110.69-19.78c-10.3-8.34-25.37-6.8-33.76 3.48-25.62 31.5-39.39 71.28-38.75 112 .59 37.58-2.47 75.27-9.11 112.05-2.34 13.05 6.31 25.53 19.36 27.89 20.11 3.5 27.07-14.81 27.89-19.36 7.19-39.84 10.5-80.66 9.86-121.33-.47-29.88 9.2-57.88 28-80.97 8.35-10.28 6.79-25.39-3.49-33.76zm109.47-62.33c-15.41-.41-30.87 1.44-45.78 4.97-12.89 3.06-20.87 15.98-17.83 28.89 3.06 12.89 16 20.83 28.89 17.83 11.05-2.61 22.47-3.77 34-3.69 75.43 1.13 137.73 61.5 138.88 134.58.59 37.88-1.28 76.11-5.58 113.63-1.5 13.17 7.95 25.08 21.11 26.58 16.72 1.95 25.51-11.88 26.58-21.11a929.06 929.06 0 0 0 5.89-119.85c-1.56-98.75-85.07-180.33-186.16-181.83zm252.07 121.45c-2.86-12.92-15.51-21.2-28.61-18.27-12.94 2.86-21.12 15.66-18.26 28.61 4.71 21.41 4.91 37.41 4.7 61.6-.11 13.27 10.55 24.09 23.8 24.2h.2c13.17 0 23.89-10.61 24-23.8.18-22.18.4-44.11-5.83-72.34zm-40.12-90.72C417.29 43.46 337.6 1.29 252.81.02 183.02-.82 118.47 24.91 70.46 72.94 24.09 119.37-.9 181.04.14 246.65l-.12 21.47c-.39 13.25 10.03 24.31 23.28 24.69.23.02.48.02.72.02 12.92 0 23.59-10.3 23.97-23.3l.16-23.64c-.83-52.5 19.16-101.86 56.28-139 38.76-38.8 91.34-59.67 147.68-58.86 69.45 1.03 134.73 35.56 174.62 92.39 7.61 10.86 22.56 13.45 33.42 5.86 10.84-7.62 13.46-22.59 5.84-33.43z"></path></svg></button>
    </div>
</div>
<div class="modal fade" id="consentModal" tabindex="-1" role="dialog" aria-labelledby="consentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="consentModalLabel">Datenschutz Einstellungen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <small>Dieses Tool hilft Ihnen, verschiedene Tags / Tracker / Analyse-Tools auf dieser Webseite auszuwählen und zu deaktivieren. Weitere Details finden Sie in unserer <a href="/datenschutz">Datenschutzerklärung</a></small>
                <div class="consent-list"></div>
                <button class="btn btn-success w-100" onclick="consent.allowAll();">Alle Cookies Zulassen</button>
                <small>Falls Sie alle Cookies und Einstellungen dieser Seite löschen möchten, klicken Sie bitte <a href='#' onclick="consent.deleteAll();">Hier</a></small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="consent.closeSettings();">Abbrechen</button>
                <button type="button" class="btn btn-primary" onclick="consent.saveSettings();">Änderungen Speichern</button>
            </div>
        </div>
    </div>
</div>

<script>
    let consent = {
        modal: $('#consentModal'),
        allowAll:function() {
            document.cookie = "accepted-cookies="+this.getFullList()+";max-age=31536000"; //Allow cookie settings to be valid for a maximum of one year!
            this.checkSettings();
            this.closeSettings();
        },
        getFullList: function() {
            let list = "";
            for(let consentId in ConsentCookieDefinitions) {
                if(list !== "") list += ",";
                list += consentId;
            }
            return list;
        },
        deleteAll: function() {
            var cookies = document.cookie.split(";");

            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
            }
            window.location.reload();
        },
        saveSettings: function() {
            let list = "";
            let negated = false;
            $('.consent-check').each(function() {
                let name = $(this).attr("id").replace("consent-","");
                if(!$(this).is(":checked")) {
                    if(consent.getCookies().indexOf(name) !== -1) {
                        negated = true; //We revoked a cookie so we will need to reload the page!
                    }
                    return;
                }
                if(list !== "") list += ",";
                list += name;
            });
            document.cookie = "accepted-cookies="+list+";max-age=31536000"; //Allow cookie settings to be valid for a maximum of one year!
            if(negated) {

                window.location.reload();
            } else {
                this.checkSettings();
                this.closeSettings();
            }
        },
        showSettings: function() {
            let list = this.modal.find(".consent-list");
            let consentTable = '<table class="table table-striped"><tr><th>Cookie</th><th>Aktiviert</th></tr>';
            for(let consentId in ConsentCookieDefinitions) {
                let consent = ConsentCookieDefinitions[consentId];
                let checked = this.getCookies().indexOf(consentId) !== -1 || consent.required?" checked":"";
                let disabled = consent.required?" disabled":"";
                let switchButton = '<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input consent-check" id="consent-'+consentId+'"'+checked+disabled+'><label class="custom-control-label" for="consent-'+consentId+'"></label></div>';
                consentTable += "<tr><td>"+consent.name+'<br><small>'+consent.desc+"</small></td><td class='text-center'>"+switchButton+"</td></tr>";
            }
            consentTable += '</table>';
            list.html(consentTable);
            this.modal.modal('show');
        },
        getCookies: function() {
            let cookies = document.cookie.split(';').reduce((cookies, cookie) => {
                const [ name, value ] = cookie.split('=').map(c => c.trim());
                cookies[name] = value;
                return cookies;
            }, {});
            let accepted = cookies["accepted-cookies"];
            if(accepted !== undefined){
                return accepted.split(',');
            } else {
                return [];
            }
        },
        closeSettings: function() {
            this.modal.modal('hide');
            $('.edit-button .btn').addClass("activated");
            setTimeout(function() {
                $('.edit-button .btn').removeClass("activated");
            },1000);
        },
        checkSettings: function() {
            let cookies = this.getCookies();
            $('.consent').toggleClass('hidden', cookies.length > 0);
            for(let cookie in ConsentCookieDefinitions) {
                if(cookies.indexOf(cookie) === -1) {
                    let parent = ConsentCookieDefinitions[cookie].inactiveParent;
                    if(parent !== undefined){
                        $(parent).each(function() {
                            $(this).find('.consent-placeholder').remove();
                            $(this).append($('<div class="consent-placeholder consent-placeholder-'+cookie+'">'+ConsentCookieDefinitions[cookie].name+'<br><small>Dieses Element wurde durch Ihre Cookie Einstellungen Blockiert</small></div>'))
                        });
                    }
                    continue;
                } else {
                    $('.consent-placeholder.consent-placeholder-'+cookie).each(function() {
                        $(this).remove();
                    })
                }
                let cookieData = ConsentCookieDefinitions[cookie];
                let url = cookieData.url;
                if(url === undefined) continue;
                yett.unblock(url);
            }
        }
    };

    consent.checkSettings();
</script>