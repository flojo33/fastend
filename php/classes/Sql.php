<?php
/**
 * Created By: florianbayer
 * Date: 07.12.17
 * Time: 10:39
 */
if(file_exists($_SERVER['DOCUMENT_ROOT'].'/mysql-settings.php')){
    include($_SERVER['DOCUMENT_ROOT'].'/mysql-settings.php');
}
/*
Sql::initialize(Settings::$MySql_Hostname, Settings::$MySql_Username, Settings::$MySql_Password);
if (Settings::$MySql_Database != "") {
    Sql::connect(Settings::$MySql_Database);
}*/

class Sql
{
    static $intern_hostname;
    static $intern_username;
    static $intern_password;
    static $intern_database;
    static $mySqli;
    static $connected;

    /**
     * Initialize the Sql Class
     *
     * @param string $hostname The hostname of the Database.
     * @param string $username The Username for the Database.
     * @param string $password The Password for the Database.
     */
    public static function initialize($hostname, $username, $password)
    {
        self::$intern_hostname = $hostname;
        self::$intern_username = $username;
        self::$intern_password = $password;
        self::$connected = false;
    }

    /**
     * Connect to a given Database
     *
     * @param string $database The database to Connect to.
     * @throws Exception
     */
    public static function connect($database)
    {
        self::$intern_database = $database;
        self::disconnect();
        self::$mySqli = new mySqli(self::$intern_hostname, self::$intern_username, self::$intern_password, self::$intern_database);
        if (self::$mySqli->connect_errno) {
            self::$connected = false;
            throw new Exception("Failed to connect to MySql: " . self::$mySqli->connect_error);
        } else {
            self::$connected = true;
            if (!self::$mySqli->set_charset("utf8")) {
                echo "Error loading character set utf8: " . self::$mySqli->error;
            }
        }
    }

    /**
     * Disconnect from Database if connected
     */
    public static function disconnect()
    {
        //if(!$fromConnect) echo "Disconnect.<br>";
        if (self::$mySqli != null) {
            if (self::$mySqli->ping()) {
                self::$mySqli->close();
            }
        }
        self::$connected = false;
    }

    /**
     * Execute a Query on the database and return the rows as an Array
     *
     * @param string $queryString the Query that should be executed
     * @param $types
     * @param array ...$parameters
     * @return array containing rows containing columns containing data
     */
    public static function executeQuery($queryString, $types , ...$parameters)
    {
        if (!self::$connected) {
            echo $queryString.": Failed to execute query: Not Connected!<br>";
            return array();
        }
        if(strlen($types) != count($parameters)) {
            echo "Failed to execute query: Parameter Count not equal to Type Count<br>";
            return array();
        }
        try {
            if ($stmt = mySqli_prepare(self::$mySqli, $queryString)) {
                if(count($parameters) > 0){
                    $stmt->bind_param($types, ...$parameters);
                }
                $stmt->execute();
                if(($SqlError = $stmt->error) != "") {
                    echo  $queryString.": Failed to execute query: $SqlError<br>";
                    $stmt->close();
                    return array();
                } else {
                    $res = $stmt->get_result();
                    $stmt->close();
                    $outputRows = array();
                    while ($row = $res->fetch_assoc()) {
                        $outputRows[] = $row;
                    }
                    $res->close();
                    return $outputRows;
                }
            } else {
                echo "Failed to execute query: ".self::$mySqli->error."<br>";
                return array();
            }
        } catch (Exception $exception) {
            echo "Failed to execute query: $exception<br>";
            return array();
        }
    }

    public static function executeQueryFast($queryString) {
        if (!self::$connected) {
            echo $queryString.": Failed to execute query: Not Connected!<br>";
            return array();
        }

        if ($result = mySqli_query(self::$mySqli, $queryString)) {

            /* fetch object array */
            $outputRows = array();

            while ($row = $result->fetch_assoc()) {
                $outputRows[] = $row;
            }
            /* free result set */
            $result->close();

            return $outputRows;
        } else {
            echo self::$mySqli->error;
            return array();
        }
    }


    /**
     * Execute a Command on the Database that does not expect a return table.
     *
     * @param $queryString
     * @param $types
     * @param array ...$parameters
     * @return bool
     */
    public static function executeCommand($queryString, $types , ...$parameters)
    {
        if (!self::$connected) {
            echo  $queryString.": Failed to execute command: Not Connected!<br>";
            return false;
        }
        if(strlen($types) != count($parameters)) {
            echo "Failed to execute command: Parameter Count not equal to Type Count<br>";
            return false;
        }
        try {
            if ($stmt = mySqli_prepare(self::$mySqli, $queryString)) {
                if(count($parameters) > 0) {
                    $stmt->bind_param($types, ...$parameters);
                }
                $stmt->execute();
                $SqlError = $stmt->error;
                $stmt->close();
                if($SqlError != "") {
                    echo "Failed to execute command: $SqlError<br>";
                    return false;
                } else {
                    return true;
                }
            } else {
                echo "Failed to execute command: ".self::$mySqli->error."<br>";
                return false;
            }
        } catch (Exception $exception) {
            echo "Failed to execute command: $exception<br>";
            return false;
        }
    }

    public static function executeCommandFast($queryString) {
        if (!self::$connected) {
            echo $queryString.": Failed to execute query: Not Connected!<br>";
            return false;
        }

        if ($result = mySqli_query(self::$mySqli, $queryString)) {
            return true;
        } else {
            return false;
        }
    }

    public static function executeMultiQuery($multiQueryString) {
        if (!self::$connected) {
            echo $multiQueryString.": Failed to execute query: Not Connected!<br>";
            return false;
        }
        $result = mysqli_multi_query(self::$mySqli, $multiQueryString);

        while (mysqli_more_results(self::$mySqli)){ mysqli_next_result(self::$mySqli);}
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Pretty print the data from a MySql Query
     *
     * @param array $data the data from a previous mySql query.
     */
    public static function printQueryResult($data)
    {
        if (count($data) > 0) {
            //Echo Header with column names
            echo "<table border=\"1\"><tr>";
            foreach (array_keys($data[0]) as $key) {
                echo "<th>$key</th>";
            }
            echo "</tr>";
            //Echo Rows
            foreach ($data as $row) {
                echo "<tr>";
                foreach ($row as $column) {
                    echo "<td>$column</td>";
                }
                echo "</tr>";
            }
            echo "</table><br>";
        } else {
            //Echo Empty Query instead of table
            echo "Empty Query Result<br>";
        }
    }

    public static function insertId() {
        if(!self::$connected) {
            echo "No insert ID because you are not connected!";
            return -1;
        } else {
            return self::$mySqli->insert_id;
        }
    }
}