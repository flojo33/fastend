<?php


class Fastend
{
    private static $data = array();
    private static $settings;

    /**
     * @param $theme FastendTheme
     * @param $id
     * @param $url
     * @param $name
     * @param $description
     * @param $content
     * @param $footer
     * @param bool $editor
     */
    public static function SetupPage($theme, $id, $url, $name, $description, $content, $footer, $editor = false) {
        self::$data['theme'] = $theme;
        self::$data['page_id'] = $id;
        self::$data['page_url'] = $url;
        self::$data['page_name'] = $name;
        self::$data['page_content'] = $content;
        self::$data['page_footer'] = $footer;
        self::$data['page_description'] = $description;
        self::$data['editor_mode'] = $editor;
    }

    public static function GetPageName() {
        return self::$data['page_name'];
    }
    public static function GetPageDescription() {
        return self::$data['page_description'];
    }

    public static function GetNavbar($classes) {
        return NavigationRenderer::GetNavigation($classes);
    }

    public static function GetPageContent() {
        return '<div class="page-content">'."\n\t".self::$data['page_content']."\n".'</div>';
    }
    public static function GetScripts() {
        self::GetDefaultElement("scripts");
    }

    public static function GetPageFooter()
    {
        return self::$data['page_footer'];
    }

    public static function GetConsentScript()
    {
        if(!self::$data["editor_mode"]) {
            self::GetDefaultElement("consent-scripts");
        }
    }

    public static function GetConsent()
    {
        if(!self::$data["editor_mode"]) {
            self::GetDefaultElement("consent");
        }
    }

    public static function GetAnalyticsScript()
    {
        //Exclude the Subpages in the backend from analytics!
        if(!self::$data["editor_mode"]) {
            self::GetDefaultElement("analytics");
        }
    }

    /**
     * @return FastendTheme
     */
    private static function GetTheme()
    {
        return self::$data['theme'];
    }

    public static function GetSiteName()
    {
        return Sql::executeQueryFast("SELECT * FROM `system` WHERE `id` = 'site_name';")[0]['value'];
    }

    public static function CreateEditBar() {
        if(Session::isLoggedIn() && !self::$data["editor_mode"]) {
            echo '<a class="btn btn-primary btn-edit-page" href="/backend/pages/edit?id='.self::$data['page_id'].'">Edit Page</a>';
        }
    }

    public static function GetEditor() {
        return self::$data["editor_mode"];
    }

    public static function getSetting($name) {
        if(!isset(self::$settings)) {
            self::$settings = [];
            $settingsQuery = Sql::executeQueryFast('SELECT * FROM `system`');
            foreach ($settingsQuery as $row) {
                self::$settings[$row['id']] = $row['value'];
            }
        }
        return self::$settings[$name];
    }

    static function GetMediaUploadsRoot($type, $public = false) {
        $path = ($public?"/media/":self::GetMediaLibraryRoot())."uploads/".$type."/";

        if(!$public && !is_dir($path)) {
            mkdir($path);
        }

        return $path;
    }

    static function GetMediaLibraryRoot() {
        return $_SERVER['DOCUMENT_ROOT']."/media/";
    }

    static function GetDefaultElement($element) {
        include($_SERVER['DOCUMENT_ROOT']."/php/elements/".$element.".php");
    }
}