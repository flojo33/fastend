<?php


class PageRenderer
{
    /**
     * Renders a page to the output given its id and the currently selected Language.
     * @param $id int id of the page to render
     */
    public static function RenderPage($id) {
        try{
            $content = self::GetPageContent($id, LanguageHandler::GetCurrentLanguage());
        } catch (Exception $e) {
            //Could no fetch page content in current or standard language! Show an error page
            $content = self::GetErrorPage($e);
        }
        $theme = FastendTheme::GetSelectedTheme();
        $page = Sql:: executeQuery('SELECT * FROM `page` WHERE `id` = ?;','i',$id)[0];
        $theme->RenderPage($id, $page["url_name"], $content["name"], $content["description"], $content["content"]);
    }

    public static function RenderFooter($language = null, $editor = false) {
        if($language == null) {
            $language = LanguageHandler::GetCurrentLanguage()->id;
        }
        return self::GetPageFooter($language, $editor);
    }

    /**
     * Renders an error page to the output given its id and the currently selected Language.
     * @param $error string
     */
    public static function RenderErrorPage($error) {
        $content = self::GetErrorPage($error);
        $theme = FastendTheme::GetSelectedTheme();
        $theme->RenderPage(-1, "", "Error", "An Error occurred while trying to render the Page!", $content["content"]);
    }

    /**
     * Shows an error page in the current language.
     * @param $errorMessage
     * @return array
     */
    private static function GetErrorPage($errorMessage) {
        //TODO
        return array("name"=>"Error","description"=>"An Error occurred while trying to render the Page!","content"=>"Error: ".$errorMessage);
    }

    public static function RemoveDomElement($element) {
        $element->parentNode->removeChild($element);
    }

    public static function RemoveElementsIfExists($selector, $parent, $queryString) {
        foreach (self::GetElements($selector, $parent, $queryString) as $result) {
            self::RemoveDomElement($result);
        }
    }

    public static function RemoveElements($query) {
        foreach ($query as $result) {
            self::RemoveDomElement($result);
        }
    }

    public static function GetElements($selector, $parent, $queryString) {
        return $selector->query($queryString, $parent);
    }

    private static function GetPageFooter($languageId, $editor) {
        $contentQuery = Sql::executeQuery('SELECT * FROM `footer_content` WHERE `language_id` = ?','i',$languageId);
        if(count($contentQuery) != 1) {
            $languageId = LanguageHandler::GetStandardLanguageId();
            $contentQuery = Sql::executeQuery('SELECT * FROM `footer_content` WHERE `language_id` = ?','i',$languageId);
        }
        if(count($contentQuery) != 1) {
            $content = '';
        } else {
            $content = $contentQuery[0]['content'];
        }

        if(!$editor) {
            $content = str_replace ('&nbsp;', '@nbsp;', $content);

            if(trim($content) != "")  $content = self::RenderContent($content);
        }


        return '<div class="page-footer-container"><div class="page-footer">'."\n\t".$content."\n".'</div></div>';
    }

    /**
     * @param $pageId int
     * @param $language Language
     * @return array
     * @throws Exception
     */
    private static function GetPageContent($pageId, $language)
    {
        $contentQuery = Sql::executeQuery('SELECT * FROM `page_content` WHERE `page_id` = ? AND `language_id` = ?','ii',$pageId,$language->id);
        if(count($contentQuery) != 1) {
            $language = Language::GetLanguageWithId(LanguageHandler::GetStandardLanguageId());
            $contentQuery = Sql::executeQuery('SELECT * FROM `page_content` WHERE `page_id` = ? AND `language_id` = ?','ii',$pageId,$language->id);
        }
        if(count($contentQuery) != 1) {
            //throw new Exception("Page not found in the given language or the standard language!");
            $content = '<div class="container"><div class="row"><div class="col-12"><p class="text-danger">This page does not have any content yet!</p></div></div></div>';
            $description = 'Page does not have any content yet!';
            $name = 'Empty Page';
        } else {
            $content = $contentQuery[0]['content'];
            $description = $contentQuery[0]['description'];
            $name = $contentQuery[0]['name'];
        }
        //return $content;
        $content = str_replace ('&nbsp;', '@nbsp;', $content);
        if(trim($content) == "") return array("content"=>$content, "name"=>$name, "description"=>$description);

        $content = self::RenderContent($content);

        return array("content"=>$content, "name"=>$name, "description"=>$description);
    }

    private static function RenderContent($content) {
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        $selector = new DOMXPath($doc);

        ButtonHandler::RenderButtons($selector);

        //Process User forms
        $userForms = $selector->query('//form[contains(@class, "custom-contact-form")]');

        foreach ($userForms as $userForm) {
            FormHandler::HandleForm($selector, $userForm);
        }

        //return $content;
        $content = str_replace('@nbsp;', '&nbsp;',str_replace("<html><body>","",str_replace("</body></html>","",utf8_decode($doc->saveHTML($doc->documentElement)))));
        return $content;
    }
}