<?php
/**
 * Created By: florianbayer
 * Date: 29.12.19
 * Time: 12:41
 */

class PageHandler
{

    /**
     * @param Language $languageId
     * @return array
     */
    public static function getAllPages($languageId) {
        $pages = Sql::executeQuery('SELECT * FROM `page`,`page_content` WHERE `page`.`id` = `page_content`.`page_id` AND `page_content`.`language_id` = ? ORDER BY `order_id` ASC;','i',$languageId->id);
        $allPages = [];
        foreach ($pages as $page) {
            $allPages[$page['id']] = new Page($page['id'],$page['url_name'] ,$page['name'], $page['parent_id'], $page['visible_in_navigation']);
        }
        $rootPages = [];
        foreach ($allPages as $page) {
            if($page->parentId != "0") {
                $allPages[$page->parentId]->addChild($page);
            } else {
                $rootPages[] = $page;
            }
        }
        return $rootPages;
    }

    public static function createUrlName($name) {
        $simplified1 = str_replace('ä','ae',str_replace('ö','oe',str_replace('ü','ue',$name)));
        $simplified2 = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\uffff] remove',$simplified1);
        return preg_replace('/[^A-Za-z0-9\-]/', '', strtolower(str_replace(' ', '-', $simplified2)));
    }

    private static $currentAllPages;

    public static function GetPageLinkWithId($int)
    {
        if(!isset(self::$currentAllPages)) {
            self::$currentAllPages = [];
            $pages = Sql::executeQueryFast('SELECT * FROM `page` ORDER BY `order_id` ASC;');
            foreach ($pages as $page) {
                self::$currentAllPages[$page['id']] = $page;
            }
        }
        if(!isset(self::$currentAllPages[$int])) {
            $path = "";
        }
        else {
            $currentElement = self::$currentAllPages[$int];
            $path = $currentElement['url_name'];
            while($currentElement['parent_id'] != "0") {
                $currentElement = self::$currentAllPages[$currentElement['parent_id']];
                $path = $currentElement['url_name']."/".$path;
            }
        }
        return "/".$path;
    }
}