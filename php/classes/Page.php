<?php
/**
 * Created By: florianbayer
 * Date: 29.12.19
 * Time: 12:41
 */

class Page
{
    public $id, $parentId;
    public $name;
    public $children;
    public $visibleInNavigation;
    public $urlName;

    public function __construct($id, $urlName, $name, $parentId, $visibleInNavigation)
    {
        $this->id = $id;
        $this->urlName = $urlName;
        $this->parentId = $parentId;
        $this->name = $name;
        $this->visibleInNavigation = $visibleInNavigation == "1";
        $this->children = array();
    }

    /**
     * @param $child Page
     */
    public function addChild($child) {
        $this->children[] = $child;
    }
}