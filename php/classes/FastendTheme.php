<?php


class FastendTheme
{
    private $name;
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function RenderPage($id, $url, $pageName, $description, $content, $editor = false) {
        Fastend::SetupPage($this, $id, $url, $pageName, $description, $content, PageRenderer::RenderFooter(null, false), $editor);
        include($_SERVER['DOCUMENT_ROOT'].'/theme/'.$this->name.'/template.php');
    }

    public function RenderFooterEditor($language) {
        Fastend::SetupPage($this, 0, "", "Footer Editor", "", '<div class="container"><div class="row"><div class="col-12"><h1>Placeholder Content</h1><p>This is some simple placeholder content to demonstrate how the footer will look in comparison to the rest of the page.</p></div></div></div>', str_replace('class="page-footer-container"','class="page-footer-container not-fixed"',PageRenderer::RenderFooter($language, true)), true);
        include($_SERVER['DOCUMENT_ROOT'].'/theme/'.$this->name.'/template.php');
    }

    public static function GetSelectedTheme() {
        return new FastendTheme(Fastend::getSetting('selected_theme'));
    }

    public static function GetDefaultTheme() {
        return new FastendTheme('default');
    }
}