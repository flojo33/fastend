<?php

class LanguageHandler
{
    private static $sessionVariableName = "selected_language";

    /**
     * Get The Currently selected Language of the user. If no language is selected, store the standard language
     * as the selected language
     * @return Language
     */
    public static function GetCurrentLanguage() {
        if(!isset($_SESSION[self::$sessionVariableName])){
            $standardLanguageId = self::GetStandardLanguageId();
            $_SESSION[self::$sessionVariableName] = $standardLanguageId;
        }
        try {
            return Language::GetLanguageWithId($_SESSION[self::$sessionVariableName]);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Get the ID of the Language that is selected as the Standard language
     * @return int
     */
    public static function GetStandardLanguageId() {
        $query = Sql::executeQueryFast("SELECT `value` FROM `system` WHERE `id` = 'standard_language_id';");
        if(count($query) == 0) {
            return 1;
        } else {
            return intval($query[0]['value']);
        }
    }

    public static function getAllLanguages() {
        $output = [];
        $languages = Sql::executeQueryFast('SELECT `id` FROM `language`');
        foreach ($languages as $language) {
            try {
                $output[] = Language::GetLanguageWithId($language['id']);
            } catch (Exception $e) {
                //Ignored as it should not happen!
            }
        }
        return $output;
    }
}