<?php


class ButtonHandler
{

    public static function RenderButtons(DOMXPath $selector)
    {
        /*
        * Find all links to internal pages and setup their relative urls!
        */
        $internalLinks = $selector->query('//a[@data-page-id]');
        // loop through all found items
        foreach($internalLinks as $internalLink) {
            $href = $internalLink->getAttribute('data-page-id');
            $internalLink->setAttribute('href',PageHandler::GetPageLinkWithId($href));
            $internalLink->removeAttribute('data-page-id');
        }
        /*
         * Find all file links and setup their download paths
         */
        $fileLinks = $selector->query('//a[@data-file-id]');
        // loop through all found items
        foreach($fileLinks as $fileLink) {
            $fileId = $fileLink->getAttribute('data-file-id');
            $file = File::GetWithId($fileId);
            if($file != null) {
                $fileLink->setAttribute('href',$file->uploadLocation);
                $fileLink->setAttribute('download',$file->name);
            } else {
                $fileLink->setAttribute('href',"/404");
                $fileLink->removeAttribute('download');
            }
            $fileLink->removeAttribute('data-file-id');
        }

        /*
         * Remove the button attributes from button links
         */
        $resultButtons = $selector->query('//a[contains(@class, "btn")]');
        foreach($resultButtons as $linkButton) {
            $linkButton->setAttribute('href',str_replace("button-1:","",str_replace("button-2:","", $linkButton->getAttribute('href'))));
        }
    }
}