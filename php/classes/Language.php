<?php

class Language
{
    public $id;
    public $name;
    public $shortName;

    /**
     * Language constructor.
     * @param $name
     * @param $shortName
     * @param int $id
     * @throws Exception
     */
    public function __construct($name, $shortName, $id = 0)
    {
        if(strlen($shortName) > 2 || trim($shortName) == "") {
            throw new Exception("Short name must be 2 Characters long and may not be empty!");
        }
        $this->id = $id;
        $this->name = $name;
        $this->shortName = $shortName;
    }

    /**
     * Language constructor.
     * @throws Exception
     */
    public function StoreInDatabase() {
        if(!Sql::executeCommand('INSERT INTO `languages` (`id`,`name`,`short_name`) VALUES (null,?,?);',"ss",$this->name,$this->shortName)) {
            throw new Exception("Could not Create the Language because it already exists!");
        }
    }

    /**
     * Language constructor.
     * @param $languageId int id of the lagnuage that should be aquired from the database
     * @return Language
     * @throws Exception
     */
    public static function GetLanguageWithId($languageId) {
        $languageQuery = Sql::executeQuery('SELECT * FROM `language` WHERE `id` = ?','i',$languageId);
        if(count($languageQuery) == 1) {
            return new Language($languageQuery[0]['name'], $languageQuery[0]['short_name'], $languageQuery[0]['id']);
        } else {
            throw new Exception("Language was not found in Database!");
        }
    }
}