<?php
class Location
{
    public $id;
    public $parentId;
    public $name;

    public $children;

    public function __construct($id,$parentId, $name)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->name = $name;
        $this->children = array();
    }

    /**
     * @param $child Location
     */
    public function AddChild($child) {
        $this->children[] = $child;
    }

    public static function RemoveWithId($id) {
        $locationQuery = Sql::executeQuery('SELECT * FROM `location` WHERE `id` = ?;',"i",$id);
        if(count($locationQuery) == 0) return;
        $subLocationQuery = Sql::executeQuery('SELECT * FROM `location` WHERE `parent` = ?;',"i",$id);
        foreach ($subLocationQuery as $subLocation) {
            self::RemoveWithId($subLocation['id']);
        }
        $files = Sql::executeQuery('SELECT * FROM `file` WHERE `location_id` = ?;',"i",$id);
        $fileTypes = FileType::GetFileTypes();
        foreach ($files as $file) {
            $type = $fileTypes[$file["type"]];
            $type->Remove($file['id']);
        }
        Sql::executeCommand('DELETE FROM `location` WHERE `id` = ?;',"i",$id);
    }
}