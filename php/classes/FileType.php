<?php

class FileType
{
    public $extensions, $name, $thumbnailType, $uploadFunction, $removeFunction;

    private static $max_upload_file_size = 30000 * 1024; // max file size in bytes

    /**
     * FileType constructor.
     * @param $name string
     * @param $extensions string[]
     * @param $thumbnailType int
     * @param callable $uploadFunction
     * @param callable $removeFunction
     */
    public function __construct($name, $extensions, $thumbnailType, $uploadFunction, $removeFunction)
    {
        $this->name = $name;
        $this->extensions = $extensions;
        $this->thumbnailType = $thumbnailType;
        $this->uploadFunction = $uploadFunction;
        $this->removeFunction = $removeFunction;
    }

    public function CreateUploadLocation()
    {
        return $_SERVER['DOCUMENT_ROOT'] . "/media/uploads/" . $this->name . "/";
    }

    public function IsValidFile($file)
    {
        return in_array(pathinfo($file, PATHINFO_EXTENSION), $this->extensions);
    }

    /**
     * @param $fileName string
     * @param $tempFile string
     * @param $uid string
     * @return string stored file extension
     */
    public function Upload($fileName, $tempFile, $uid) {
        return call_user_func(array($this, $this->uploadFunction), $fileName, $tempFile, $uid);
    }

    /**
     * @param $id string
     */
    public function Remove($id) {
        call_user_func(array($this, $this->removeFunction), $id);
    }

    /**
     * @param $fileName string
     * @param $file string
     * @param $uid string
     * @return string
     * @throws Exception
     */
    private static function UploadDefaultFile($fileName, $file, $uid) {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $fileType = self::GetFileType($fileName);
        $path = Fastend::GetMediaUploadsRoot($fileType->name) . $uid . ".".$extension;
        if(!move_uploaded_file($file, $path)) throw new Exception("Could not move uploaded file!");
        return $extension;
    }

    /**
     * @param $fileName string
     * @param $file string
     * @param $uid string
     * @return string Extension .png
     * @throws Exception
     */
    private static function UploadImageFile($fileName, $file, $uid) {

        switch (pathinfo($fileName, PATHINFO_EXTENSION)) {
            case "jpeg":
            case "jpg":
                //$source_image = imagecreatefromstring(file_get_contents($file));
                $source_image = imagecreatefromjpeg($file);
                $exif = exif_read_data($file);

                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 3:
                            $source_image = imagerotate($source_image, 180, 0);
                            break;

                        case 6:
                            $source_image = imagerotate($source_image, -90, 0);
                            break;

                        case 8:
                            $source_image = imagerotate($source_image, 90, 0);
                            break;
                    }
                }
                break;
            case "png":
                $simg = imagecreatefrompng($file);
                $width = imagesx($simg);
                $height = imagesy($simg);

                $source_image = imagecreatetruecolor($width, $height); // png ?: gif
                $background = imagecolorallocate($source_image , 0, 0, 0);
                imagecolortransparent($source_image, $background);
                imagealphablending($source_image, false);
                imagesavealpha($source_image, true);

                imagecopyresampled($source_image,$simg,0,0,0,0, $width,$height,$width,$height);
                break;
            case "gif":
                //TODO load images as in png
                $source_image = imagecreatefromgif($file);
                break;
            default:
                throw new Exception("Invalid file for rotation check!");
        }
        $rootPath = Fastend::GetMediaUploadsRoot("image");
        $path = $rootPath . $uid . ".png";
        $thumbnail_path = $rootPath . "thumb_" . $uid . ".png";

        imagepng($source_image, $path);

        Image::CreatePngThumbnail($source_image, $thumbnail_path, 200);
        //images are always stored as pngs to simplify operations later on.*/
        return "png";
    }

    private static function RemoveImageFile($id) {
        Image::RemoveWithId($id);
    }
    private static function RemoveDefaultFile($id) {
        File::RemoveWithId($id);
    }

    public static function GetFileTypes()
    {
        return array(
            "image" => new FileType("image", array("png", "jpeg", "jpg", "gif"), "", 'UploadImageFile', 'RemoveImageFile'),
            "vector" => new FileType("vector", array("svg"), "", 'UploadDefaultFile', 'RemoveDefaultFile'),
            "video" => new FileType("video", array("mp4"), '<i class="fa fa-file-video"></i>', 'UploadDefaultFile', 'RemoveDefaultFile'),
            "pdf" => new FileType("pdf", array("pdf"), '<i class="fa fa-file-pdf"></i>', 'UploadDefaultFile', 'RemoveDefaultFile'),
            "word" => new FileType("word", array("doc","docx"), '<i class="fa fa-file-word"></i>','UploadDefaultFile', 'RemoveDefaultFile'),
            "excel" => new FileType("excel", array("xls","xlsx"), '<i class="fa fa-file-excel"></i>', 'UploadDefaultFile', 'RemoveDefaultFile'),
            "powerpoint" => new FileType("powerpoint", array("ppt","pptx"), '<i class="fa fa-file-powerpoint"></i>', 'UploadDefaultFile', 'RemoveDefaultFile'),
            "zip" => new FileType("zip", array("zip"), '<i class="fa fa-file-archive"></i>', 'UploadDefaultFile', 'RemoveDefaultFile'),
            "text" => new FileType("text", array("txt","log"), '<i class="fa fa-file"></i>', 'UploadDefaultFile', 'RemoveDefaultFile')
        );
    }

    /**
     * @param $file string
     * @return FileType
     * @throws Exception
     */
    private static function GetFileType($file) {
        $wantedFileType = null;
        foreach (self::GetFileTypes() as $name=>$fileType) {
            if($fileType->IsValidFile($file)) {
                $wantedFileType = $fileType;
                break;
            }
        }
        if($wantedFileType == null) throw new Exception("The file type is not valid");
        return $wantedFileType;
    }

    /**
     * @param $fileName string
     * @param $tempName string
     * @param $size int
     * @param $locationId int
     * @throws Exception
     */
    public static function UploadFile($fileName, $tempName, $size, $locationId) {

        if(!is_uploaded_file($tempName)) throw new Exception("This file should not have been uploaded!");

        //Check if file is too large
        if ($size > self::$max_upload_file_size) throw new Exception("The file is too large!");

        //Check file type and select the correct FileType class.
        $wantedFileType = self::GetFileType($fileName);
        // unique file path
        $uid = uniqid();

        $savedExtension = $wantedFileType->Upload($fileName, $tempName, $uid);

        $description = $fileName." description";
        Sql::executeCommand('INSERT INTO `file` (`id`,`name`,`description`,`upload_date`,`location_id`,`type`,`extension`) VALUES (?,?,?,NOW(),?,?,?)','sssiss',$uid, $fileName, $description, $locationId, $wantedFileType->name, $savedExtension);
    }
}