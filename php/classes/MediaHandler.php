<?php


class MediaHandler
{
    public static function GetMediaLibraryLocations() {
        //TODO add where to limit to users directories only!
        $locationsQuery = Sql::executeQueryFast('SELECT * FROM `location`;');
        $locations = [];
        $rootLocations = [];
        foreach ($locationsQuery as $locationRow) {
            $newLocation = new Location($locationRow['id'], $locationRow['parent'], $locationRow['name']);
            $locations[$locationRow['id']] = $newLocation;

        }
        foreach ($locations as $location) {
            if($location->parentId == 0) {
                $rootLocations[] = $location;
            } else {
                $locations[$location->parentId]->AddChild($location);
            }
        }
        return $rootLocations;
    }

    public static function GetMediaAtLocation($locationId) {
        $currentLocationQuery = Sql::executeQuery('SELECT * FROM `location` WHERE `id` = ?;','i',$locationId);
        $locationsQuery = Sql::executeQuery('SELECT * FROM `location` WHERE `parent` = ?;','i',$locationId);
        $fileQuery = Sql::executeQuery('SELECT * FROM `file` WHERE `location_id` = ?;','i',$locationId);
        if($locationId == 0) {
            $currentLocation = array("id"=>0,"name"=>"Media Library","parent"=>-1);
        } else {
            $currentLocation = $currentLocationQuery[0];
        }
        $fileTypes = FileType::GetFileTypes();
        for ($i = 0; $i < count($fileQuery); $i++) {
            $thumbnailType = $fileTypes[$fileQuery[$i]["type"]]->thumbnailType;
            if($thumbnailType == "") {
                $thumbnailLocation = '/media/uploads/'.$fileQuery[$i]['type'].'/thumb_'.$fileQuery[$i]['id'].'.'.$fileQuery[$i]['extension'];
                if(!file_exists($thumbnailLocation)) {
                    $thumbnailLocation = '/media/uploads/'.$fileQuery[$i]['type'].'/'.$fileQuery[$i]['id'].'.'.$fileQuery[$i]['extension'];
                }
                $thumbnail = "<div class=\"image\" style=\"background-image: url('".$thumbnailLocation."');\"></div>";
            } else {
                $thumbnail = $thumbnailType;
            }
            $fileQuery[$i]["thumbnail"] = $thumbnail;
        }
        for($i = 0; $i < count($locationsQuery); $i++) {
            $locationsQuery[$i]["thumbnail"] = '<i class="fa fa-folder></i>';
        }
        return array("currentLocation"=>$currentLocation,"locations"=>$locationsQuery, "files"=>$fileQuery);
    }

    public static function RemoveElements($elements) {
        $fileTypes = FileType::GetFileTypes();
        foreach ($elements as $element) {
            switch ($element["type"]) {
                case "location":
                    Location::RemoveWithId($element["id"]);
                    break;
                default:
                    $type = $fileTypes[$element["type"]];
                    $type->Remove($element['id']);
                    break;
            }
        }
}
}