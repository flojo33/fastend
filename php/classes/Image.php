<?php
class Image {
    /**
     * Create a .png thumbnail from an existing image resource
     * @param resource $image Input image resource
     * @param string $dest Path to save the image to
     * @param integer $targetWidth Target width of the thumbnail
     * @param integer $targetHeight Target Height of the thumbnail
     * @return bool returns true if the creation of the thumbnail was successful
     */
    static function CreatePngThumbnail($image, $dest, $targetWidth, $targetHeight = 0) {
        $width = imagesx($image);
        $height = imagesy($image);
        // maintain aspect ratio when no height set
        if ($targetHeight == 0) {
            // get width to height ratio
            $ratio = $width / $height;
            // if is portrait
            // use ratio to scale height to fit in square
            if ($width > $height) {
                $targetHeight = floor($targetWidth / $ratio);
            }
            // if is landscape
            // use ratio to scale width to fit in square
            else {
                $targetHeight = $targetWidth;
                $targetWidth = floor($targetWidth * $ratio);
            }
        }
        // create duplicate image based on calculated target size
        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

        $background = imagecolorallocate($thumbnail , 0, 0, 0);
        imagecolortransparent($thumbnail, $background);
        imagealphablending($thumbnail, false);
        imagesavealpha($thumbnail, true);

        // copy entire source image to duplicate image and resize
        imagecopyresampled(
            $thumbnail,
            $image,
            0, 0, 0, 0,
            $targetWidth, $targetHeight,
            $width, $height
        );
        //Save the $thumbnail to disk
        return imagepng($thumbnail, $dest, 9);
    }

    static function RemoveWithId($id) {
        $query = Sql::executeQuery('SELECT * FROM `file` WHERE `id` = ?;','s',$id);
        if(count($query) == 0) return;
        $cropQuery = Sql::executeQuery('SELECT * FROM `image_crop` WHERE `image_id` = ?;',"s",$id);
        foreach ($cropQuery as $crop) {
            unlink(Fastend::GetMediaUploadsRoot("image")."/crop_".$id.'_'.$crop['crop_id'].".png");
        }
        unlink(Fastend::GetMediaUploadsRoot("image")."/thumb_".$id.'.png');
        File::RemoveWithId($id);
    }
}