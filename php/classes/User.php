<?php
/**
 * Created By: florianbayer
 * Date: 31.12.19
 * Time: 11:41
 */

class User
{
    /*
     * Static functions for initializing users.
     */
    public static function userExists($username)
    {
        return count(Sql::executeQuery('SELECT * FROM `user` WHERE `username` = ?', 's', $username)) > 0;
    }
    public static function userWithIdExists($id)
    {
        return count(Sql::executeQuery('SELECT * FROM `user` WHERE `id` = ?', 'i', $id)) > 0;
    }

    public static function createNew($username, $password, $rights = [])
    {
        if (self::userExists($username)) {
            throw new Exception("A user with this username already exits!");
        }
        $hash = Passwords::generate_hash($password);
        Sql::executeCommand('INSERT INTO `user` (`id`,`username`,`password_hash`) VALUES (NULL,?,?);', "ss", $username, $hash);
    }

    /*
     * Non-Static functions
     */
    public $id;
    public $username;
    public $rights;

    public function __construct($id)
    {
        if(!self::userWithIdExists($id)) {
            throw new Exception("This user does not exist!");
        }
        $data = Sql::executeQuery("SELECT * FROM `user` WHERE `id` = ?;",'i',$id)[0];
        $this->id = $data['id'];
        $this->username = $data['username'];
        $this->rights = [];
        $rightsQuery = Sql::executeQuery("SELECT `session_right`.`name` as 'right_name' FROM `user_session_right`, `session_right` WHERE `user_session_right`.`session_right_id` = `session_right`.`id` AND `user_session_right`.`user_id` = ?;",'i',$this->id);
        foreach ($rightsQuery as $rightRow) {
            $this->rights[] = $rightRow['right_name'];
        }
    }

    /**
     * Add multiple rights given an array of right names.
     * @param $rightNameArray array
     * @throws Exception
     */
    public function addRights($rightNameArray) {
        foreach ($rightNameArray as $rightName) {
            try {
                self::addRight($rightName);
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * Add a right by the rights name
     * @param $rightName
     * @throws Exception
     */
    public function addRight($rightName) {
        $right = Sql::executeQuery('SELECT * FROM `session_right` WHERE `name` = ?','s',$rightName);
        if(count($right) == 1) {
            $id = $right[0]['id'];
            self::addRightById($id);
        } else {
            throw new Exception("The right ".$rightName." does not exist!");
        }
    }

    /**
     * Add a right by the rights id.
     * @param $id
     */
    private function addRightById($id) {
        Sql::executeCommand('INSERT INTO `user_session_right` (`user_id`,`session_right_id`) VALUES (?,?);', "ii", $this->id, $id);
    }

    /**
     * Returns true if the user has a right with the given name.
     * @param $rightName
     * @return bool
     */
    public function hasRight($rightName) {
        return in_array($rightName, $this->rights);
    }
}