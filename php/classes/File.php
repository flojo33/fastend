<?php


class File
{
    public $name;
    public $type;
    public $description;
    public $uploadLocation;
    public $thumbnailLocation;

    /**
     * File constructor.
     * @param $name string
     * @param $description string
     * @param $type FileType
     * @param $uploadLocation string
     * @param $thumbnailLocation string
     */
    public function __construct($name, $description, $type, $uploadLocation, $thumbnailLocation)
    {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->uploadLocation = $uploadLocation;
        $this->thumbnailLocation = $thumbnailLocation;
    }

    public static function RemoveWithId($id)
    {
        $query = Sql::executeQuery('SELECT * FROM `file` WHERE `id` = ?;','s',$id);
        if(count($query) == 0) return;
        unlink(Fastend::GetMediaUploadsRoot($query[0]["type"])."/".$id.'.'.$query[0]["extension"]);
        Sql::executeCommand('DELETE FROM `file` WHERE `id` = ?;',"s",$id);
    }

    public static function GetWithId($id) {
        $query = Sql::executeQuery('SELECT * FROM `file` WHERE `id` = ?;','s',$id);
        if(count($query) == 0) return null;
        $file = $query[0];
        $type = FileType::GetFileTypes()[$file["type"]];
        return new File($file["name"], $file["description"],$type, Fastend::GetMediaUploadsRoot($file["type"],true).$id.'.'.$file["extension"], Fastend::GetMediaUploadsRoot($file["type"])."/".$id.'.'.$file["extension"]);
    }
}