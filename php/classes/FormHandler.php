<?php


class FormHandler
{
    private static function HandleFormError($selector, $userForm, $error) {
        $errorElement = PageRenderer::GetElements($selector, $userForm, 'div[contains(@class, "form-error-text")]');
        if(count($errorElement) == 1) {
            $errorElement[0]->nodeValue = $error;
        }
        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-mail-title-text")]');
        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-success-text")]');
        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-receiver-text")]');
        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-sender-text")]');
    }

    public static function HandleForm($selector, $userForm) {
        $formId = str_replace("custom-form-","", $userForm->getAttribute("id"));
        $receiverEmailInputQuery = $selector->query('input[contains(@class, "form-receiver")]', $userForm);
        if(count($receiverEmailInputQuery) == 1) {
            $receiverInput = $receiverEmailInputQuery[0];
            $receiverMailServerId = $receiverInput->getAttribute('value');
            $mailServerQuery = Sql::executeQuery('SELECT * FROM `mail_server` WHERE `id` = ?;','i',$receiverMailServerId);
            if(count($mailServerQuery) == 1) {
                $mailServer = $mailServerQuery[0];
                PageRenderer::RemoveDomElement($receiverInput);

                $receiverTextQuery = PageRenderer::GetElements($selector, $userForm, 'div[contains(@class, "form-receiver-text")]');
                if(count($receiverTextQuery) > 0) {
                    $receiverText = str_replace("\n","<br>",$receiverTextQuery[0]->nodeValue);
                } else {
                    $receiverText = "";
                }
                PageRenderer::RemoveElements($receiverTextQuery);

                $subjectQuery = PageRenderer::GetElements($selector, $userForm, 'div[contains(@class, "form-mail-title-text")]');
                if(count($subjectQuery) > 0) {
                    $subject = $subjectQuery[0]->nodeValue;
                } else {
                    $subject = "";
                }
                PageRenderer::RemoveElements($subjectQuery);

                $senderTextQuery = PageRenderer::GetElements($selector, $userForm, 'div[contains(@class, "form-sender-text")]');
                if(count($senderTextQuery) > 0) {
                    $senderText = str_replace("\n","<br>",$senderTextQuery[0]->nodeValue);
                } else {
                    $senderText = "";
                }
                PageRenderer::RemoveElements($senderTextQuery);


                $captchaQuery = PageRenderer::GetElements($selector, $userForm, 'div[contains(@class, "captchaContainer")]');
                if(count($captchaQuery) > 0) {
                    $captchaKey = $captchaQuery[0]->getAttribute("data-secret-key");
                    $captchaQuery[0]->removeAttribute ("data-secret-key");
                } else {
                    $captchaKey = "";
                }

                if(isset($_POST["form-".$formId."-id"])) {
                    $success = true;

                    $results = array();
                    $senderMail = "";
                    $formGroupQuery = PageRenderer::GetElements($selector, $userForm, 'descendant::div[contains(@class, "form-group")]');
                    foreach ($formGroupQuery as $formGroup) {
                        $inputQuery = PageRenderer::GetElements($selector, $formGroup, 'descendant::*[contains(@class, "input-element")]');
                        if(count($inputQuery) != 0) {
                            $input = $inputQuery[0];
                            $name = PageRenderer::GetElements($selector, $formGroup, 'descendant::label')[0]->nodeValue;
                            //Check the inputs post data!
                            $id = $input->getAttribute("name");
                            if(isset($_POST[$id])) {
                                $value = $_POST[$id];
                            } else {
                                $value = "";
                            }
                            $classes = explode(" ",$formGroup->getAttribute("class"));
                            $required = in_array("required",$classes);
                            $type = "unknown";
                            foreach ($classes as $class) {
                                if($class == "main-mail") {
                                    $senderMail = $value;
                                    continue;
                                }
                                if(in_array($class, array("fixed","required","form-group"))) continue;
                                $type = str_replace("form-group-","",$class);
                            }
                            $ok = true;
                            if($required) {
                                switch ($type) {
                                    case "text":
                                    case "textarea":
                                        if(trim($value) == "") {
                                            $ok = false;
                                        }
                                        break;
                                    case "email":
                                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                            $ok = false;
                                        }
                                        break;
                                    case "checkbox":
                                        if ($value != "on") {
                                            $ok = false;
                                        }
                                        break;
                                    case "select":
                                        if ($value == "") {
                                            $ok = false;
                                        }
                                        break;
                                    default:
                                        $ok = false;
                                        break;
                                }
                            }
                            $results[$id] = array("name"=>$name,"input"=>$input,"value"=>$value,"id"=>$id,"type"=>$type,"required"=>$required, "ok"=>$ok);
                        }
                        if(!$ok) {
                            $success = false;
                        }
                    }

                    if(isset($_POST["form-".$formId."-captcha-response"])) {
                        $captchaResponse = $_POST["form-".$formId."-captcha-response"];
                    } else {
                        $captchaResponse = "";
                    }

                    $captchaData = array(
                        'secret' => $captchaKey,
                        'response' => $captchaResponse
                    );

                    $captchaVerify = curl_init();
                    curl_setopt($captchaVerify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                    curl_setopt($captchaVerify, CURLOPT_POST, true);
                    curl_setopt($captchaVerify, CURLOPT_POSTFIELDS, http_build_query($captchaData));
                    curl_setopt($captchaVerify, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($captchaVerify, CURLOPT_RETURNTRANSFER, true);
                    $captchaCertificationResponse = json_decode(curl_exec($captchaVerify),true);
                    if(!isset($captchaCertificationResponse["success"]) || !$captchaCertificationResponse["success"]) {
                        $success = false;
                        $captchaQuery[0]->setAttribute("class",$captchaQuery[0]->getAttribute("class")." is-invalid");
                    }

                    if($success == true && $senderMail =="") {
                        self::HandleFormError($selector, $userForm, "No User E-MaiL received.");
                        return;
                    }
                    if($success) {
                        $contents = "<br><br>";
                        foreach ($results as $result) {
                            if($result["type"] == "checkbox" && $result["required"]) continue;
                            switch ($result["type"]) {
                                case "checkbox":
                                    $value = $result["value"] == "on"?"Yes":"No";
                                    break;
                                case "textarea":
                                    $value = "<br>".str_replace("\n","<br>",$result["value"])."<br>";
                                    break;
                                default:
                                    $value = $result["value"];
                                    break;
                            }
                            $contents.= "<strong>".utf8_decode($result["name"]).":</strong> ".$value."<br>";
                        }
                        $sentInitialMail = Mail::SendMail($mailServer["email"], Fastend::getSetting("site_name") . " Webserver", utf8_decode(str_replace("\n","<br>",$receiverText)).$contents, $mailServer["email"], $mailServer["server"], $mailServer["username"], $mailServer["password"], $mailServer["type"], $mailServer["port"]);;
                        if (!$sentInitialMail) {
                            $success = false;
                        } else {
                            $success = Mail::SendMail($senderMail, utf8_decode($subject), utf8_decode(str_replace("\n","<br>",$senderText)).$contents, $mailServer["email"], $mailServer["server"], $mailServer["username"], $mailServer["password"], $mailServer["type"], $mailServer["port"]);
                        }
                    }
                    if($success) {
                        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-error-text")]');
                    } else {
                        PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-success-text")]');
                        foreach ($results as $result) {
                            switch ($result["type"]) {
                                case "select":
                                    $options = PageRenderer::GetElements($selector, $result["input"], 'option');
                                    foreach ($options as $option) {
                                        $value = $option->getAttribute("value");
                                        if(utf8_decode($value) == $result["value"]) {
                                            $option->setAttribute("selected","");
                                        } else {
                                            $option->removeAttribute("selected");
                                        }
                                    }
                                    break;
                                case "checkbox":
                                    if($result["value"] == "on") {
                                        $result["input"]->setAttribute("checked","");
                                    } else {
                                        $result["input"]->removeAttribute("checked");
                                    }
                                    break;
                                case "text":
                                case "email":
                                    $result["input"]->setAttribute("value",$result["value"]);
                                    break;
                                case "textarea":
                                    $result["input"]->nodeValue = $result["value"];
                                    break;
                            }
                            if(!$result["ok"]) {
                                $currentClasses = $result["input"]->getAttribute("class");
                                $result["input"]->setAttribute("class",$currentClasses." is-invalid");
                            } else {
                                if($result["required"]) {
                                    $currentClasses = $result["input"]->getAttribute("class");
                                    $result["input"]->setAttribute("class",$currentClasses." is-valid");
                                }
                            }
                        }
                    }
                } else {
                    PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-success-text")]');
                    PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-error-text")]');
                    PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-receiver-text")]');
                    PageRenderer::RemoveElementsIfExists($selector, $userForm, 'div[contains(@class, "form-sender-text")]');
                }
            } else {
                self::HandleFormError($selector, $userForm, "There is an error in this Forms configuration. Please Try again later.");
            }
        } else {
            self::HandleFormError($selector, $userForm, "There is an error in this Forms configuration. Please Try again later.");
        }
    }
}