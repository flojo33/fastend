<?php
/**
 * Created By: florianbayer
 * Date: 12.06.20
 * Time: 09:26
 */

class NavigationRenderer
{
    public static function GetNavigation($classes) {
        if(Fastend::getSetting('navbar_visible') == "false") return '';
        $navbar =
            '<nav class="navbar navbar-expand-lg '.$classes.'">
                <div class="container">
                    <a class="navbar-brand" href="/">' . Fastend::GetSiteName() . '</a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">';

        if(Fastend::getSetting('navbar_simple') == "true") {
            $pages = PageHandler::getAllPages(LanguageHandler::GetCurrentLanguage());

            foreach ($pages as $page) {
                $navbar .= self::RenderNavigationPage($page, false, "");
            }
        } else {
            foreach (self::GetCustomNavBarItems() as $rootItem) {
                $navbar .= self::RenderCustomNavBarItem($rootItem);
            }
        }

        $navbar .=
            '            </ul>
                    </div>
                </div>
            </nav>';
        return $navbar;
    }

    private static function RenderCustomNavBarItem($item) {
        if (count($item->children) > 0) {
            if ($item->parent_id != 0) {
                $output = '<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">' . $item->name . '</a><ul class="dropdown-menu">';
            } else {
                $output = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $item->name . '</a><ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">';
            }
            foreach ($item->children as $child) {
                $output .= self::RenderCustomNavBarItem($child);
            }

            $output .= '</ul></li>';
            return $output;
        } else {
            $linkPageId = $item->page_id;
            if(Fastend::getSetting('index_page') == $item->page_id) {
                $linkUrl = '/';
            } else {
                $linkUrl = PageHandler::GetPageLinkWithId($linkPageId);
            }
            $linkAnchor = $item->anchor;
            if($linkAnchor != "") {
                $linkUrl .= '#'.$linkAnchor;
            }
            if ($item->parent_id != 0) {
                return '<a class="dropdown-item" href="'.$linkUrl.'">' . $item->name . '</a>';

            } else {
                return '<li class="nav-item"><a class="nav-link" href="'.$linkUrl.'">' . $item->name . '</a></li>';
            }
        }
    }

    private static function RenderNavigationPage($page, $isChild, $currentUrl)
    {
        if (!$page->visibleInNavigation) return '';
        if (count($page->children) > 0) {
            if ($isChild) {
                $output = '<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">' . $page->name . '</a><ul class="dropdown-menu">';
            } else {
                $output = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $page->name . '</a><ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">';
            }
            foreach ($page->children as $child) {
                $output .= self::RenderNavigationPage($child, true, $currentUrl . '/' . $page->urlName);
            }

            $output .= '</ul></li>';
            return $output;
        } else {
            if ($isChild) {
                return '<a class="dropdown-item" href="' . $currentUrl . '/' . $page->urlName . '">' . $page->name . '</a>';

            } else {
                return '<li class="nav-item"><a class="nav-link" href="' . $currentUrl . '/' . $page->urlName . '">' . $page->name . '</a></li>';
            }
        }
    }

    public static function GetCustomNavBarItems() {
        $query = Sql::executeQueryFast('SELECT * FROM `navbar_items` ORDER BY `id` ASC');
        $allItems = [];
        foreach ($query as $row) {
            $object = (object) $row;
            $object->children = [];
            $allItems[$object->id] = $object;
        }
        $rootItems = [];
        foreach ($allItems as $item) {
            if($item->parent_id == 0) {
                $rootItems[] = $item;
            } else {
                $allItems[$item->parent_id]->children[] = $item;
            }
        }
        return $rootItems;
    }
}