<?php
/**
 * Created By: florianbayer
 * Date: 08.12.17
 * Time: 12:01
 */

class Passwords
{
    //Create Password Hash
    public static function createHashForPassword($password)
    {
        $hash = self::generate_hash($password, 12);
        return $hash;
    }

    //Verify A Password
    public static function verifyPassword($password, $hash)
    {
        return self::validate_pw($password, $hash);
    }

    //Maybe make it more difficult in the future
    static function salt()
    {
        return uniqid(mt_rand(), true);
    }

    static function validate_pw($password, $hash)
    {
        /* Regenerating the with an available hash as the options parameter should
         * produce the same hash if the same password is passed.
         */
        return crypt($password, $hash) == $hash;
    }

    static function generate_hash($password, $cost = 12)
    {
        /* To generate the salt, first generate enough random bytes. Because
         * base64 returns one character for each 6 bits, the we should generate
         * at least 22*6/8=16.5 bytes, so we generate 17. Then we get the first
         * 22 base64 characters
         */
        $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)), 0, 22);
        /* As blowfish takes a salt with the alphabet ./A-Za-z0-9 we have to
         * replace any '+' in the base64 string with '.'. We don't have to do
         * anything about the '=', as this only occurs when the b64 string is
         * padded, which is always after the first 22 characters.
         */
        $salt = str_replace("+", ".", $salt);
        /* Next, create a string that will be passed to crypt, containing all
         * of the settings, separated by dollar signs
         */
        $param = '$' . implode('$', array(
                "2y", //select the most secure version of blowfish (>=PHP 5.3.7)
                str_pad($cost, 2, "0", STR_PAD_LEFT), //add the cost in two digits
                $salt //add the salt
            ));

        //now do the actual hashing
        return crypt($password, $param);
    }
}