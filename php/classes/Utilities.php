<?php
class Utilities
{

    public static function RemoveDirectory($dir) {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }

        rmdir($dir);
    }

    /**
     * @param string $src source directory to copy from
     * @param string $dst destination directory to copy to
     * @param array $ignored list of ignore directories that will not be copied.
     */
    public static function CopyDirectory($src, $dst, $ignored=array()) {
        if(in_array($src,$ignored)) return;
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    self::CopyDirectory($src . '/' . $file,$dst . '/' . $file,$ignored);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public static function RunUpdates($initial = false) {
        include($_SERVER['DOCUMENT_ROOT'].'/setup/updates/updates.php');
        $maxId = count($updates);
        if($initial) {
            $currentId = 0;
        }else {
            $updateQuery = Sql::executeQueryFast("SELECT * FROM `system` WHERE `id` = 'latest_update_id'");
            if(count($updateQuery)==0) {
                $currentId = 0;
            } else {
                $currentId = intval($updateQuery[0]['value']);
            }
        }
        for($i = $currentId; $i < $maxId; $i++) {
            $updates[$i]();
        }
        Sql::executeCommand("Update `system` SET `value` = ? WHERE `id` = 'latest_update_id'","i",$maxId);
    }
}