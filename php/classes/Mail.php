<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/phpmailer/PHPMailerAutoload.php');

class Mail
{
    public static function SendMail($receiver, $subject, $message, $senderEmail, $host, $username, $password, $ssl, $port) {
        try {
            $mail = new PHPMailer;

            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $host;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $username;                 // SMTP username
            $mail->Password = $password;                           // SMTP password
            $mail->SMTPSecure = $ssl;                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $port;                                    // TCP port to connect to
            $mail->addAddress($receiver);
            $mail->setFrom($senderEmail);
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //$mail->addAttachment('');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = str_replace("<strong>","",str_replace("</strong>","",str_replace("<br>","\n",$message)));
            if(!$mail->send()) {
                return false;
            } else {
                return true;
            }
        }
        catch (Exception $e) {
            echo $e;
            return false;
        }
    }
}