<?php
/**
 * Created By: florianbayer
 * Date: 31.12.19
 * Time: 12:04
 */

class Session
{
    private static $session_started = false;
    public static function StartSession() {
        if(!self::$session_started) {
            self::$session_started = true;
            session_start();
        }
    }
    /**
     * Get the id of the currently logged in User.
     * @return integer
     */
    public static function getUserId() {
        self::StartSession();
        if(isset($_SESSION['user_id'])){
            return $_SESSION['user_id'];
        } else {
            return 0;
        }
    }

    /**
     * Returns true if a user is logged in.
     * @return bool
     */
    public static function isLoggedIn() {
        return self::getUserId() != 0;
    }


    /**
     * Set the currently logged in User to the user with $id
     * @param $id
     */
    public static function logIn($id) {
        self::StartSession();
        $_SESSION['user_id'] = $id;
    }

    /**
     * Log the current user out.
     */
    public static function logOut() {
        self::StartSession();
        unset($_SESSION['user_id']);
    }

    /**
     * Returns the currently logged in User and null if the user does not exist or is not logged in.
     * @return User|null
     */
    public static function getUser() {
        if(!self::isLoggedIn()) {
            return null;
        }
        $id = self::getUserId();
        if($id == 0) {
            return null;
        }
        try {
            return new User($id);
        } catch (Exception $e) {
            return null;
        }
    }


    /**
     * Returns true if the current logged in user has the given right.
     * @param $rightArray
     * @return bool
     */
    public static function checkRights($rightArray) {
        if(!self::isLoggedIn()) return false;
        $user = self::getUser();
        foreach ($rightArray as $right) {
            if(!$user->hasRight($right)) {
                return false;
            }
        }
        return true;
    }

    public static function ExecuteLogin() {
        $username = isset($_POST['login-username'])?trim($_POST['login-username']):"";
        $password = isset($_POST['login-password'])?trim($_POST['login-password']):"";
        if($username == "") {
            return false;
        } else {
            if($password == "") {
                return false;
            } else {
                $userQuery = Sql::executeQuery('SELECT * FROM `user` WHERE `username` = ?', 's', $username);
                if(count($userQuery) == 1) {
                    if(Passwords::verifyPassword($password,$userQuery[0]['password_hash'])) {
                        Session::logIn($userQuery[0]['id']);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
    }
}