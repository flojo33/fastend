# FastEnd

Easy to use PHP Framework for creating manageable websites with Bootstrap.

# Currently supported Features

- Automatically update to newest Git-Master version using the script in setup/updates/update.php

# Work in Progress

- Roll back to previous version
- Page Editor (Update of an existing editor coming up)
- Theme Editor (No Idea yet)